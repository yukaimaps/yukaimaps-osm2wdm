Import OpenStreetMap vers WDM
=============================

Le document suivant décrit comment les données OpenStreetMap (OSM) sont importées et converties au format [WDM](https://gitlab.com/yukaimaps/yukaidi-tagging-schema/-/blob/main/doc/Walk_data_model.md) dans le cadre du projet Yukaimaps.

**Table des matières :**

[[_TOC_]]

# Conventions utilisées dans le document

## abréviations utilisées

* OSM (OpenStreetMap): données issues du base de données libre et ouverte [OpenStreetMap](https://www.openstreetmap.org). [Voir la description du modèle attributaire](https://wiki.openstreetmap.org/).
* WDM (Walk Data Model): données du projet Yukaimaps décrivant les cheminements et l'accessibilité des lieux. [Voir la description du modèle attributaire](https://gitlab.com/yukaimaps/yukaidi-tagging-schema/-/blob/main/doc/Walk_data_model.md).
* ERP : Établissement recevant du public

## description des géométries

Pour les données OSM et pour les données WDM :

* ![node] noeud fait référence à un objet ponctuel
* ![way] chemin correspond à une succession de ![node] noeuds. Dans OSM, cela peut constituer soit un objet linéaire soit une zone dans le cas d'un chemin fermé formant un polygone
* ![area] zone fait référence à un objet polygonal. Il peut s'agir à la fois d'un ![way] chemin fermé ou d'une ![relation] relation, c'est-à-dire l'association de plusieurs ![way] chemins constituant un polygone ou un multi-polygone

[node]: https://wiki.openstreetmap.org/w/images/thumb/4/48/Osm_element_node.svg/20px-Osm_element_node.svg.png
[way]: https://wiki.openstreetmap.org/w/images/thumb/4/48/Osm_element_way.svg/20px-Osm_element_way.svg.png
[area]: https://wiki.openstreetmap.org/w/images/thumb/4/48/Osm_element_area.svg/20px-Osm_element_area.svg.png
[relation]:https://wiki.openstreetmap.org/w/images/thumb/4/48/Osm_element_relation.svg/20px-Osm_element_relation.svg.png

## description des tags

* highway=* signifie que la clef highway est présente, avec n'importe quelle valeur
* highway=`absent` signifie que la clef highway n'est pas présente
* conveying!=no signifie que la clef conveying est présente, et que la valeur associée n'est pas no. Par exemple conveying=yes
* conveying=yes/no signifie que la clef conveying a soit la valeur yes, soit la valeur no
* coveying!=yes/no signifie que la clef conveying est présente, et que sa valeur n'est ni yes, ni no. Par exemple conveying=reversible
* crossing:markings~dots signifie que la clef crossing:markings contient la valeur dots. Par exemple crossing:markings=dots ou crossing:markings=zebra;dots

Lorsqu'il est indiqué qu'un tag WDM est construit à partir de la valeur d'un tag OSM, il est sous-entendu que si le tag OSM n'est pas présent, aucun tag WDM ne sera créé dans ce cas.

# Paramétrage

Plusieurs modes d'imports sont disponibles :

* import des objets de transport
* import des ERP
* import des cheminements

À noter que les paramètres d'import sont cumulatifs : l'import des objets de transport et des cheminements produira un résultat différent de l'import des objets de transport puis des cheminements.

Il existe en complément un paramètre pour importer les rues et routes accessibles aux piétons.

# Règles générales

## Identifiants d'import

Trois attributs sont systématiquement ajoutés pour chaque objet WDM créé qui présente des tags :

| tag WDM            | source                                          |
| ------------------ | ----------------------------------------------- |
| Ref:Import:Source  | valeur fixe "OpenStreetMap"                     |
| Ref:Import:Id      | identifiant de l'objet OSM, sous la forme w1225 |
| Ref:Import:Version | version de l'objet OSM dans le présent export   |

Lorsqu'une transformation géométrique a eu lieu, l'objet aura également un tag WDM Ref:Import:Geometry:Id indiquant l'identifiant de l'objet dont est issu la géométrie.

## Géométries

Sauf cas particulier, la géométrie d'un objet OSM est convertie en géométrie d'objet WDM avec les règles suivantes :

* un ![node] noeud est converti en ![node] noeud
* un ![way] chemin est converti en ![way] chemin. Si le ![way] chemin OSM contient des ![node] noeuds qui ont été convertis, on retrouve ces ![node] noeuds sur le ![way] chemin WDM
* une ![area] zone de type ![way] chemin fermé est convertie en ![area] chemin fermé sur le même modèle
* une ![area] zone de type multipolygone (![relation] relation avec type=multipolygon) est convertie en ![area] chemin fermé en ne conservant que les éléments avec le rôle outer (ou pas de rôle si outer n'est pas présent). Si la géométrie résultante n'est pas un polygone, l'objet est importé sous forme ![node] noeud.

## Attributs génériques

Les attributs listés ici concernent tous les objets WDM créés.

### Name

Le tag WDM Name est rempli avec la valeur du tag name de l'objet OSM initial.

### Description

Le tag WDM Description est rempli avec la valeur du tag description de l'objet OSM initial.

### FixMe

Le tag WDM FixMe est rempli avec la valeur du tag fixme de l'objet OSM initial.

### WheelchairAccess:Description

Le tag WDM WheelchairAccess:Description est rempli avec la valeur du tag wheelchair:description (ou à défaut wheelchair:description:fr) de l'objet OSM initial.

### VisualSigns:Description

Le tag WDM VisualSigns:Description est rempli avec la valeur du tag deaf:description (ou à défaut deaf:description:fr) de l'objet OSM initial.

### AudibleSignals:Description

Le tag WDM AudibleSignals:Description est rempli avec la valeur du tag blind:description (ou à défaut blind:description:fr) de l'objet OSM initial.

### Image

Le tag Image est construit à partir des tags suivants, dans cet ordre de préférence :

| condition OSM       | valeur du tag WDM                                                               |
| ------------------- | ------------------------------------------------------------------------------- |
| image=*             | valeur du tag OSM                                                               |
| wikimedia_commons=* | "https://commons.wikimedia.org/wiki/" + la valeur du tag OSM                    |
| panoramax=*         | "https://api.panoramax.xyz/api/pictures/" + la valeur du tag OSM + "/sd.jpg" |

# Lecture des objets de transport

La lecture des objets de transport n'est réalisée que lorsque le paramètrage implique d'importer les objets de transport.

## Lecture des arrêts de bus

Les objets OSM suivants sont susceptibles d'être transformés en objet WDM Quay=Bus/Coach/TrolleyBus :

* highway=bus_stop
* public_transport=platform et bus=yes
* public_transport=platform et membre d'une ![relation] relation avec route=bus/coach/trolleybus
* public_transport=platform et coach=yes
* public_transport=platform et trolleybus=yes

L'objet WDM créé peut subir au préalable une transformation géométrique :

* S'il s'agit d'un ![node] noeud, la géométrie est conservée
* Sinon la géométrie sera ponctuelle

Si l'objet OSM fait référence à plusieurs modes, le Quay de l'objet WDM sera choisi dans cet ordre : Bus, puis Coach, puis TrolleyBus.

## Lecture des arrêts de bateau-bus

Les objets OSM suivants sont susceptibles d'être transformés en objet WDM Quay=Water :

* public_transport=platform et ferry=yes
* public_transport=platform et membre d'une ![relation] relation avec route=ferry

L'objet WDM créé peut subir au préalable une transformation géométrique :

* S'il s'agit d'un ![node] noeud, la géométrie est conservée
* Sinon la géométrie sera ponctuelle

## Lecture des quais et arrêts de tram

Les objets OSM suivants sont considérés :

* railway=platform et membre d'une ![relation] relation avec route=tram/light_rail
* public_transport=platform et tram=yes
* public_transport=platform et light_rail=yes

S'il s'agit d'un ![node] noeud, l'objet OSM sera transformé en Quay=Tram.
S'il s'agit d'un ![way] chemin linéaire non fermé, l'objet sera transformé en Quay=Tram avec une géométrie ponctuelle.
S'il s'agit d'une ![area] zone, l'objet sera transformé en InsideSpace=Quay et Quay=Tram.

## Lecture des quais de gare

Les ![area] zones OSM suivantes sont susceptibles d'être transformés en objet WDM InsideSpace=Quay et Quay=Rail :

* public_transport=platform et train=yes
* public_transport=platform et membre d'une ![relation] relation avec route=train
* railway=platform et membre d'une ![relation] relation avec route=train
* railway=platform et train=yes

## Lecture des quais de métro

Les ![area] zones OSM suivantes sont susceptibles d'être transformés en objet WDM InsideSpace=Quay et Quay=Metro  :

* public_transport=platform et subway=yes
* public_transport=platform et membre d'une ![relation] relation avec route=subway/monorail
* railway=platform et membre d'une ![relation] relation avec route=subway/monorail
* railway=platform et subway=yes
* public_transport=platform et monorail=yes
* railway=platform et monorail=yes

## Lecture des arrêts de funiculaire

Les objets OSM suivants sont susceptibles d'être transformés en objet WDM Quay=Funicular :

* public_transport=platform et funicular=yes
* public_transport=platform et membre d'une ![relation] relation avec route=funicular

L'objet WDM créé peut subir au préalable une transformation géométrique :

* S'il s'agit d'un ![node] noeud, la géométrie est conservée
* Sinon, la géométrie sera ponctuelle

## Lecture des arrêts de téléphérique urbain

Les objets OSM suivants sont susceptibles d'être transformés en objet WDM Quay=CableWay :

* public_transport=platform et aerialway=yes
* public_transport=platform et membre d'une ![relation] relation avec route=aerialway

L'objet WDM créé peut subir au préalable une transformation géométrique :

* S'il s'agit d'un ![node] noeud, la géométrie est conservée
* Sinon, la géométrie sera ponctuelle

## Conversions communes à tous les arrêts

Pour tous les arrêts précédemment listés (avec `Quay=*`), les attributs suivants peuvent être ajoutés.

### PublicCode

Le tag WDM PublicCode est rempli avec la valeur du tag local_ref (ou à défaut ref) de l'objet OSM initial.

### TactileWarningStrip

Le tag WDM TactileWarningStrip est rempli avec la valeur du tag OSM tactile_paving avec les mêmes règles de conversions que pour les obstacles et équipements.

### Lighting

Le tag WDM Lighting est rempli avec la valeur du tag OSM lit ou lit:perceived avec les mêmes règles de conversions que pour les cheminements.

### WheelchairAccess

Le tag WDM WheelchairAccess est construit à partir de la valeur du tag wheelchair de l'objet OSM initial.

| valeur du tag OSM | valeur du tag WDM |
| ----------------- | ----------------- |
| no                | No                |
| yes / designated  | Yes               |
| limited / bad     | Limited           |

### Outdoor

Le tag WDM Outdoor vaudra Covered si covered=yes, ou sera absent sinon.

### Shelter

Le tag WDM Shelter est construit à partir de la valeur du tag shelter de l'objet OSM initial.

| valeur du tag OSM | valeur du tag WDM |
| ----------------- | ----------------- |
| no                | No                |
| yes               | Yes               |
| autres conditions | tag non renseigné |

### Seating

Le tag WDM Seating est construit à partir de la valeur du tag bench de l'objet OSM initial.

| valeur du tag OSM | valeur du tag WDM |
| ----------------- | ----------------- |
| no                | No                |
| yes               | Yes               |
| autres conditions | tag non renseigné |

### RubbishDisposal

Le tag WDM RubbishDisposal est construit à partir de la valeur du tag bin de l'objet OSM initial.

| valeur du tag OSM | valeur du tag WDM |
| ----------------- | ----------------- |
| no                | No                |
| yes               | Yes               |
| autres conditions | tag non renseigné |

### AudibleSignals

| conditions OSM                                  | valeur du tag WDM |
| ----------------------------------------------- | ----------------- |
| departures_board:speech_output!=no              | Yes               |
| passenger_information_display:speech_output!=no | Yes               |
| announcement!=no                                | Yes               |
| speech_output!=no                               | Yes               |
| departures_board:speech_output=no               | No                |
| passenger_information_display:speech_output=no  | No                |
| announcement=no                                 | No                |
| speech_output=no                                | No                |
| autres conditions                               | tag non renseigné |

### VisualSigns

| conditions OSM                    | valeur du tag WDM |
| --------------------------------- | ----------------- |
| departures_board!=no              | Yes               |
| passenger_information_display!=no | Yes               |
| autres conditions                 | tag non renseigné |

### Ref:Export:Id

Les règles suivantes sont appliquées, dans l'ordre :

| conditions OSM    | valeur du tag WDM                                |
| ----------------- | ------------------------------------------------ |
| `ref:FR:Tallis`=* | FR:Quay:III: (avec III la valeur du tag OSM)     |
| `ref:FR:Mobigo`=* | FR:Quay:III: (avec III la valeur du tag OSM)     |

## Lecture des gares routières

Les ![node] noeuds et ![area] zones avec les attributs suivants sont convertis en objet WDM avec PointOfInterest=StopPlace et StopPlace=BusStation :

* amenity=bus_station
* public_transport=station et station=bus
* public_transport=station et bus=yes
* public_transport=station et coach=yes (dans ce cas, ce sera StopPlace=CoachStation)

## Lecture des stations de métro

Les ![node] noeuds et ![area] zones avec les attributs suivants sont convertis en objet WDM avec PointOfInterest=StopPlace et StopPlace=MetroStation :

* public_transport=station et station=subway/monorail
* public_transport=station et subway=yes
* public_transport=station et subway=monorail
* railway=station et station=subway/monorail
* railway=station et subway=yes
* railway=station et monorail=yes

## Lecture des stations de tramway

Les ![node] noeuds et ![area] zones avec les attributs suivants sont convertis en objet WDM avec PointOfInterest=StopPlace et StopPlace=TramStation :

* public_transport=station et station=tram/light_rail
* railway=station et station=tram/light_rail
* railway=station et tram=yes
* railway=station et light_rail=yes

## Lecture des gares ferroviaires

Les ![node] noeuds et ![area] zones avec les attributs suivants sont convertis en objet WDM avec PointOfInterest=StopPlace et StopPlace=RailStation :

* public_transport=station et station=train
* public_transport=station et train=yes
* railway=station/halt et station=train
* railway=station/halt et train=yes

## Lecture des gares de ferry

Les ![node] noeuds et ![area] zones avec les attributs suivants sont convertis en objet WDM avec PointOfInterest=StopPlace et StopPlace=FerryPort :

* public_transport=station et station=ferry
* public_transport=station et ferry=yes

## Lecture des gares de téléphérique

Les ![node] noeuds et ![area] zones avec les attributs suivants sont convertis en objet WDM avec PointOfInterest=StopPlace et StopPlace=LiftStation :

* aerialway=station
* public_transport=station et station=aerialway
* public_transport=station et aerialway=yes

## Lecture des aéroports

Les ![node] noeuds et ![area] zones avec les attributs suivants sont convertis en objet WDM avec PointOfInterest=StopPlace et StopPlace=Airport :

* aeroway=aerodrome et aerodrome:type=international/regional

## Conversions communes à toutes les gares et stations

Pour tous les objets précédemment listés avec PointOfInterest=StopPlace et StopPlace=`*`, tous les attributs présents sur les PointOfInterest=`*` peuvent également être ajoutés.

### AudioInformation

| conditions OSM                                  | valeur du tag WDM |
| ----------------------------------------------- | ----------------- |
| departures_board:speech_output!=no              | Yes               |
| passenger_information_display:speech_output!=no | Yes               |
| announcement!=no                                | Yes               |
| departures_board:speech_output=no               | No                |
| passenger_information_display:speech_output=no  | No                |
| announcement=no                                 | No                |
| autres conditions                               | tag non renseigné |

### VisualDisplays

| conditions OSM                                                     | valeur du tag WDM |
| ------------------------------------------------------------------ | ----------------- |
| departures_board!=no/timetable                                     | Yes               |
| passenger_information_display!=no                                  | Yes               |
| contient au moins 1 élément avec passenger_information_display!=no | Yes               |
| contient au moins 1 élément avec departures_board!=no              | Yes               |
| contient au moins 1 élément avec information=terminal              | Yes               |
| autres conditions                                                  | tag non renseigné |

### LargePrintTimetable

| conditions OSM                                                                     | valeur du tag WDM |
| ---------------------------------------------------------------------------------- | ----------------- |
| departures_board=timetable                                                         | Yes               |
| contient au moins 1 élément avec information=board et  board_type=public_transport | Yes               |
| contient au moins 1 élément avec departures_board=timetable                        | Yes               |
| autres conditions                                                                  | tag non renseigné |

### RealTimeDepartures

| conditions OSM                                                     | valeur du tag WDM |
| ------------------------------------------------------------------ | ----------------- |
| departures_board=realtime                                          | Yes               |
| passenger_information_display!=no                                  | Yes               |
| contient au moins 1 élément avec passenger_information_display!=no | Yes               |
| contient au moins 1 élément avec departures_board=realtime         | Yes               |
| autres conditions                                                  | tag non renseigné |

### Assistance

On ajoute le tag WDM Assistance=Boarding si la gare ou un élément à l'intérieur de la gare a le tag service:SNCF:acces_plus=yes.

### Ref:Export:Id

| conditions OSM    | valeur du tag WDM                                |
| ----------------- | ------------------------------------------------ |
| `ref:FR:uic8`=* | FR:StopPlace:StopArea_OCEIII_rail: (avec III la valeur du tag OSM)     |

# Lecture des cheminements

La lecture des cheminements n'est réalisée que lorsque le paramètrage implique d'importer les cheminements.

Les ![way] chemins OSM avec highway=* sont susceptibles d'être convertis en ![way] chemins WDM SitePathLink.

Les règles de conversion sont appliquées dans cet ordre :

| Conditions OSM                                                                 | Attribut WDM                                            |
| ------------------------------------------------------------------------------ | ------------------------------------------------------- |
| footway=sidewalk                                                               | SitePathLink=Pavement                                   |
| footway=crossing                                                               | SitePathLink=Crossing                                   |
| path=crossing                                                                  | SitePathLink=Crossing                                   |
| crossing!=no                                                                   | SitePathLink=Crossing                                   |
| highway=corridor                                                               | SitePathLink=Corridor                                   |
| conveying!=no et highway=steps                                                 | SitePathLink=Escalator                                  |
| conveying!=no                                                                  | SitePathLink=Travelator                                 |
| highway=steps                                                                  | SitePathLink=Stairs                                     |
| incline=up/down et highway=footway                                             | SitePathLink=Ramp                                       |
| tunnel=building_passage et highway=footway/path/steps/pedestrian/living_street | SitePathLink=Concourse                                  |
| tunnel=yes et highway=footway/path/steps/pedestrian/living_street              | SitePathLink=Corridor                                   |
| indoor=yes                                                                     | SitePathLink=Hall                                       |
| highway=platform                                                               | SitePathLink=Quay                                       |
| area=yes et highway=pedestrian/footway                                         | plusieurs SitePathLink=OpenSpace (voir § retraitements) |
| highway=living_street                                                          | SitePathLink=Street                                     |
| highway=pedestrian                                                             | SitePathLink=Street                                     |
| highway=footway/path                                                           | SitePathLink=Footpath                                   |
| highway=cycleway et foot=yes/designated et segredated=yes                      | SitePathLink=Pavement                                   |
| highway=cycleway et foot=yes/designated et segredated=no                       | SitePathLink=Footpath                                   |
| autres conditions                                                              | pas de SitePathLink=* créé                              |

Les ![node] noeuds OSM avec highway=elevator sont convertis en ![node] noeuds SitePathLink=Elevator.

Les ![area] zones OSM avec highway=elevator sont convertis en ![node] noeuds SitePathLink=Elevator (voir § retraitements).

Les ![node] noeuds OSM avec highway=crossing peuvent être convertis en ![way] chemins SitePathLink=Crossing (voir § retraitements)

## Retraitements

**Gestion des zones piétonnes**
<br>Pour la ![area] zone avec area=yes et highway=pedestrian/footway :

* on crée un ![node] noeud avec PathJunction=OpenSpace et Ref:Import:Geometry:Id, à l'intérieur de la zone
* puis on sélectionne les ![node] noeuds utiles qui constituent le contour de la zone : ce sont ceux qui ont des attributs propres ou qui sont reliés à un ![way] chemin highway=\*
* on crée alors un ![way] chemin avec SitePathLink=OpenSpace entre chacun de ces ![node] noeuds utiles et le ![node] noeud PathJunction=OpenSpace (sauf s'il existe déjà un chemin les reliant). Les différents attributs OSM de la zone sont alors importés sur ces objets et le contour de la zone est ignoré.
* si un des ![way] chemins générés SitePathLink=OpenSpace se situe en dehors de la zone initiale, le traitement est annulé et le contour de la zone est importé tel quel, avec SitePathLink=OpenSpace

**Gestion des ascenseurs en polygone**
<br>Pour la ![area] zone avec highway=elevator :

* s'il y a un ![node] noeud OSM avec highway=elevator à l'intérieur, on ignore la zone et seul le noeud est importé
* sinon, on crée un ![node] noeud avec SitePathLink=Elevator et Ref:Import:Geometry:Id, au barycentre de la zone. Les différents attributs OSM de la zone seront importés sur cet objet. Le contour de la zone est ignoré.
* puis on sélectionne les ![node] noeuds utiles qui constituent le contour de la zone : ce sont ceux qui ont des attributs propres ou qui sont reliés à un ![way] chemin highway=\*
* on crée alors un ![way] chemin avec SitePathLink=Corridor entre chacun de ces ![node] noeuds utiles et le ![node] noeud barycentre (sauf s'il existe déjà un chemin les reliant)
* on ajoute un tag Level sur chacun de ces ![way] chemins SitePathLink=Corridor, en concaténant les valeurs des tags OSM level et repeat_on, ou à défaut (si les 2 tags sont absents), la valeur du tag level sur la ![area] zone initiale

**Gestion des passages piétons**
<br>Pour les ![node] noeuds OSM avec highway=crossing qui ne font pas partie d'un ![way] chemin highway=footway, un ![way] chemin est généré perpendiculairement au chemin auquel il appartient dans OSM, d'une longueur de 3,5 m de chaque côté du ![node] noeud initial. Le ![way] chemin généré aura SitePathLink=Crossing, et les règles de conversion des attributs s'appliqueront à partir des attributs du noeud initial. Les deux ![node] noeuds d'extrémité du chemin auront PathJunction=Crossing.

**Gestion des noeuds de cheminements**
<br> Lors de la création d'un ![way] SitePathLink=Crossing, l'attribut PathJunction=Crossing est ajouté sur les ![node] noeuds de début et de fin.
<br> Lors de la création d'un ![way] SitePathLink=Stairs, l'attribut PathJunction=Stairs est ajouté sur les ![node] noeuds de début et de fin.

**Gestion des routes accessibles aux piétons**
<br>Lorsque la configuration implique d'importer les routes accessibles aux piétons, les ![way] chemins OSM suivants sont également importés :

* highway=\* et foot!=no
* highway=\* et sidewalk:right!=no/separate
* highway=\* et sidewalk:left!=no/separate
* highway=\* et sidewalk!=no/none/separate

Leur géométrie initiale est utilisée et les tags suivants sont appliqués : SitePathLink=Pavement et FixMe:Geometry : la géométrie de ce chemin est peut-être à modifier.

## Conversion des attributs

### WheelchairAccess

Le tag WDM WheelchairAccess est construit à partir de la valeur du tag wheelchair de l'objet OSM initial.

| valeur du tag OSM | valeur du tag WDM |
| ----------------- | ----------------- |
| no                | No                |
| yes / designated  | Yes               |
| limited / bad     | Limited           |

### Tilt, TiltSide

Le tag WDM Tilt est construit à partir de la valeur du tag
incline:across de l'objet OSM initial :

* si incline:across est de la forme `valeur%`, on prend la valeur absolue de cette valeur pour remplir le tag Tilt
* si incline:across est de la forme `valeur°`, on applique la conversion (`tan([incline:across in °]) * 100`) et on prend la valeur absolue du résultat pour remplir le tag Tilt

Si incline:across est positif, TiltSide vaut "Right". Sinon, il vaut "Left".

### Slope

Le tag WDM Slope est construit à partir de la valeur du tag
incline de l'objet OSM initial :

* si incline=up/down, le tag Slope n'est pas renseigné
* si incline est de la forme `valeur%`, on prend la valeur absolue de cette valeur pour remplir le tag Slope
* si incline est de la forme `valeur°`, on applique la conversion (`tan([incline in °]) * 100`) et on prend la valeur absolue du résultat pour remplir le tag Slope

### HighwayType

| conditions OSM                  | valeur du tag WDM  |
| ------------------------------- | ------------------ |
| footway=sidewalk                | Street             |
| footway=crossing                | Street             |
| highway=living_street           | LivingStreet       |
| highway=pedestrian/footway/path | Pedestrian         |
| highway=cycleway                | Bicycle            |
| source:maxspeed=FR:zone30       | LimitedSpeedStreet |
| zone:maxspeed=FR:30             | LimitedSpeedStreet |
| autres conditions               | tag non renseigné  |

### Width

Le tag WDM Width est construit à partir de la valeur du tag width ou est_width de l'objet OSM initial.

| conditions OSM                                                    | valeur du tag WDM                            |
| ----------------------------------------------------------------- | -------------------------------------------- |
| width est un nombre décimal positif                               | valeur de width                              |
| width est un nombre décimal positif, suivi d'un espace et de "m"  | valeur de width sans l'unité                 |
| width est un nombre décimal positif, suivi d'un espace et de "cm" | valeur de width sans l'unité, divisé par 100 |
| autres conditions                                                 | tag non renseigné                            |

Cas particulier : si l'objet initial est un ![node] highway=elevator, on utilisera à la place door:width, avec les mêmes règles de gestion, pour remplir l'attribut Width.

### InternalWidth

Si l'objet initial est un ![node]![way] highway=elevator, le tag WDM InternalWidth est construit à partir de la valeur du premier tag suivant rencontré, avec les mêmes règles de gestion que Width :

* maxwidth:physical
* width
* est_width

Sinon, le tag n'est pas renseigné.

### Depth

Si l'objet initial est un ![node]![way] highway=elevator, le tag WDM Depth est construit à partir de la valeur du premier tag suivant rencontré, avec les mêmes règles de gestion que Width :

* maxlength:physical
* length
* est_length

Sinon, le tag n'est pas renseigné.

### Attendant

Le tag WDM Attendant est construit à partir de la valeur du tag supervised de l'objet OSM initial :

* Yes si supervised!=no
* No si supervised=no

Le tag n'est conservé que si l'objet WDM est bien un SitePathLink=Elevator.

### StructureType

| conditions OSM    | valeur du tag WDM |
| ----------------- | ----------------- |
| tunnel=yes        | Tunnel            |
| covered=yes       | Underpass         |
| bridge=yes        | Overpass          |
| indoor=yes        | Corridor          |
| autres conditions | tag non renseigné |

### Outdoor

| conditions OSM                         | valeur du tag WDM |
| -------------------------------------- | ----------------- |
| indoor=yes                             | No                |
| tunnel=yes                             | No                |
| tunnel=building_passage                | Covered           |
| covered=yes                            | Covered           |
| autres conditions et highway!=elevator | Yes               |

### Incline

| conditions OSM                            | valeur du tag WDM |
| ----------------------------------------- | ----------------- |
| incline=up                                | Up                |
| incline=down                              | Down              |
| incline est un nombre positif (en % ou °) | Up                |
| incline est un nombre négatif (en % ou °) | Down              |
| autres conditions                         | tag non renseigné |

### FlooringMaterial

Le tag WDM FlooringMaterial est construit à partir de la valeur du tag surface de l'objet OSM initial :

| valeurs du tag OSM                                   | valeur du tag WDM |
| ---------------------------------------------------- | ----------------- |
| asphalt / tarmac                                     | Asphalt           |
| carpet                                               | Carpet            |
| concrete / concrete:lanes / concrete:plates / cement | Concrete          |
| ground / dirt / unpaved / sand / earth               | Earth             |
| clay / mud / stepping_stone / snow / soil            | Earth             |
| grass_paver                                          | FibreglassGrating |
| grass / artificial_turf                              | Grass             |
| gravel / fine_gravel / pebblestone / chipseal        | Gravel            |
| paving_stones / sett / cobblestone                   | PavingStones      |
| unhewn_cobblestone /  paving_stones:30 / bricks      | PavingStones      |
| plastic / linoleum                                   | PlasticMatting    |
| rubber                                               | Rubber            |
| compacted                                            | Sand              |
| metal / metal_grid                                   | SteelPlate        |
| rock / stone / bare_rock / rocks                     | Stone             |
| wood                                                 | Wood              |
| tartan / woodwchips / decoturf / acrylic             | Other             |
| autre valeur                                         | tag non renseigné |

### Flooring

Le tag WDM Flooring est construit à partir de la valeur du tag smoothness de l'objet OSM initial.

| valeurs du tag OSM                | valeur du tag WDM |
| --------------------------------- | ----------------- |
| excellent/good                    | Good              |
| intermediate                      | Worn              |
| bad/very_bad                      | Discomfortable    |
| horrible/very_horrible/impassable | Hazardous         |

### LinearCue

Le tag WDM LinearCue est construit à partir de la valeur du tag tactile_paving, ou à défaut guide_strips de l'objet OSM initial.

| conditions OSM                | valeur du tag WDM |
| ----------------------------- | ----------------- |
| tactile_paving=yes/contrasted | GuidingStrip      |
| guide_strips!=no              | GuidingStrip      |

**Cas particuliers** : 
<br>Si l'objet OSM était un ![node] noeud avec highway=crossing, on ajoute plutôt le tag TactileWarningStrip (à partir de tactile_paving) sur les ![node] noeuds PathJunction=Crossing de début et de fin du SitePathLink.
<br>Si l'objet est un SitePathLink=Stairs, on ajoute plutôt le tag TactileWarningStrip (à partir de tactile_paving) sur les ![node] noeuds PathJunction=Stairs de début et de fin du SitePathLink, avec les règles de conversion suivantes :

* TactileWarningStrip=None sur les deux noeuds si tactile_paving=no
* TactileWarningStrip=Good et FixMe:TactileWarningStrip="Vérifier l’état de la bande d'éveil à vigilance" sur les deux noeuds si tactile_paving=yes/contrasted
* TactileWarningStrip=Good et FixMe:TactileWarningStrip="Vérifier l’état de la bande d'éveil à vigilance" sur le noeud du haut de l'escalier si tactile_paving=partial
* TactileWarningStrip=Good et FixMe:TactileWarningStrip="Vérifier l’état de la bande d'éveil à vigilance" sur le noeud du bas de l'escalier si tactile_paving=incorrect

Le haut et le bas de l'escalier sont calculés à l'aide du tag OSM incline et du sens du tracé. Si le tag incline est absent et que tactile_paving=partial/incorrect, aucun tag n'est ajouté sur les noeuds PathJunction=Stairs.

### Lighting

Le tag WDM Lighting est construit à partir de la valeur du tag lit et éventuellement lit:perceived de l'objet OSM initial.

| conditions OSM                | valeur du tag WDM |
| ----------------------------- | ----------------- |
| lit=no                        | No                |
| lit!=no                       | Yes               |
| lit:perceived=good/daylike    | Yes               |
| lit:perceived=none/minimal    | No                |
| autre valeur de lit:perceived | Bad               |

### Crossing

Le tag WDM Crossing est renseigné uniquement si SitePathLink=Crossing.

Voici les conditions qui s'appliquent sur le ![way] chemin d'origine pour renseigner le tag Crossing :

| conditions OSM                                                                             | valeur du tag WDM |
| ------------------------------------------------------------------------------------------ | ----------------- |
| comprend un ![node] noeud avec railway=crossing                                            | Rail              |
| comprend un ![node] noeud avec railway=level_crossing                                      | Rail              |
| comprend un ![node] noeud avec railway=railway=tram_crossing                               | UrbanRail         |
| comprend un ![node] noeud avec railway=tram_level_crossing                                 | UrbanRail         |
| comprend un ![node] noeud qui appartient aussi à un ![way] chemin railway=tram/light_rail  | UrbanRail         |
| comprend un ![node] noeud qui appartient aussi à un ![way] chemin railway!=tram/light_rail | Rail              |
| comprend un ![node] noeud qui appartient aussi à un ![way] chemin highway=cycleway         | CycleWay          |
| autres conditions                                                                          | Road              |

### CrossingIsland

Le tag WDM CrossingIsland est renseigné uniquement si SitePathLink=Crossing.

Les conditions OSM sont à vérifier à la fois sur le ![way] chemin d'origine ainsi que sur les éventuels ![node] noeuds qui constituent le chemin.


| conditions OSM      | valeur du tag WDM |
| ------------------- | ----------------- |
| crossing:island=yes | Yes               |
| crossing:island=no  | No                |
| autres conditions   | tag non renseigné |

### CrossingBarrier

Le tag WDM CrossingBarrier est renseigné uniquement si SitePathLink=Crossing.

Les conditions OSM sont à vérifier à la fois sur le ![way] chemin d'origine ainsi que sur les éventuels ![node] noeuds qui constituent le chemin.

| conditions OSM       | valeur du tag WDM |
| -------------------- | ----------------- |
| crossing:barrier!=no | Yes               |
| crossing:barrier=no  | No                |
| autres conditions    | tag non renseigné |

### PedestrianLights

Le tag WDM PedestrianLights est renseigné uniquement si SitePathLink=Crossing.

Les conditions OSM sont à vérifier à la fois sur le ![way] chemin d'origine ainsi que sur les éventuels ![node] noeuds qui constituent le chemin.

| conditions OSM           | valeur du tag WDM |
| ------------------------ | ----------------- |
| crossing=traffic_signals | Yes               |
| flashing_lights=yes      | Yes               |
| crossing:light=yes       | Yes               |
| crossing:signals=yes     | Yes               |
| button_operated=yes      | Yes               |
| flashing_lights=no       | No                |
| crossing:light=no        | No                |
| crossing:signals=no      | No                |
| crossing=uncontrolled    | No                |
| crossing=unmarked        | No                |
| autres conditions        | tag non renseigné |

### ZebraCrossing

Le tag WDM ZebraCrossing est renseigné uniquement si SitePathLink=Crossing.

Les conditions OSM sont à vérifier à la fois sur le ![way] chemin d'origine ainsi que sur les éventuels ![node] noeuds qui constituent le chemin.

| conditions OSM                   | valeur du tag WDM            |
| -------------------------------- | ---------------------------- |
| crossing=unmarked                | None                         |
| crossing:markings=no             | None                         |
| crossing:markings=surface        | None                         |
| crossing:markings=dots           | None                         |
| crossing:markings=dashes         | None                         |
| crossing:markings=pictograms     | None                         |
| crossing:markings~zebra          | Good (voir cas particuliers) |
| crossing:markings~zebra:double   | Good (voir cas particuliers) |
| crossing:markings~zebra:bicolour | Good (voir cas particuliers) |
| crossing_ref=zebra               | Good (voir cas particuliers) |
| autres conditions                | tag non renseigné            |

**Cas particulier**

Si ZebraCrossing=Good, on lui ajoute le tag suivant

* FixMe:ZebraCrossing = Vérifier l'état du marquage de ce passage piéton

### VisualObstacle

Le tag WDM VisualObstacle est renseigné uniquement si SitePathLink=Crossing.

Si crossing:buffer_marking = both/left/right/yes, le tag VisualObstacle=None est ajouté sur les ![node] noeuds PathJunction=Crossing de début et de fin du SitePathLink.

### TactileWarningStrip

Le tag WDM TactileWarningStrip est renseigné uniquement si SitePathLink=Crossing.

Si un ![node] des noeuds qui constituent le chemin a les tags highway=crossing et tactile_paving alors le tag TactileWarningStrip est ajouté sur les ![node] noeuds PathJunction=Crossing de début et de fin du SitePathLink, avec les règles de gestion suivantes :

| Conditions OSM                | Valeur du tag WDM            |
| ----------------------------- | ---------------------------- |
| tactile_paving=no             | None                         |
| tactile_paving=yes/contrasted | Good (voir cas particuliers) |
| autres conditions             | attribut non renseigné       |

**Cas particuliers** : Si TactileWarningStrip=Good, on lui ajoute le tag FixMe:TactileWarningStrip="Vérifier l’état du marquage de la bande d'éveil à vigilance"

### KerbDesign

Le tag WDM KerbDesign est renseigné uniquement si SitePathLink=Crossing.

Si un ![node] des noeuds qui constituent le chemin a les tags highway=crossing et kerb alors le tag KerbDesign est ajouté sur les ![node] noeuds PathJunction=Crossing de début et de fin du SitePathLink, avec les règles de gestion suivantes :

| conditions OSM    | valeur du tag WDM               |
| ----------------- | ------------------------------- |
| kerb=raised       | Raised                          |
| kerb=rolled       | Raised                          |
| kerb=lowered      | Lowered (voir cas particuliers) |
| kerb=flush        | Flush                           |
| kerb=no           | None                            |
| autres conditions | tag non renseigné               |

**Cas particuliers** : lorsque KerbDesign=Lowered, on ajoute un tag FixMe:KerbDesign="Vérifier s'il s'agit d'un ressaut abaissé ou d'un bateau".

### Bollard

Le tag WDM Bollard est renseigné uniquement si SitePathLink=Crossing.

Si un ![node] des noeuds qui constituent le chemin a les tags highway=crossing et crossing:bollard alors le tag Bollard est ajouté sur les ![node] noeuds PathJunction=Crossing de début et de fin du SitePathLink, avec les règles de gestion suivantes :

| conditions OSM                  | valeur du tag WDM |
| ------------------------------- | ----------------- |
| crossing:bollard=no             | No                |
| crossing:bollard=yes/contrasted | Yes               |
| autres conditions               | tag non renseigné |

### VisualGuidanceBands

Le tag WDM VisualGuidanceBands est renseigné uniquement si SitePathLink=Crossing/Ramp.


| conditions OSM                                     | valeur du tag WDM |
| -------------------------------------------------- | ----------------- |
| tactile_paving=no et crossing:markings=no/`absent` | No                |
| tactile_paving=yes/contrasted                      | Yes               |
| crossing:markings~surface/lines                    | Yes               |
| crossing:markings~zebra:double/ladder              | Yes               |
| crossing:markings~ladder:skewed/ladder:paired      | Yes               |
| crossing:markings~dashes/dots/pictograms           | Yes               |
| autres conditions                                  | tag non renseigné |

### AcousticCrossingAids

Le tag WDM AcousticCrossingAids est renseigné uniquement si SitePathLink=Crossing.

Les conditions OSM sont à vérifier à la fois sur le ![way] chemin d'origine ainsi que sur les éventuels ![node] noeuds qui constituent le chemin.


| conditions OSM                      | valeur du tag WDM            |
| ----------------------------------- | ---------------------------- |
| traffic_signals:sound!=no           | Good (voir cas particuliers) |
| crossing:bell!=no                   | Good (voir cas particuliers) |
| traffic_signals:sound=no            | None                         |
| crossing:bell=no                    | None                         |
| autres conditions                   | tag non renseigné            |

**Cas particulier**

Si AcousticCrossingAids=Good, on lui ajoute le tag suivant

* FixMe:AcousticCrossingAids = Vérifier l'état du dispositif d'aide sonore de ce passage piéton

### VibratingCrossingAids

Le tag WDM VibratingCrossingAids est renseigné uniquement si SitePathLink=Crossing.

Les conditions OSM sont à vérifier à la fois sur le ![way] chemin d'origine ainsi que sur les éventuels ![node] noeuds qui constituent le chemin.

| conditions OSM                      | valeur du tag WDM            |
| ----------------------------------- | ---------------------------- |
| traffic_signals:vibration!=no       | Good (voir cas particuliers) |
| traffic_signals:floor_vibration!=no | Good (voir cas particuliers) |
| traffic_signals:vibration=no        | None                         |
| traffic_signals:floor_vibration=no  | None                         |
| autres conditions                   | tag non renseigné            |

**Cas particulier**

Si VibratingCrossingAids=Good, on lui ajoute le tag suivant

* FixMe:VibratingCrossingAids = Vérifier l'état du dispositif délivrant des vibrations de ce passage piéton

### RaisedButtons

Le tag WDM RaisedButtons est renseigné uniquement si SitePathLink=Elevator.

| conditions OSM                                  | valeur du tag WDM |
| ----------------------------------------------- | ----------------- |
| tactile_writing=no                              | None              |
| tactile_writing=yes                             | Raised            |
| tactile_writing:embossed_printed_letters!=no    | Raised            |
| tactile_writing:embossed_printed_letters:fr!=no | Raised            |
| tactile_writing:engraved_printed_letters!=no    | Raised            |
| tactile_writing:braille!=no                     | Braille           |
| tactile_writing:braille:fr!=no                  | Braille           |
| autres conditions                               | tag non renseigné |

### AudioAnnouncements

Le tag WDM AudioAnnouncements est renseigné uniquement si SitePathLink=Elevator.

| conditions OSM       | valeur du tag WDM |
| -------------------- | ----------------- |
| speech_output=no     | No                |
| speech_output!=no    | Yes               |
| speech_output:fr!=no | Yes               |
| autres conditions    | tag non renseigné |

### PublicCode

Le tag WDM PublicCode est renseigné uniquement si SitePathLink=Elevator.

Le tag est construit avec la valeur du tag OSM ref ou à défaut local_ref.

### Level

Le tag Level est construit avec la valeur du tag OSM level.

### MagneticInductionLoop

Le tag WDM MagneticInductionLoop est renseigné uniquement si SitePathLink=Elevator.

Le tag est construit avec la valeur du tag OSM audio_loop ou à défaut  hearing_loop, en camelCase.

### MirrorOnOppositeSide

Le tag WDM MirrorOnOppositeSide est renseigné uniquement si SitePathLink=Elevator.

Le tag est construit avec les régles suivantes :

| conditions OSM      | valeur du tag WDM |
| ------------------- | ----------------- |
| elevator:mirror=no  | No                |
| elevator:mirror!=no | Yes               |
| mirror=no           | No                |
| mirror!=no          | Yes               |

### AutomaticDoor

Le tag WDM AutomaticDoor est renseigné uniquement si SitePathLink=Elevator.

Si le ![way] chemin d'origine comporte un ![node] noeud avec automatic_door=no, alors le tag WDM aura la valeur No. Dans le cas contraire, le tag prendra la valeur Yes.

### Handrail

| conditions OSM                         | valeur du tag WDM |
| -------------------------------------- | ----------------- |
| handrail=no                            | None              |
| handrail=yes/both                      | Both              |
| handrail=left ou handrail:left!=no     | Left              |
| handrail=right ou handrail:right!=no   | Right             |
| handrail=center ou handrail:center!=no | Center            |
| autres conditions                      | tag non renseigné |

### Handrail:TactileWriting

Le tag WDM Handrail:TactileWriting est renseigné uniquement si SitePathLink=Stairs/Ramp/Elevator.

| conditions OSM                                  | valeur du tag WDM |
| ----------------------------------------------- | ----------------- |
| tactile_writing=no                              | No                |
| tactile_writing=yes                             | Yes               |
| tactile_writing:embossed_printed_letters!=no    | Yes               |
| tactile_writing:embossed_printed_letters:fr!=no | Yes               |
| tactile_writing:engraved_printed_letters!=no    | Yes               |
| tactile_writing:braille!=no                     | Yes               |
| tactile_writing:braille:fr!=no                  | Yes               |
| autres conditions                               | tag non renseigné |

### StepCount

Le tag WDM StepCount est renseigné uniquement si SitePathLink=Stairs.

Il est rempli avec la valeur du tag step_count (ou à défaut est_step_count) de l'objet OSM initial. Si la valeur n'est pas un entier positif, le tag n'est pas renseigné.

### VisualContrast

Le tag WDM VisualContrast est renseigné uniquement si SitePathLink=Stairs. Il est ajouté sur les ![node] noeuds PathJunction=Stairs de début et de fin du SitePathLink.

Le tag est construit à partir du tag OSM step:contrast :

| valeurs du tag OSM | valeur du tag WDM |
| ------------------ | ----------------- |
| no                 | No                |
| yes                | Yes               |
| bad                | Yes               |
| autre valeur       | tag non renseigné |

### StepHeight

Le tag WDM StepHeight est renseigné uniquement si SitePathLink=Stairs.

Il est rempli avec la valeur du tag step:height de l'objet OSM initial, avec les mêmes règles de conversion que Width.

### StepLength

Le tag WDM StepLength est renseigné uniquement si SitePathLink=Stairs.

Il est rempli avec la valeur du tag step:length de l'objet OSM initial, avec les mêmes règles de conversion que Width.

### StepCondition

Le tag WDM StepCondition est renseigné uniquement si SitePathLink=Stairs.

Il est rempli avec la valeur du tag OSM step:condition de l'objet OSM initial, avec les règles de gestion suivantes :

* Even si step:condition=even
* Uneven si step:condition=uneven
* Rough si step:condition=rough

### StairRamp

Le tag WDM StairRamp est renseigné uniquement si SitePathLink=Stairs.

Il est renseigné avec les règles de gestion suivantes :

* None si ramp=no/separate
* Bicycle si ramp:bicycle!=no
* Luggage si ramp:luggage!=no
* Stroller si ramp:stroller!=no
* Other si ramp=yes et aucun des tags suivants ne sont renseignés : ramp:bicycle, ramp:luggage et ramp:stroller

### Conveying

Le tag WDM Conveying est renseigné uniquement si SitePathLink=Escalator/Travelator.

Il est construit à partir du tag OSM conveying :
| valeurs du tag OSM | valeur du tag WDM |
| ------------------ | ----------------- |
| reversible         | Reversible        |
| backward           | Backward          |
| autre valeur       | Forward           |

### MaxWeight

Le tag WDM MaxWeight est construit à partir de la valeur du tag maxweight de l'objet OSM initial.

| conditions OSM                                                        | valeur du tag WDM                                       |
| --------------------------------------------------------------------- | ------------------------------------------------------- |
| maxweight est un nombre décimal positif                               | valeur de maxweight multipliée par 1000                 |
| maxweight est un nombre décimal positif, suivi d'un espace et de "t"  | valeur de maxweight sans l'unité et multipliée par 1000 |
| maxweight est un nombre décimal positif, suivi d'un espace et de "kg" | valeur de maxweight sans l'unité                        |
| autres conditions                                                     | tag non renseigné                                       |

# Lecture des places de stationnement

La lecture des places de stationnement n'est réalisée que lorsque le paramètrage implique d'importer les cheminements.

Les objets OSM avec amenity=parking_space sont susceptibles d'être convertis en ![node] noeud places de stationnement avec ParkingBay=*.

| Conditions OSM                                      | Attribut WDM        |
| --------------------------------------------------- | ------------------- |
| amenity=parking_space et parking_space=disabled     | ParkingBay=Disabled |
| amenity=parking_space et capacity:disabled!=0       | ParkingBay=Disabled |
| amenity=parking_space et access:disabled=designated | ParkingBay=Disabled |
| amenity=parking_space et wheelchair!=no             | ParkingBay=Disabled |

Si l'objet OSM est un ![node] noeud, la géométrie sera conservée. Sinon, le barycentre ou à défaut le premier ![node] noeud sera utilisé pour construire la géométrie de l'objet WDM.

## Conversion des attributs

### FlooringMaterial

Le tag WDM FlooringMaterial est construit à partir de la valeur du tag surface de l'objet OSM initial, en camelCase, avec les mêmes règles de conversion que pour les SitePathLink.

### Flooring

Le tag WDM Flooring est construit à partir de la valeur du tag smoothness de l'objet OSM initial avec les mêmes règles de conversion que pour les SitePathLink.

### Lighting

Le tag WDM Lighting est rempli avec la valeur du tag OSM lit ou lit:perceived avec les mêmes règles de conversions que pour les SitePathLink.

### Slope

Le tag WDM Slope est construit à partir de la valeur du tag incline de l'objet OSM initial avec les mêmes règles de conversion que pour les SitePathLink.

### Tilt, TiltSide

Les tag WDM Tilt et TiltSide sont construits à partir de la valeur du tag incline:across de l'objet OSM initial avec les mêmes règles de conversion que pour les SitePathLink.

### BayGeometry

Le tag WDM BayGeometry est construit à partir de la valeur du tag orientation de l'objet OSM initial:

* Orthogonal si orientation=perpendicular
* Angled si orientation=diagonal
* Parallel si orientation=parallel
* non renseigné sinon

### ParkingVisibility

Le tag WDM ParkingVisibility est construit à partir de la valeur du tag markings de l'objet OSM initial:

* Unmarked si markings=no
* non renseigné sinon

### VehicleRecharging

L'objet WDM aura VehicleRecharging=Yes si le tag OSM capacity:charging!=0.

Sinon, VehicleRecharging vaudra No.

### Length

| conditions OSM                                                                                                | valeur du tag WDM                                                                                  |
| ------------------------------------------------------------------------------------------------------------- | -------------------------------------------------------------------------------------------------- |
| le tag length est renseigné                                                                                   | valeur du tag length, avec les mêmes règles de conversions que pour les SitePathLink               |
| le tag parking_space:length est renseigné                                                                     | valeur du tag parking_space:length, avec les mêmes règles de conversions que pour les SitePathLink |
| l'objet OSM initial est une ![area] zone et capacity=1/`non renseigné` ou capacity:disabled=1/`non renseigné` | longueur en mètre arrondi au cm de la plus grande longueur                                         |
| autres conditions                                                                                             | tag non renseigné                                                                                  |


### Width

| conditions OSM                                                                                                | valeur du tag WDM                                                                                 |
| ------------------------------------------------------------------------------------------------------------- | ------------------------------------------------------------------------------------------------- |
| le tag width est renseigné                                                                                    | valeur du tag width, avec les mêmes règles de conversions que pour les SitePathLink               |
| le tag parking_space:width est renseigné                                                                      | valeur du tag parking_space:width, avec les mêmes règles de conversions que pour les SitePathLink |
| l'objet OSM initial est une ![area] zone et capacity=1/`non renseigné` ou capacity:disabled=1/`non renseigné` | longueur en mètre arrondi au cm de la plus petite longueur                                        |
| autres conditions                                                                                             | tag non renseigné                                                                                 |

# Lecture des obstacles et équipements

La lecture des obstacles et équipements n'est réalisée que lorsque le paramètrage implique d'importer les cheminements.

## Lecture des ressauts

Les ![node] noeuds OSM avec kerb=raised faisant partie d'un ![way] chemin sont transformés en Obstacle=Kerb + ObstacleType=Surface sauf dans les cas suivants :

* si le noeud a aussi highway=crossing
* si le noeud fait partie d'un chemin footway=crossing ou path=crossing

Remarque : le tag Heigth n'est jamais ajouté sur ce type d'objet.

## Lecture des sanitaires

Les ![node] noeuds et ![area] zone OSM avec amenity=toilets/shower sont transformés en Amenity=Toilets.

Dans le cas d'une ![area] zone, le barycentre ou le premier ![node] noeud est utilisé pour construire la géométrie. NB : si l'objet OSM comporte aussi un tag indoor=room, une ![area] zone WDM sera également crée pour représenter la pièce intérieure où sont situés les sanitaires.

Si le ![node] noeud WDM fait partie ou est situé à moins de 1,40 mètre d'un ![way] chemin SitePathLink, alors les tags suivants sont également ajoutés :

* Obstacle=Toilets
* ObstacleType=Ground

## Lecture des abris

Les ![node] noeuds et ![area] zone OSM avec amenity=shelter sont transformés en Amenity=Shelter.

Dans le cas d'une ![area] zone, le barycentre ou le premier ![node] noeud est utilisé pour construire la géométrie.

Si le ![node] noeud WDM fait partie ou est situé à moins de 1,40 mètre d'un ![way] chemin SitePathLink, alors le tag Obstacle=Shelter est également ajouté.

## Lecture des téléphones d'urgence

Les ![node] noeuds OSM avec emergency=phone sont transformés en Amenity=EmergencyPhone.

Si le ![node] noeud fait partie ou est situé à moins de 1,40 mètre d'un ![way] chemin SitePathLink, alors les tags suivants sont également ajoutés :

* Obstacle=Yes
* ObstacleType=Ground

## Lecture des guichets d'ERP

Les ![node] noeuds et ![area] zone OSM avec les attributs suivants sont transformés en Amenity=ReceptionDesk :

* amenity=reception_desk
* tourism=information et information=office/visitor_centre

Dans le cas d'une ![area] zone, le barycentre ou le premier ![node] noeud est utilisé pour construire la géométrie.

Remarque : si l'objet est un guichet de transport (shop=tickets et tickets:public_transport), c'est un objet Amenity=TicketOffice qui est créé à la place.

## Lecture des équipements de signalétique

Les ![node] noeuds OSM avec information!=office/visitor_centre sont transformés en Amenity=Sign.

Si le ![node] noeud fait partie ou est situé à moins de 1,40 mètre d'un ![way] chemin SitePathLink, alors les tags suivants sont également ajoutés :

* Obstacle=Yes
* ObstacleType=Ground

## Lecture des boîtes aux lettres

Les ![node] noeuds OSM avec amenity=post_box sont transformés en Amenity=PostBox.

Si le ![node] noeud fait partie ou est situé à moins de 1,40 mètre d'un ![way] chemin SitePathLink, alors les tags suivants sont également ajoutés :

* Obstacle=PostBox
* ObstacleType=Ground

Seuls sont conservés ceux qui font partis ou sont situés à moins de 1,40 mètre d'un ![way] chemin SitePathLink.

## Lecture des poteaux

Les ![node] noeuds OSM avec man_made=utility_pole ou power=pole sont transformés en objet WDM avec les attributs suivants :

* Obstacle=Pole
* ObstacleType=Ground

Seuls sont conservés ceux qui font partis ou sont situés à moins de 1,40 mètre d'un ![way] chemin SitePathLink.

## Lecture des DAE

Les ![node] noeuds OSM avec emergency=defibrillator sont transformés en Amenity=Defibrillator.

## Lecture des bancs

Les ![node] noeuds OSM avec amenity=bench sont transformés en Amenity=Seating.

Si le ![node] noeud fait partie ou est situé à moins de 1,40 mètre d'un ![way] chemin SitePathLink, alors les tags suivants sont également ajoutés :

* Obstacle=Bench
* ObstacleType=Ground

## Lecture des poubelles

Les ![node] noeuds OSM avec amenity=waste_basket sont transformés en Amenity=RubbishDisposal.

Si le ![node] noeud fait partie ou est situé à moins de 1,40 mètre d'un ![way] chemin SitePathLink, alors les tags suivants sont également ajoutés :

* Obstacle=Yes
* ObstacleType=Ground

## Lecture des abris à chariots

Les ![node] noeuds et ![area] zone OSM avec amenity=trolley_bay sont transformés en Amenity=TrolleyStand.

Dans le cas d'une ![area] zone, le barycentre ou le premier ![node] noeud est utilisé pour construire la géométrie.

Si l'objet initial était un ![node] noeud et que le ![node] noeud WDM fait partie ou est situé à moins de 1,40 mètre d'un ![way] chemin SitePathLink, alors le tag Obstacle=Yes est également ajouté.

## Lecture des équipements spécifiques aux gares

### TicketOffice

Les ![node] noeuds et ![area] zone OSM avec shop=ticket et tickets:public_transport sont transformés en Amenity=TicketOffice.

Dans le cas d'une ![area] zone, le barycentre ou le premier ![node] noeud est utilisé pour construire la géométrie.

De plus :

* aucun objet PointOfInterest=Shop n'est créé dans ce cas
* si l'objet OSM a aussi amenity=reception_desk, aucun objet WDM ReceptionDesk n'est créé
* en revanche, si l'objet OSM a aussi indoor=room, l'objet WDM InsideSpace=Room est bien créé

### TicketValidator

Les ![node] noeuds OSM avec barrier=turnstile ou amenity=ticket_validator  sont transformés en Amenity=TicketValidator.

Si le ![node] noeud a barrier=turnstile, fait partie ou est situé à moins de 1,40 mètre d'un ![way] chemin SitePathLink, alors les tags suivants sont également ajoutés :

* Obstacle=Turnstile
* ObstacleType=Pass

### LuggageLocker

Les ![node] noeuds et ![area] zone OSM avec amenity=luggage_locker sont transformés en Amenity=LuggageLocker.

Dans le cas d'une ![area] zone, le barycentre ou le premier ![node] noeud est utilisé pour construire la géométrie.

Si l'objet initial était un ![node] noeud et que le ![node] noeud WDM fait partie ou est situé à moins de 1,40 mètre d'un ![way] chemin SitePathLink, alors le tag Obstacle=Yes est également ajouté.

### TicketVendingMachine

Les ![node] noeuds OSM avec vending=public_transport_tickets et amenity=vending_machine sont transformés en Amenity=TicketVendingMachine.

Si le ![node] noeud fait partie ou est situé à moins de 1,40 mètre d'un ![way] chemin SitePathLink, alors les tags suivants sont également ajoutés :

* Obstacle=Yes
* ObstacleType=Ground

### MeetingPoint

Les ![node] noeuds OSM avec amenity=meeting_point sont transformés en Amenity=MeetingPoint.

Si le ![node] noeud fait partie ou est situé à moins de 1,40 mètre d'un ![way] chemin SitePathLink, alors les tags suivants sont également ajoutés :

* Obstacle=Yes
* ObstacleType=Ground

## Lecture des autres équipements et obstacles

Pour tous les cas ci-dessous :

* on considère uniquement les ![node] noeuds OSM
* seuls sont conservés ceux qui ont un tag obstacle:wheelchair=yes ou ceux font partis ou sont situés à moins de 1,40 mètre d'un ![way] chemin SitePathLink.

| conditions OSM                | tag WDM                  | autre tag WDM            |
| ----------------------------- | ------------------------ | ------------------------ |
| natural=tree                  | Obstacle=Vegetation      | ObstacleType=Ground      |
| barrier=planter               | Obstacle=Vegetation      | ObstacleType=Ground      |
| barrier=bollard/block         | Obstacle=Bollard         | ObstacleType=Ground      |
| man_made=street_cabinet       | Obstacle=StreetFurniture | ObstacleType=Ground      |
| man_made=manhole              | Obstacle=DrainHole       | ObstacleType=Ground      |
| man_made=planter              | Obstacle=Planter         | ObstacleType=Ground      |
| barrier=cycle_barrier         | Obstacle=CycleBarrier    | ObstacleType=Pass        |
| barrier=motorcycle_barrier    | Obstacle=CycleBarrier    | ObstacleType=Pass        |
| barrier=full-height_turnstile | Obstacle=Turnstile       | ObstacleType=Pass        |
| barrier=horse_stile           | Obstacle=Turnstile       | ObstacleType=Pass        |
| barrier=kissing_gate          | Obstacle=Turnstile       | ObstacleType=Pass        |
| barrier=stile                 | Obstacle=Turnstile       | ObstacleType=Pass        |
| highway=street_lamp           | Obstacle=Pole            | Electrified=Yes          |
| advertising=billboard         | Obstacle=AdvertisingSupport |                       |
| advertising=column            | Obstacle=AdvertisingSupport |  ObstacleType=Ground  |
| advertising=poster_box        | Obstacle=AdvertisingSupport |  ObstacleType=Ground  |
| recycling_type=container      | Obstacle=RecyclingContainer |  ObstacleType=Ground  |
| barrier=height_restrictor     | Obstacle=Yes             | ObstacleType=Overhanging |
| barrier=bar                   | Obstacle=Yes             | ObstacleType=Ground      |
| obstacle:wheelchair=yes       | Obstacle=Yes             |                          |

## Conversion des attributs

### Length

Le tag Length est rempli avec la valeur du tag OSM length ou à défaut est_length ou spacing avec les mêmes règles de conversion que pour les SitePathLink.

### Width

Le tag Width est rempli avec la valeur du tag OSM width ou à défaut est_width avec les mêmes règles de conversion que pour les SitePathLink.

### RemainingWidth

Le tag Width est rempli avec la valeur du tag OSM maxwidth:physical ou à défaut maxwidth ou opening avec les mêmes règles de conversion que pour les SitePathLink.

### Height

Le tag Height est rempli avec la valeur du tag OSM height ou à défaut est_height ou maxheight avec les mêmes règles de conversion que pour les SitePathLink.

### TactileWarningStrip

Le tag TactileWarningStrip est renseigné à partir du tag OSM tactile_paving avec les conditions suivantes :

| Conditions OSM                | Valeur du tag WDM            |
| ----------------------------- | ---------------------------- |
| tactile_paving=no             | None                         |
| tactile_paving=yes/contrasted | Good (voir cas particuliers) |
| autres conditions             | attribut non renseigné       |

**Cas particuliers** : Si TactileWarningStrip=Good, on lui ajoute le tag FixMe:TactileWarningStrip="Vérifier l’état du marquage de la borne d'éveil à vigilance"

### KerbDesign

| conditions OSM    | valeur du tag WDM               |
| ----------------- | ------------------------------- |
| kerb=raised       | Raised                          |
| kerb=lowered      | Lowered (voir cas particuliers) |
| kerb=flush        | Flush                         |
| kerb=rolled       | Raised                          |
| kerb=no           | None                            |
| autres conditions | tag non renseigné               |

**Cas particuliers** : lorsque KerbDesign=Lowered, on ajoute un tag FixMe:KerbDesign="Vérifier s'il s'agit d'un ressaut abaissé ou d'un bateau".

### KerbHeight

| conditions OSM    | valeur du tag WDM                                                           |
| ----------------- | --------------------------------------------------------------------------- |
| kerb:height=*     | valeur du tag avec les mêmes règles de conversion que pour les SitePathLink |
| height=*          | valeur du tag avec les mêmes règles de conversion que pour les SitePathLink |
| est_height=*      | valeur du tag avec les mêmes règles de conversion que pour les SitePathLink |
| kerb=raised       | 0.05    (voir cas particuliers)                                             |
| kerb=lowered      | 0.03    (voir cas particuliers)                                             |
| kerb=flush        | 0.01    (voir cas particuliers)                                             |
| autres conditions | tag non renseigné                                                           |

**Cas particuliers** : lorsque la valeur du tag WDM a été déduite du tag OSM kerb, on ajoute un tag FixMe:KerbHeight="Vérifier la hauteur du ressaut".

### Altitude

Le tag Altitude est renseigné à partir du tag OSM ele.

### PublicCode

Le tag PublicCode est renseigné à partir du tag OSM ref, ou à défaut local_ref.

### FreeToUse

Le tag WDM FreeToUse est rempli avec les règles suivantes :

* No si fee!=no
* No si charge=*
* Yes si fee=no

### WheelchairAccess

Le tag WDM WheelchairAccess est construit à partir de la valeur du tag wheelchair de l'objet OSM initial avec les mêmes règles de conversion que pour les SitePathLink.

**Cas particulier** : pour les Amenity=Toilets, on utilisera le tag toilets:wheelchair à défaut.

### Toilets:BabyChange

Le tag WDM Toilets:BabyChange est rempli avec les règles suivantes :

* Yes si changing_table=yes et changing_table:wheelchair=yes
* Bad si changing_table=yes
* No si changing_table=no

### Toilets:Shower

Le tag WDM Toilets:Shower est rempli avec les règles suivantes :

* Yes si shower!=no
* No si shower=no

### Toilets:HandWashing

Le tag WDM Toilets:HandWashing est rempli avec les règles suivantes :

* Yes si toilets:handwashing=yes
* No si toilets:handwashing=no

### Toilets:DrinkingWater

Le tag WDM Toilets:DrinkingWater est rempli avec les règles suivantes :

* Yes si drinking_water=yes
* No si drinking_water=no

### Toilets:Position

Le tag WDM Toilets:Position est rempli avec les valeurs suivantes, dans cet ordre et séparées par un ";" :

* Seated si toilets:position~seated
* Urinal si toilets:position~urinal
* Squat si toilets:position~squat

Exemple : toilets:position=urinal;seated donnera Toilets:Position=Seated;Urinal

### SeatCount

Le tag WDM SeatCount est renseigné avec la valeur du tag seats ou à défaut capacity:seats.

### BackRest

Le tag WDM BackRest est renseigné avec la valeur du tag backrest :

* Yes si backrest=yes
* No si backrest=no
* non renseigné sinon

### Staffing

Le tag WDM Staffing est rempli avec les règles suivantes :

* No si supervised=no
* FullTime si supervised=yes
* PartTime si supervised!=yes/no

### MagneticInductionLoop

Le tag WDM MagneticInductionLoop est renseigné avec la valeur du tag OSM audio_loop ou à défaut hearing_loop, en camelCase.

# Lecture des ERP

La lecture des ERP n’est réalisée que lorsque le paramètrage implique d’importer les ERP.

## Lecture des ERP pour la pratique des sports

Les ![node] noeuds et ![area] zones OSM avec les attributs suivants sont susceptibles d'être convertis en PointOfInterest=Sport.

| conditions OSM                   | tag WDM                 | autre tag WDM         |
| -------------------------------- | ----------------------- | --------------------- |
| sport=climbing                   | Sport=ClimbingCenter    | PointOfInterest=Sport |
| sport=swimming                   | Sport=SwimmingCenter    | PointOfInterest=Sport |
| leisure=sports_centre/sports_hall| Sport=SportCenter       | PointOfInterest=Sport |
| leisure=fitness_centre           | Sport=FitnessCenter     | PointOfInterest=Sport |
| leisure=ice_rink                 | Sport=IceCenter         | PointOfInterest=Sport |
| leisure=stadium                  | Sport=Stadium           | PointOfInterest=Sport |
| leisure=horse_riding             | Sport=HorseRidingCenter | PointOfInterest=Sport |
| leisure=bowling_alley            | Sport=BowlingCenter     | PointOfInterest=Sport |
| leisure=dance                    | Sport=DancingCenter     | PointOfInterest=Sport |
| leisure=golf_course              | Sport=GolfingCenter     | PointOfInterest=Sport |

## Lecture des ERP de divertissement

Les ![node] noeuds et ![area] zones OSM avec les attributs suivants sont susceptibles d'être convertis en PointOfInterest=Game.

| conditions OSM              | tag WDM                | autre tag WDM        |
| --------------------------- | ---------------------- | -------------------- |
| leisure=adult_gaming_centre | Game=AdultGamingCenter | PointOfInterest=Game |
| amenity=casino              | Game=AdultGamingCenter | PointOfInterest=Game |
| leisure=amusement_arcade    | Game=ArcadeCenter      | PointOfInterest=Game |
| leisure=escape_game         | Game=EscapeRoom        | PointOfInterest=Game |
| leisure=hackerspace         | Game=Hackerspace       | PointOfInterest=Game |
| leisure=resort              | Game=Resort            | PointOfInterest=Game |
| tourism=theme_park          | Game=ThemePark         | PointOfInterest=Game |

## Lecture des ERP liés à l'éducation

Les ![node] noeuds et ![area] zones OSM avec les attributs suivants sont susceptibles d'être convertis en PointOfInterest=Education.

| conditions OSM           | tag WDM                    | autre tag WDM             |
| ------------------------ | -------------------------- | ------------------------- |
| school:FR=maternelle     | Education=EarlyChildhood   | PointOfInterest=Education |
| school:FR=élémentaire    | Education=Elementary       | PointOfInterest=Education |
| school:FR=primaire       | Education=Primary          | PointOfInterest=Education |
| school:FR=collège        | Education=MiddleSchool     | PointOfInterest=Education |
| school:FR=lycée          | Education=HighSchool       | PointOfInterest=Education |
| school:FR=secondaire     | Education=Secondary        | PointOfInterest=Education |
| amenity=university       | Education=University       | PointOfInterest=Education |
| amenity=college          | Education=College          | PointOfInterest=Education |
| amenity=kindergaten      | Education=Kindergarten     | PointOfInterest=Education |
| amenity=school           | PointOfInterest=Education  |                           |


## Lecture des lieux de culte

Les ![node] noeuds et ![area] zones OSM avec les attributs suivants sont susceptibles d'être convertis en PointOfInterest=Religion.

| conditions OSM           | tag WDM                  | autre tag WDM            |
| ------------------------ | ------------------------ | ------------------------ |
| religion=buddhist        | Religion=Buddhist        | PointOfInterest=Religion |
| religion=christian       | Religion=Christian       | PointOfInterest=Religion |
| religion=hindu           | Religion=Hindu           | PointOfInterest=Religion |
| religion=jewish          | Religion=Jewish          | PointOfInterest=Religion |
| religion=muslim          | Religion=Muslim          | PointOfInterest=Religion |
| religion=shinto          | Religion=Shinto          | PointOfInterest=Religion |
| religion=sikh            | Religion=Sikh            | PointOfInterest=Religion |
| religion=taoist          | Religion=Taoist          | PointOfInterest=Religion |
| amenity=place_of_worship | PointOfInterest=Religion |                          |

## Lecture des ERP liés à la culture

Les ![node] noeuds et ![area] zones OSM avec les attributs suivants sont susceptibles d'être convertis en PointOfInterest=Theater/Museum/Exhibition.

| conditions OSM           | tag WDM                 | autre tag WDM              |
| ------------------------ | ----------------------- | -------------------------- |
| amenity=theatre          | Theater=Theater         | PointOfInterest=Theater    |
| amenity=cinema           | Theater=Cinema          | PointOfInterest=Theater    |
| tourism=museum           | Museum=Museum           | PointOfInterest=Museum     |
| tourism=aquarium         | Museum=Aquarium         | PointOfInterest=Museum     |
| tourism=zoo              | Museum=Zoo              | PointOfInterest=Museum     |
| tourism=arts_centre      | Exhibition=ArtsCentre   | PointOfInterest=Exhibition |
| tourism=gallery          | Exhibition=Gallery      | PointOfInterest=Exhibition |
| amenity=community_centre | Theater=CommunityCentre | PointOfInterest=Theater    |

## Lecture des lieux de restauration

Les ![node] noeuds et ![area] zones OSM avec les attributs suivants sont susceptibles d'être convertis en PointOfInterest=Restaurant.

| conditions OSM     | tag WDM               | autre tag WDM              |
| ------------------ | --------------------- | -------------------------- |
| amenity=pub        | Restaurant=Pub        | PointOfInterest=Restaurant |
| amenity=bar        | Restaurant=Bar        | PointOfInterest=Restaurant |
| amenity=fast_food  | Restaurant=FastFood   | PointOfInterest=Restaurant |
| amenity=cafe       | Restaurant=Cafe       | PointOfInterest=Restaurant |
| amenity=food_court | Restaurant=FoodCourt  | PointOfInterest=Restaurant |
| amenity=ice_cream  | Restaurant=IceCream   | PointOfInterest=Restaurant |
| amenity=restaurant | Restaurant=Restaurant | PointOfInterest=Restaurant |

## Lecture des établissements de santé

Les ![node] noeuds et ![area] zones OSM avec les attributs suivants sont susceptibles d'être convertis en PointOfInterest=Healthcare.

| conditions OSM               | tag WDM               | autre tag WDM              |
| ---------------------------- | --------------------- | -------------------------- |
| healthcare=laboratory        | Healthcare=Laboratory | PointOfInterest=Healthcare |
| healthcare=sample_collection | Healthcare=Laboratory | PointOfInterest=Healthcare |
| amenity=doctors/dentist      | Healthcare=Doctor     | PointOfInterest=Healthcare |
| healthcare=doctor            | Healthcare=Doctor     | PointOfInterest=Healthcare |
| amenity=hospital             | Healthcare=Hospital   | PointOfInterest=Healthcare |
| healthcare=hospital          | Healthcare=Hospital   | PointOfInterest=Healthcare |
| amenity=clinic               | Healthcare=Clinic     | PointOfInterest=Healthcare |
| healthcare=clinic            | Healthcare=Clinic     | PointOfInterest=Healthcare |

Remarque : les pharmacies sont considérées comme des commerces (PointOfInterest=Shop).

## Lecture des services publics

Les ![node] noeuds et ![area] zones OSM avec les attributs suivants sont susceptibles d'être convertis en PointOfInterest=Government.

| conditions OSM                                   | tag WDM                          | autre tag WDM              |
| ------------------------------------------------ | -------------------------------- | -------------------------- |
| amenity=townhall                                 | Government=Townhall              | PointOfInterest=Government |
| government=local_authority                       | Government=LocalAuthority        | PointOfInterest=Government |
| government=parliament                            | Government=Parliament            | PointOfInterest=Government |
| government=prefecture                            | Government=Prefecture            | PointOfInterest=Government |
| government=social_welfare                        | Government=SocialWelfare         | PointOfInterest=Government |
| government=social_security                       | Government=SocialSecurity        | PointOfInterest=Government |
| government=tax                                   | Government=Tax                   | PointOfInterest=Government |
| government=audit                                 | Government=Audit                 | PointOfInterest=Government |
| amenity=police                                   | Government=Police                | PointOfInterest=Government |
| amenity=post_office                              | Government=Post                  | PointOfInterest=Government |
| office=employment_agency (voir cas particuliers) | Government=UnemploymentInsurance | PointOfInterest=Government |
| amenity=courthouse                               | Government=CourtHouse            | PointOfInterest=Government |
| amenity=chamber_of_commerce                      | Government=ChamberOfCommerce     | PointOfInterest=Government |
| government=customs                               | Government=Customs               | PointOfInterest=Government |
| office=diplomatic                                | PointOfInterest=Government       | Government=Diplomatic      |
| office=government                                | PointOfInterest=Government       |                            |

**Cas particuliers**
Les objets OSM office=employment_agency sont convertis en PointOfInterest=Government + Government=UnemploymentInsurance uniquement si une des conditions est remplie :

* brand:wikidata=Q8901192
* brand="Pôle Emploi"
* operator:wikidata=Q8901192
* network:wikidata=Q8901192
* name="Pôle Emploi"

Sinon, ils sont importés en PointOfInterest=Shop + Shop=EmploymentAgency.

#### Tags additionnels pour les services publics

Pour les Government=Parliament uniquement :

| Conditions OSM | Tag WDM             |
| -------------- | ------------------- |
| admin_level=4  | Parliament=State    |
| admin_level=6  | Parliament=District |

Pour les Government=Tax uniquement :
| Conditions OSM             | Tag WDM            |
| -------------------------- | ------------------ |
| operator:wikidata=Q3550086 | Tax=Payroll        |
| brand:wikidata=Q3550086    | Tax=Payroll        |
| network:wikidata=Q3550086  | Tax=Payroll        |
| operator=URSSAF            | Tax=Payroll        |
| autres conditions          | Tax=PersonalIncome |

Pour les Government=Police uniquement :
| Conditions OSM              | Tag WDM         |
| --------------------------- | --------------- |
| police:FR=gendarmerie       | Police=Military |
| police:FR=police            | Police=Civil    |
| police:FR=police_municipale | Police=Local    |
| police:FR=police_rurale     | Police=Rural    |

## Lecture des bibliothèques

Les ![node] noeuds et ![area] zones OSM avec les attributs suivants sont susceptibles d'être convertis en PointOfInterest=Library.

| conditions OSM      | tag WDM       | autre tag WDM           |
| ------------------- | ------------- | ----------------------- |
| amenity=library     | Library=Books | PointOfInterest=Library |
| amenity=toy_library | Library=Games | PointOfInterest=Library |

## Lecture des services d'hébergement spécifique

Les ![node] noeuds et ![area] zones OSM avec les attributs suivants sont susceptibles d'être convertis en PointOfInterest=DisabledCare/Senior.

| conditions OSM                                                   | tag WDM                | autre tag WDM                |
| ---------------------------------------------------------------- | ---------------------- | ---------------------------- |
| social_facility:for=disabled + social_facility = group_home      | Lodging=GroupHome      | PointOfInterest=DisabledCare |
| social_facility:for=disabled + social_facility = assisted_living | Lodging=AssistedLiving | PointOfInterest=DisabledCare |
| social_facility:for=disabled + social_facility = nursing_home    | Lodging=NursingHome    | PointOfInterest=DisabledCare |
| social_facility:for=senior + social_facility = group_home        | Lodging=GroupHome      | PointOfInterest=Senior       |
| social_facility:for=senior + social_facility = assisted_living   | Lodging=AssistedLiving | PointOfInterest=Senior       |
| social_facility:for=senior + social_facility = nursing_home      | Lodging=NursingHome    | PointOfInterest=Senior       |

## Lecture des hôtels

Les ![node] noeuds et ![area] zones OSM avec les attributs suivants sont susceptibles d'être convertis en PointOfInterest=Lodging.

| conditions OSM    | tag WDM           | autre tag WDM           |
| ----------------- | ----------------- | ----------------------- |
| tourism=apartment | Lodging=Apartment | PointOfInterest=Lodging |
| tourism=camp_site | Lodging=CampSite  | PointOfInterest=Lodging |
| tourism=chalet    | Lodging=Chalet    | PointOfInterest=Lodging |
| tourism=hostel    | Lodging=Hostel    | PointOfInterest=Lodging |
| tourism=hotel     | Lodging=Hotel     | PointOfInterest=Lodging |
| tourism=motel     | Lodging=Motel     | PointOfInterest=Lodging |

## Lecture des refuges d'altitude

Les ![node] noeuds et ![area] zones OSM avec les attributs suivants sont susceptibles d'être convertis en PointOfInterest=MountainShelter.

| conditions OSM         | tag WDM                    | autre tag WDM                   |
| ---------------------- | -------------------------- | ------------------------------- |
| tourism=wilderness_hut | MountainShelter=Wilderness | PointOfInterest=MountainShelter |
| tourism=alpine_hut     | MountainShelter=Alpine     | PointOfInterest=MountainShelter |

## Lecture des parcs de stationnement couvert

Les ![node] noeuds et ![area] zones OSM avec amenity=parking + parking=multi-storey sont convertis en PointOfInterest=ParkingLots + ParkingLots=MultiStorey.

## Lecture des banques

Les ![node] noeuds et ![area] zones OSM avec amenity=bank sont convertis en PointOfInterest=Bank.

## Lecture des commerces

Les ![node] noeuds et ![area] zones OSM avec les attributs suivants sont susceptibles d'être convertis en PointOfInterest=Shop.

| conditions OSM                                   | tag WDM                                    | autre tag WDM        |
| ------------------------------------------------ | ------------------------------------------ | -------------------- |
| office=estate_agent                              | Shop=EstateAgent                           | PointOfInterest=Shop |
| office=guide                                     | Shop=Guide                                 | PointOfInterest=Shop |
| office=insurance                                 | Shop=Insurance                             | PointOfInterest=Shop |
| office=moving_company                            | Shop=MovingCompany                         | PointOfInterest=Shop |
| amenity=boat_rental                              | Shop=BoatRental                            | PointOfInterest=Shop |
| amenity=bureau_de_change                         | Shop=BureauDeChange                        | PointOfInterest=Shop |
| amenity=car_rental                               | Shop=CarRental                             | PointOfInterest=Shop |
| amenity=car_wash                                 | Shop=CarWash                               | PointOfInterest=Shop |
| amenity=crematorium                              | Shop=Crematorium                           | PointOfInterest=Shop |
| amenity=driving_school                           | Shop=DrivingSchool                         | PointOfInterest=Shop |
| amenity=fuel                                     | Shop=Fuel                                  | PointOfInterest=Shop |
| amenity=internet_cafe                            | Shop=InternetCafe                          | PointOfInterest=Shop |
| amenity=nightclub                                | Shop=Nightclub                             | PointOfInterest=Shop |
| amenity=pharmacy                                 | Shop=Pharmacy                              | PointOfInterest=Shop |
| amenity=vehicle_inspection                       | Shop=VehicleInspection                     | PointOfInterest=Shop |
| office=employment_agency (voir cas particuliers) | Shop=EmploymentAgency                      | PointOfInterest=Shop |
| shop=ticket (voir cas particuliers)              | Shop=Ticket                                | PointOfInterest=Shop |
| shop=*                                           | Shop= valeur du tag OSM shop, en camelCase | PointOfInterest=Shop |

**Cas particuliers**

Les objets OSM avec office=employment_agency sont transformés en PointOfInterest=Shop uniquement s'ils ne remplissent pas les conditions pour être des Government=UnemploymentInsurance (voir paragraphe services publics).

Les objets OSM avec shop=ticket ne sont transformés en PointOfInterest=Shop que s'ils ne remplissent pas les conditions pour être transformés en Amenity=TicketOffice (voir paragraphe guichet d'ERP).

## Conversion des attributs

Pour tous les objets avec PointOfInterest=* les attributs suivants peuvent être ajoutés

### Lighting

Le tag WDM Lighting est rempli avec la valeur du tag OSM lit ou lit:perceived avec les mêmes règles de conversions que pour les SitePathLink.

### WheelchairAccess

Le tag WDM WheelchairAccess est rempli avec la valeur du tag OSM wheelchair avec les mêmes règles de conversions que pour les SitePathLink.

### AudibleSignals

Le tag WDM AudibleSignals est rempli avec la valeur du tag OSM speech_output avec les mêmes règles de conversions que pour les SitePathLink.

### VisualSigns

Le tag WDM VisualSigns vaut Yes si il y au moins un élément avec tourism=information et information!=office/visitor_center à l'intérieur du point d'intérêt.

### `Ref:FR:SIRET`

Le tag WDM `Ref:FR:SIRET` est rempli avec la valeur du tag OSM `ref:FR:SIRET`.

### Phone

Le tag WDM Phone est rempli avec la valeur du tag OSM contact:phone, ou à défaut phone ou à défaut phone:FR.

### Website

Le tag WDM Phone est rempli avec la valeur du tag OSM contact:website, ou à défaut website.

### WiFi

Le tag WDM WiFi est rempli avec les règles suivantes :

* Free si internet_access=wlan/yes/wifi et internet_access:fee=no
* Free si on peut trouver à l'intérieur un noeud avec internet_access=wlan/yes/wifi et internet_access:fee=no
* Free si wifi=free
* Yes si internet_access=wlan/yes/wifi
* Yes si on peut trouver à l'intérieur un noeud avec internet_access=wlan/yes/wifi
* Yes si wifi=yes
* No si internet_access=no
* No si wifi=no

### AddressLine

Le tag WDM AddressLine est rempli de la manière suivante, par ordre de préférence :

* la concaténation des valeurs des tags addr:housenumber et addr:street, avec un espace pour séparateur
* la concaténation des valeurs des tags contact:housenumber et contact:street, avec un espace pour séparateur
* la concaténation des valeurs du tags addr:housenumber et du tag name de la relation associatedStreet dont l'objet fait partie, avec un espace pour séparateur
* la concaténation des valeurs du tags addr:housenumber et du tag name de la relation street dont l'objet fait partie, avec un espace pour séparateur
* la valeur du tag addr:street
* si aucun des conditions n'est remplie, le tag WDM est absent

### PostCode

Le tag WDM PostCode est rempli avec la valeur du tag OSM addr:postcode ou contact:postcode à défaut.

### Toilets

Le tag WDM Toilets est rempli avec les règles suivantes :

* Yes si toilets:wheelchair=yes
* Bad si toilets=yes
* No si toilets=no

### Toilets:BabyChange

Le tag WDM Toilets:BabyChange est rempli avec les règles suivantes :

* Yes si changing_table=yes et changing_table:wheelchair=yes
* Bad si changing_table=yes
* No si changing_table=no

### Toilets:Shower

Le tag WDM Toilets:Shower est rempli avec les règles suivantes :

* Yes si shower!=no
* No si shower=no

### Staffing

Le tag WDM Staffing est rempli avec les règles suivantes :

* No si supervised=no
* FullTime si supervised=yes
* PartTime si supervised!=yes/no

### NearbyCarPark

Le tag WDM NearbyCarPark=Yes est ajouté si on peut trouver un objet OSM amenity=parking à moins de 100 mètres du lieu.

### NearbyCyclePark

Le tag WDM NearbyCyclePark=Yes est ajouté si on peut trouver un objet OSM amenity=bicycle_parking à moins de 50 mètres du lieu.

### Emergency

Le tag WDM Emergency contient une suite de valeurs séparées par un point-virgule avec les conditions suivantes :

| Conditions OSM                                                                        | valeur ajoutée |
| ------------------------------------------------------------------------------------- | -------------- |
| s'il y au moins un élément avec emergency=phone à l'intérieur du point d'intérêt      | SOSPoint       |
| s'il y au moins un élément avec amenity=fire_station à l'intérieur du point d'intérêt | Fire           |
| s'il y au moins un élément avec amenity=police à l'intérieur du point d'intérêt       | Police         |

# Lecture des espaces intérieurs

La lecture des espaces intérieurs n’est réalisée que lorsque le paramètrage implique d’importer les objets de transport.

Les ![area] zones OSM avec indoor=`*` sont susceptibles d'être convertis en espaces intérieurs WDM avec InsideSpace=`*`.

Les objets suivants sont considérés :

| Conditions OSM  | Attribut WDM         |
| --------------- | -------------------- |
| indoor=room     | InsideSpace=Room     |
| indoor=area     | InsideSpace=Area     |
| indoor=corridor | InsideSpace=Corridor |

Puis, seuls les objets répondants à ces conditions sont effectivement conservés :

* tag access`absent` ou access!=private/no
* objet initial situé à l'intérieur d'un ERP ou constituant lui-même un ERP

# Lecture des portes, entrées et accès

La lecture des portes, entrées et accès n’est réalisée que lorsque le paramètrage implique d’importer les objets de transport ou les ERP. Seuls les Entrance=StopPlace sont importés dans le cas d'un import des objets de transport.

| Conditions OSM                                 | Attribut WDM         |
| ---------------------------------------------- | -------------------- |
| railway=subway_entrance/train_station entrance | Entrance=StopPlace   |
| indoor=door                                    | Entrance=InsideSpace |
| entrance=*                                     | Entrance=Building    |
| door!=no                                       | Entrance=Building    |

Seuls les objets avec access `absent` ou access!=no/private sont conservés.

S'il s'agit d'un ![node] noeud, la géométrie est conservée. Sinon, seul le premier ![node] noeud de l'objet est utilisé.

Si l'objet a Entrance=Building et est situé sur le ![area] contour d'un ERP avec PointOfInterest!=StopPlace, alors Entrance=PointOfInterest.

Si l'objet a Entrance=Building et est situé sur le ![area] contour d'un espace intérieur avec InsideSpace!=Quay, alors Entrance=InsideSpace.

## Conversion des attributs

### EntrancePassage

| Conditions OSM    | valeur du tag WDM |
| ----------------- | ----------------- |
| door!=no          | Door              |
| barrier=gate      | Gate              |
| barrier!=gate     | Barrier           |
| door=no           | Opening           |
| autres conditions | tag non renseigné |

### IsEntry

| Conditions OSM                                                                                | valeur du tag WDM |
| --------------------------------------------------------------------------------------------- | ----------------- |
| entrance=exit/emergency                                                                       | No                |
| objet OSM membre d'une ![relation] relation public_transport=stop_area avec le rôle exit_only | No                |
| autres conditions                                                                             | Yes               |

### IsExit

| Conditions OSM                                                                                 | valeur du tag WDM |
| ---------------------------------------------------------------------------------------------- | ----------------- |
| entrance=entrance                                                                              | No                |
| objet OSM membre d'une ![relation] relation public_transport=stop_area avec le rôle entry_only | No                |
| autres conditions                                                                              | Yes               |

### PublicCode

Le tag WDM PublicCode est rempli avec la valeur du tag ref de l'objet OSM initial.

### Width

Le tag Width est rempli avec la valeur du tag width, ou à défaut door:width, wheelchair:entrance_width ou est_width avec les mêmes règles de conversions que pour les SitePathLink.

### Height

Si le tag OSM height est présent, on utilise sa valeur avec les mêmes règles de conversions que pour le tag width des SitePathLink. Sinon, le tag n'est pas renseigné.

### KerbHeight

Si le tag OSM kerb:height ou wheelchair:step_height est présent, on utilise sa valeur avec les mêmes règles de conversions que pour le tag width des SitePathLink. Sinon, le tag n'est pas renseigné.

### TactileWarningStrip

Le tag WDM TactileWarningStrip est rempli avec la valeur du tag OSM tactile_paving avec les mêmes règles de conversions que pour les obstacles et équipements.

### WheelchairAccess

Le tag WDM WheelchairAccess est rempli avec la valeur du tag OSM wheelchair avec les mêmes règles de conversions que pour les SitePathLink.

### AddressLine

Le tag WDM AddressLine est rempli de la manière suivante, par ordre de préférence :

* la concaténation des valeurs des tags addr:housenumber et addr:street, avec un espace pour séparateur
* la concaténation des valeurs des tags contact:housenumber et contact:street, avec un espace pour séparateur
* la concaténation des valeurs du tags addr:housenumber et du tag name de la relation associatedStreet dont l'objet fait partie, avec un espace pour séparateur
* la concaténation des valeurs du tags addr:housenumber et du tag name de la relation street dont l'objet fait partie, avec un espace pour séparateur
* la valeur du tag addr:street
* si aucun des conditions n'est remplie, le tag WDM est absent

### PostCode

Le tag WDM PostCode est rempli avec la valeur du tag OSM addr:postcode ou contact:postcode à défaut.

### AutomaticDoor

Le tag AutomaticDoor est construit avec la valeur du tag OSM automatic_door :

| Valeur OSM        | valeur WDM        |
| ----------------- | ----------------- |
| no                | No                |
| slowdown_button   | Yes               |
| continuous        | Yes               |
| floor             | Yes               |
| motion            | Yes               |
| yes               | Yes               |
| autres conditions | tag non renseigné |

### Door

Le tag Door est construit avec la valeur du tag OSM door :

| Valeur OSM     | valeur WDM        |
| -------------- | ----------------- |
| no             | None              |
| hinged         | Hinged            |
| sliding        | Sliding           |
| swinging       | Swing             |
| revolving      | Revolving         |
| yes            | tag non renseigné |
| autres valeurs | Other             |

### DoorHandle:Inside

Le tag DoorHandle:Inside est construit avec la valeur du tag OSM door:handle:inside ou à défaut door:handle, en camelCase.

Si la valeur WDM ne fait pas partie de la liste ci-dessous, alors le tag prend la valeur "Other".

Valeurs acceptées :
<br> - Knob (pommeau)<br> - Lever (poignée à levier)<br> - CrashBar (barre antipanique) <br>- Button (bouton) <br> - Landing (poignée palière) <br> - GrabRail (poignée de tirage) <br> - WindowLever (levier de fenêtre) <br> - Vertical (bâton de maréchal, barre verticale)

### DoorHandle:Outside

Le tag DoorHandle:Outside est construit avec la valeur du tag OSM door:handle:outside ou à défaut door:handle, en camelCase.

Si la valeur WDM ne fait pas partie de la liste ci-dessous, alors le tag prend la valeur "Other".

Valeurs acceptées :
<br> - Knob (pommeau)<br> - Lever (poignée à levier)<br> - CrashBar (barre antipanique) <br>- Button (bouton) <br> - Landing (poignée palière) <br> - GrabRail (poignée de tirage) <br> - WindowLever (levier de fenêtre) <br> - Vertical (bâton de maréchal, barre verticale)

### GlassDoor

| Conditions OSM                                   | valeur du tag WDM          |
| ------------------------------------------------ | -------------------------- |
| material!=glass                                  | No                         |
| material=glass et entrance:coloured_band!=no     | Yes                        |
| material=glass et entrance:coloured_band=no      | Bad                        |
| material=glass et entrance:coloured_band absent  | Yes (voir cas particulier) |

Cas particulier : si material=glass et entrance:coloured_band absent, on ajoute FixMe:GlassDoor = Vérifier la présence de marquage ou d'autre système de repérage des parois transparentes

# Construction des cheminements d'accès

La construction des cheminements d'accès n'a lieu que pour les objets créés par l'import et lorsque le paramétrage implique d'importer les cheminements.

Pour chaque ![node] point WDM ParkingBay=* non relié à un SitePathLink :

* on projette le ParkingBay sur le SitePathLink le plus proche
* s'il est situé à moins de 5 mètres, un nouveau ![way] chemin SitePathLink est créé.
* Il prend les mêmes attributs que le SitePathLink sur lequel on a projeté

Pour chaque ![node] point WDM Entrance=* non relié à un SitePathLink :

* on projette l'objet Entrance sur le SitePathLink le plus proche
* s'il est situé à moins de 5 mètres, un nouveau ![way] chemin SitePathLink est créé.
* Il prend les mêmes attributs que le SitePathLink sur lequel on a projeté

Pour chaque ![node] point WDM Quay=* non relié à un SitePathLink :

* on projette le Quay sur le SitePathLink le plus proche
* s'il est situé à moins de 5 mètres, un nouveau ![way] chemin SitePathLink est créé.
* Il prend les mêmes attributs que le SitePathLink sur lequel on a projeté
