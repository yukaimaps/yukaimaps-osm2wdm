#
# Converts OpenStreetMap dataset into WDM
#

from lxml import etree
from lxml.builder import E
from math import tan, radians
from . import model, mappings, geometry, tags
import traceback
import logging
import re
from copy import deepcopy
from shapely import STRtree, Polygon, point_on_surface, distance, within # type: ignore
from dataclasses import dataclass
from typing import Dict, List, Any, Optional
from collections import OrderedDict


logging.basicConfig(level=logging.INFO)
logger_rw_xml = logging.getLogger('xml')
logger_osm = logging.getLogger('osm')
logger_wdm = logging.getLogger('wdm')


def readXMLFile(inputPath: str):
	"""Parses XML file

	Parameters
	----------
	inputPath : str
		The file path of the XML to read

	Returns
	-------
	lxml.etree._Element
		The read XML tree root
	"""

	logger_rw_xml.info('Reading file '+str(inputPath))
	with open(inputPath, 'r') as inputFile:
		xml = etree.parse(inputFile)
		return xml.getroot()


def writeXMLFile(xml: etree._Element, outputPath: str):
	"""Saves XML Element into a text file

	Parameters
	----------
	xml: lxml.etree._Element
		The XML root Element to save
	outputPath: str
		Path to file to write on filesystem
	"""

	logger_rw_xml.info('Writing file '+str(outputPath))
	et = etree.ElementTree(xml)
	et.write(outputPath, pretty_print=True)


def getOsmiumFilter():
	"""Get the string to pass to Osmium CLI for filtering OSM data"""

	res = []

	for k in sorted(mappings.USED_OSM_TAGS):
		v = mappings.USED_OSM_TAGS[k]
		e = "nwr/" + k
		if v != "*":
			e += "=" + ",".join(sorted(list(v)))
		res.append(e)

	return " ".join(res)


def transformOsmIntoWDM(osm: etree._Element, withData: list[str] = ["transport","poi","path", "road-with-sidewalk"]):
	"""Parses a whole OSM XML Element and returns WDM XML equivalent

	Parameters
	----------
	osm: lxml.etree._Element
		The OSM XML root element
	withData : list[str]
		List of wanted result features (values: transport, poi, path, road-with-sidewalk)

	Returns
	-------
	lxml.etree._Element
		The WDM equivalent
	"""

	logger_wdm.info("Starting conversion...")

	# Check expected data values
	for d in withData:
		if d not in ["transport","poi","path","road-with-sidewalk"]:
			raise Exception("Unexpected value for withData parameter: "+d)

	# Create result WDM
	ods = model.OsmDataSet(xml=osm)
	wds = model.WalkDataSet(xml=E.osm(version="0.6", generator="Yukaimaps Osm2Wdm"), ods=ods)

	def skipEntrance(osmFeature, wds):
		return wds.hasOsmFeatureImported(osmFeature) or wds.ods.getTag(osmFeature, "access") in ["no", "private"]


	osmFeatures: List[etree._Element]
	progress: model.Progress

	########################################
	# Entrances
	#

	# Read railway entrances
	if "transport" in withData or "poi" in withData:
		logger_wdm.info('Processing railway entrances')
		osmFeatures = (
			ods.search("railway", "subway_entrance")
			+ ods.search("railway", "train_station_entrance")
		)
		logger_wdm.debug(f"Found {len(osmFeatures)} railway entrances")
		progress = model.Progress(logger_wdm, "Converting", len(osmFeatures))

		for osmFeature in osmFeatures:
			progress.tick()
			if skipEntrance(osmFeature, wds):
				continue
			logger_osm.debug(f"Entrance found ({osmFeature.tag}/{osmFeature.attrib['id']})")
			try:
				appendEntrance(osmFeature, wds, {"Entrance": "StopPlace"})
			except:
				logger_osm.exception(f"Skipped entrance {osmFeature.tag}/{osmFeature.attrib['id']}", exc_info=True)

	# Read all other entrances
	buildingEntranceToRecheck = {}

	if "poi" in withData:
		logger_wdm.info('Processing entrances')
		osmFeaturesDoor1 = ods.search("indoor", "door")
		osmFeaturesDoor2 = ods.search("entrance") + ods.search("door")
		progress = model.Progress(logger_wdm, "Converting", len(osmFeaturesDoor1) + len(osmFeaturesDoor2))

		# indoor=door
		for osmFeature in osmFeaturesDoor1:
			progress.tick()
			if skipEntrance(osmFeature, wds):
				continue
			logger_osm.debug(f"Entrance found ({osmFeature.tag}/{osmFeature.attrib['id']})")
			try:
				appendEntrance(osmFeature, wds, {"Entrance": "InsideSpace"})
			except:
				logger_wdm.exception(f"Skipped entrance {osmFeature.tag}/{osmFeature.attrib['id']}", exc_info=True)

		# entrance=*
		for osmFeature in osmFeaturesDoor2:
			progress.tick()
			if skipEntrance(osmFeature, wds):
				continue

			logger_osm.debug(f"Entrance found ({osmFeature.tag}/{osmFeature.attrib['id']})")
			try:
				createdEntrance = appendEntrance(osmFeature, wds, {"Entrance": "Building"})
				buildingEntranceToRecheck[createdEntrance.attrib["id"]] = createdEntrance
			except:
				logger_wdm.exception(f"Skipped entrance {osmFeature.tag}/{osmFeature.attrib['id']}", exc_info=True)
		
		del osmFeaturesDoor1, osmFeaturesDoor2


	########################################
	# Parkings
	#

	if "path" in withData:
		logger_wdm.info('Processing parkings')
		osmFeatures = ods.search("amenity", "parking_space")
		logger_wdm.debug(f"Found {len(osmFeatures)} parking spaces")
		progress = model.Progress(logger_wdm, "Converting", len(osmFeatures))

		for osmFeature in osmFeatures:
			progress.tick()
			if not (
				ods.getTag(osmFeature, "parking_space") == "disabled"
				or ods.getTag(osmFeature, "access:disabled") == "designated"
				or ods.getTag(osmFeature, "wheelchair") is not None
				or ods.getTag(osmFeature, "capacity:disabled") is not None
			):
				continue
			elif wds.hasOsmFeatureImported(osmFeature):
				continue
			elif ods.getTag(osmFeature, "wheelchair") == "no":
				# Check if wheelchair=no is put on a parking space that _should be_ accessible
				if not (
					ods.getTag(osmFeature, "parking_space") == "disabled"
					or ods.getTag(osmFeature, "access:disabled") == "designated"
					or ods.getTag(osmFeature, "capacity:disabled") is not None
				):
					continue
			elif ods.getTag(osmFeature, "capacity:disabled") == "0":
				continue

			logger_osm.debug(f"Parking found ({osmFeature.tag}/{osmFeature.attrib['id']})")
			try:
				appendParking(osmFeature, wds)
			except:
				logger_wdm.exception(f"Skipped parking {osmFeature.tag}/{osmFeature.attrib['id']}", exc_info=True)


	########################################
	# Points of interest
	#

	if "poi" in withData:
		for poiMapping in mappings.POI_OSM_TO_WDM:
			logger_wdm.info(f"Processing point of interests ({poiMapping['osm_key']}={poiMapping.get('osm_val') or '*'})")
			osmFeatures = ods.search(poiMapping['osm_key'], poiMapping.get('osm_val'))
			logger_wdm.debug(f"Found {len(osmFeatures)} {poiMapping['osm_key']}={poiMapping.get('osm_val') or '*'} POIs")
			progress = model.Progress(logger_wdm, "Converting", len(osmFeatures))

			for osmFeature in osmFeatures:
				progress.tick()
				if poiMapping.get("without_osm_key") and ods.getTag(osmFeature, poiMapping.get("without_osm_key")): #type: ignore
					continue
				if wds.hasOsmFeatureImported(osmFeature):
					continue

				try:
					wdmTags = poiMapping["wdm_tags"]

					# Handle special cases
					if wdmTags.get("Government") == "Parliament":
						if ods.getTag(osmFeature, "admin_level") == "4":
							wdmTags = wdmTags | { "Parliament": "State" }
						elif ods.getTag(osmFeature, "admin_level") == "6":
							wdmTags = wdmTags | { "Parliament": "District" }

					if wdmTags.get("Government") == "Tax":
						if tags.isOsmOperatedBy(ods.getTags(osmFeature), "URSSAF", "Q3550086"):
							wdmTags = wdmTags | { "Tax": "Payroll" }
						else:
							wdmTags = wdmTags | { "Tax": "PersonalIncome" }

					if wdmTags.get("Government") == "Police":
						osmPolice = ods.getTag(osmFeature, "police:FR")
						if osmPolice == "gendarmerie":
							wdmTags = wdmTags | { "Police": "Military" }
						elif osmPolice == "police":
							wdmTags = wdmTags | { "Police": "Civil" }
						elif osmPolice == "police_municipale":
							wdmTags = wdmTags | { "Police": "Local" }
						elif osmPolice == "police_rurale":
							wdmTags = wdmTags | { "Police": "Rural" }

					if wdmTags.get("PointOfInterest") == "ParkingLots":
						osmParking = ods.getTag(osmFeature, "parking")
						if osmParking == "multi-storey":
							wdmTags = wdmTags | { "ParkingLots": "MultiStorey" }
						else :
							continue

					if wdmTags.get("Lodging") in [ "GroupHome", "AssistedLiving", "NursingHome" ]:
						osmSocialFacilityFor = ods.getTag(osmFeature, "social_facility:for")
						if osmSocialFacilityFor == "senior":
							wdmTags = wdmTags | { "PointOfInterest": "Senior" }
						elif osmSocialFacilityFor == "disabled":
							wdmTags = wdmTags | { "PointOfInterest": "DisabledCare" }
						else :
							continue

					if wdmTags.get("Shop") == "EmploymentAgency" and tags.isOsmOperatedBy(ods.getTags(osmFeature), "Pôle Emploi", "Q8901192"):
						wdmTags = { "PointOfInterest": "Government", "Government": "UnemploymentInsurance" }

					# Skip shop=ticket selling public transport tickets (converted to Amenity=TicketOffice later)
					if wdmTags.get("PointOfInterest") == "Shop" and ods.getTag(osmFeature, "tickets:public_transport") in ["yes", "only"]:
						continue

					if wdmTags.get("PointOfInterest") == "Shop" and wdmTags.get("Shop") is None:
						wdmTags = wdmTags | { "Shop": tags.toWDMCase(ods.getTag(osmFeature, "shop")) }

					logger_osm.debug(f"POI found ({osmFeature.tag}/{osmFeature.attrib['id']})")
					appendPointOfInterest(osmFeature, wds, wdmTags)

				except Exception:
					logger_wdm.exception(f"Skipped POI {osmFeature.tag}/{osmFeature.attrib['id']}", exc_info=True)


	########################################
	# Public transports
	#

	# Read quay
	if "transport" in withData:
		logger_wdm.info("Processing quays")
		osmFeatures = ods.search("public_transport", "platform")
		progress = model.Progress(logger_wdm, "Converting", len(osmFeatures) * len(mappings.QUAY_OSMPT_TO_WDM))

		for quayMapping in mappings.QUAY_OSMPT_TO_WDM:
			wdmTags = {"Quay": quayMapping["wdmQuay"]}
			if quayMapping["wdmInsideSpace"]:
				wdmTags["InsideSpace"] = "Quay"

			# public_transport=platform quays
			for osmFeature in osmFeatures:
				progress.tick()
				keep = False

				if wds.hasOsmFeatureImported(osmFeature, True):
					continue

				# Check transportmode=yes
				if ods.getTag(osmFeature, quayMapping["osmType"]) == "yes":
					keep = True

				# Check member of route=transportmode relation
				if not keep and ods.isInRelation(osmFeature, {"route": quayMapping["osmType"]}):
					keep = True

				if not keep:
					continue

				logger_osm.debug(f"Quay found ({osmFeature.tag}/{osmFeature.attrib['id']})")
				try:
					appendQuay(osmFeature, wds, wdmTags, quayMapping["simplify"])
				except:
					logger_wdm.exception(f"Skipped quay {osmFeature.tag}/{osmFeature.attrib['id']}", exc_info=True)


	# Read bus stop
	if "transport" in withData:
		logger_wdm.info("Processing bus stops")
		osmFeatures = ods.search("highway", "bus_stop")
		logger_wdm.debug(f"Found {len(osmFeatures)} bus stops")
		progress = model.Progress(logger_wdm, "Converting", len(osmFeatures))
		for osmFeature in osmFeatures:
			progress.tick()
			if wds.hasOsmFeatureImported(osmFeature, True):
				continue

			logger_osm.debug(f"Bus stop found ({osmFeature.tag}/{osmFeature.attrib['id']})")
			try:
				appendQuay(osmFeature, wds, {"Quay": "Bus"}, True)
			except:
				logger_wdm.exception(f"Skipped bus stop {osmFeature.tag}/{osmFeature.attrib['id']}", exc_info=True)


	# Read railway platform
	if "transport" in withData:
		logger_wdm.info("Processing railway platforms")
		osmFeatures = ods.search("railway", "platform")
		progress = model.Progress(logger_wdm, "Converting", len(osmFeatures) * len(mappings.QUAY_OSMRAIL_TO_WDM))

		for quayMapping in mappings.QUAY_OSMRAIL_TO_WDM:
			wdmTags = {"Quay": quayMapping["wdmQuay"]}
			if quayMapping["wdmInsideSpace"]:
				wdmTags["InsideSpace"] = "Quay"

			for osmFeature in osmFeatures:
				progress.tick()
				if wds.hasOsmFeatureImported(osmFeature, True):
					continue
			
				if (
					(quayMapping["onStandalone"] and (
						quayMapping.get("modeTagOnStandalone") in [False, None]
						or ods.getTag(osmFeature, quayMapping["osmType"]) == "yes"
					))
					or ods.isInRelation(osmFeature, {"route": quayMapping["osmType"]})
				):
					logger_osm.debug(f"Railway platform found ({osmFeature.tag}/{osmFeature.attrib['id']})")
					try:
						appendQuay(osmFeature, wds, wdmTags, False)
					except:
						logger_wdm.exception(f"Skipped railway platform {osmFeature.tag}/{osmFeature.attrib['id']}", exc_info=True)


	# Read stations
	if "transport" in withData:
		for stationMapping in mappings.STATION_OSM_TO_WDM:
			logger_wdm.info(f"Processing stations ({stationMapping['osm_key']}={stationMapping['osm_val']})")
			osmFeatures = ods.search(stationMapping['osm_key'], stationMapping['osm_val'])
			progress = model.Progress(logger_wdm, "Converting", len(osmFeatures))
			for osmFeature in osmFeatures:
				progress.tick()
				if wds.hasOsmFeatureImported(osmFeature):
					continue

				# Find if any sub-mapping matches
				for stationSubMapping in stationMapping["mappings"]:
					if tags.hasAll(ods.getTags(osmFeature), stationSubMapping["osmTags"]):
						logger_osm.debug(f"Station found ({osmFeature.tag}/{osmFeature.attrib['id']})")
						try:
							appendStation(osmFeature, wds, {"PointOfInterest": "StopPlace", "StopPlace": stationSubMapping["wdmType"]})
							break
						except:
							logger_wdm.exception(f"Skipped station {osmFeature.tag}/{osmFeature.attrib['id']}", exc_info=True)


	########################################
	# Paths
	#

	if "path" in withData or "road-with-sidewalk" in withData:
		logger_wdm.info("Processing elevators")

		# Look for elevators
		osmElevatorNodes = ods.search("highway", "elevator", "node")
		osmElevatorWays = ods.search("highway", "elevator", "way")
		elevatorNodes = []
		progress = model.Progress(logger_wdm, "Converting", len(osmElevatorNodes) + len(osmElevatorWays))

		# Import elevator nodes
		for osmFeature in osmElevatorNodes:
			progress.tick()
			if wds.hasOsmFeatureImported(osmFeature, checkSimplifiedGeometries=True):
				continue
			logger_osm.debug(f"Elevator found ({osmFeature.tag}/{osmFeature.attrib['id']})")
			try:
				appendElevator(osmFeature, wds)
				elevatorNodes.append(geometry.xmlToShapely(ods, osmFeature))
			except:
				logger_wdm.exception(f"Skipped elevator {osmFeature.tag}/{osmFeature.attrib['id']}", exc_info=True)
			
		# Import elevator ways, only if no elevator node is inside
		elevatorNodesTree = STRtree(elevatorNodes)
		for osmFeature in osmElevatorWays:
			progress.tick()
			if (
				wds.hasOsmFeatureImported(osmFeature, checkSimplifiedGeometries=True)
				or geometry.hasOtherFeaturesNear(geometry.xmlToShapely(ods, osmFeature), elevatorNodesTree, 0)
			):
				continue
			logger_osm.debug(f"Elevator found ({osmFeature.tag}/{osmFeature.attrib['id']})")
			try:
				appendElevator(osmFeature, wds)
			except:
				logger_wdm.exception(f"Skipped elevator {osmFeature.tag}/{osmFeature.attrib['id']}", exc_info=True)
		
		del osmElevatorNodes, osmElevatorWays
		
		logger_wdm.info("Processing paths")
		osmFeatures = ods.search("highway")
		pathMappings:List[Dict[str,Any]] = mappings.PATH_OSM_TO_WDM.copy() if "path" in withData else []
		pathMappings += mappings.ROAD_SIDEWALK_OSM_TO_WDM.copy() if "road-with-sidewalk" in withData else []
		progress = model.Progress(logger_wdm, "Converting", len(osmFeatures) * len(pathMappings))

		# Check if it matches one of the mappings
		for mapping in pathMappings:
			for osmFeature in osmFeatures:
				progress.tick()
				if not tags.hasAll(ods.getTags(osmFeature), mapping["osmTags"], mapping.get("withoutOsmTags")):
					continue
				if wds.hasOsmFeatureImported(osmFeature, checkSimplifiedGeometries=True):
					continue

				# TODO: handle sidewalk=* -> parallel ways with offset

				# Crossing nodes
				if mapping["wdmSpl"] == "Crossing" and osmFeature.tag == "node":
					# Do not add crossing where pedestrian can't cross (cycleways)
					if not isPedestrianCrossingAllowed(osmFeature, wds):
						logger_osm.debug(f"Skipped non-pedestrian crossing ({osmFeature.tag}/{osmFeature.attrib['id']})")
						continue

					roadSegment = geometry.getRoadSegmentAroundCrossing(osmFeature, wds)
					if roadSegment is not None and set(roadSegment) != {None}:
						try:
							logger_osm.debug(f"Crossing found ({osmFeature.tag}/{osmFeature.attrib['id']})")
							appendLinearCrossing(roadSegment, wds)
							continue
						except Exception:
							logger_wdm.exception("Skipped linear crossing", exc_info=True)

				# Open spaces
				if mapping["wdmSpl"] == "OpenSpace" and osmFeature.tag == "way":
					try:
						logger_osm.debug(f"Open pedestrian space found ({osmFeature.tag}/{osmFeature.attrib['id']})")
						appendOpenSpace(osmFeature, wds)
						continue
					except model.SkipFeatureProcess:
						logger_osm.debug("Skipped open space")
						continue
					except Exception:
						logger_wdm.exception("Skipped open space", exc_info=True)

				# Classic paths
				if osmFeature.tag != "relation":
					logger_osm.debug(f"Path found ({osmFeature.tag}/{osmFeature.attrib['id']})")
					try:
						appendPath(osmFeature, wds, mapping["wdmSpl"])
					except:
						logger_wdm.exception(f"Skipped path {osmFeature.tag}/{osmFeature.attrib['id']}", exc_info=True)
		
		# Look for pedestrian multipolygons
		logger_wdm.info("Processing pedestrian complex areas")
		osmFeatures = ods.search("highway", "pedestrian", "relation") + ods.search("highway", "footway", "relation")
		progress = model.Progress(logger_wdm, "Converting", len(osmFeatures))
		for osmFeature in osmFeatures:
			progress.tick()
			if wds.hasOsmFeatureImported(osmFeature, checkSimplifiedGeometries=True):
				continue
			
			try:
				logger_osm.debug(f"Open pedestrian space found ({osmFeature.tag}/{osmFeature.attrib['id']})")
				appendOpenSpace(osmFeature, wds)
				continue
			except model.SkipFeatureProcess:
				logger_osm.debug("Skipped open space")
				continue
			except Exception:
				logger_wdm.exception("Skipped open space", exc_info=True)


	########################################
	# Obstacles
	#

	if "path" in withData:
		for mapping in mappings.OBSTACLE_OSM_TO_WDM:
			logger_wdm.info(f"Processing obstacles ({mapping['osm_key']}={mapping.get('osm_val', '*')})")
			osmFeatures = ods.search(mapping['osm_key'], mapping.get('osm_val'))
			progress = model.Progress(logger_wdm, "Converting", len(osmFeatures))

			for osmFeature in osmFeatures:
				progress.tick()
				if wds.hasOsmFeatureImported(osmFeature, True, True):
					continue

				# TODO : check if feature matches obstacleLocation (on_path, near_path = <1.40m of a SitePathLink)

				wdmTags = mapping.get("wdmTags", {}) | mapping.get("obstacleTags", {})
				logger_osm.debug(f"Obstacle found ({osmFeature.tag}/{osmFeature.attrib['id']})")
				try:
					# Skip kerb on crossings
					if (
						mapping.get("osm_key") == "kerb"
						and mapping.get("osm_val") == "raised"
					):
						# Node itself is a crossing
						if ods.getTag(osmFeature, "highway") == "crossing":
							continue
						else:
							# Check if parent way is a crossing
							isKerbInCrossing = False
							for nodeParentWay in ods.nodeParentWays.get(osmFeature.attrib["id"], set()):
								if ods.getTag(nodeParentWay, "footway") == "crossing" or ods.getTag(nodeParentWay, "path") == "crossing":
									isKerbInCrossing = True
									break
							if isKerbInCrossing:
								continue
					
					wdmObstacle = appendObstacle(osmFeature, wds, wdmTags)

					# Remove Height tag if any set on kerb
					if wds.getTag(wdmObstacle, "Height") is not None and wds.getTag(wdmObstacle, "Obstacle") == "Kerb":
						wdmObstacle.remove(wdmObstacle.find("tag[@k=\"Height\"]"))
				except:
					logger_wdm.exception(f"Skipped obstacle {osmFeature.tag}/{osmFeature.attrib['id']}", exc_info=True)


	########################################
	# Post-process
	#

	# Prepare WDS
	logger_wdm.info("Starting post-process...")
	wds.processNodesParentWays()
	wds.searches = {}

	# Check if entrance type has to be changed according to contour feature
	logger_wdm.info("Post-processing entrances types")
	progress = model.Progress(logger_wdm, "Processing", len(buildingEntranceToRecheck))
	for eId in buildingEntranceToRecheck:
		progress.tick()
		parentWays = wds.nodeParentWays.get(eId)
		if parentWays is None:
			continue

		hasPoiWays = False
		hasInsideWays = False
		for pw in parentWays:
			if wds.getTag(pw, "PointOfInterest") not in [None, "StopPlace"]:
				hasPoiWays = True
				break
			elif wds.getTag(pw, "InsideSpace") not in [None, "Quay"]:
				hasInsideWays = True

		if hasPoiWays:
			tags.appendTag(buildingEntranceToRecheck[eId], "Entrance", "PointOfInterest")
		elif hasInsideWays:
			tags.appendTag(buildingEntranceToRecheck[eId], "Entrance", "InsideSpace")

	# Change crossing nodes on fixme path junction
	if "path" in withData:
		logger_wdm.info("Post-processing crossings fixmes")
		crossingNodes = wds.search("SitePathLink", "Crossing", "node")
		progress = model.Progress(logger_wdm, "Processing", len(crossingNodes))
		for crossingNode in crossingNodes:
			progress.tick()
			for crossingNodeTag in crossingNode.iter("tag"):
				if crossingNodeTag.attrib["k"] not in mappings.TAG_WDM_NOT_MEANINGFUL:
					crossingNode.remove(crossingNodeTag)

			tags.removeTag(crossingNode, "SitePathLink")
			tags.appendTag(crossingNode, "PathJunction", "Yes")
			tags.appendTag(crossingNode, "FixMe", "Il y a un passage piéton ici, découpez les cheminements existants pour le créer sous forme linéaire.")
		del wds.searches["SitePathLink"]

	# Check for tags computed according to inner/surrounding features
	logger_wdm.info("Post-processing tags based on inner features")
	progress = model.Progress(logger_wdm, "Processing", len(mappings.NEARBY_SEARCHES))
	for nearbySearch in mappings.NEARBY_SEARCHES:
		progress.tick()
		nearbyGeoms = []
		# Create a search tree index using OSM features
		for osmFeature in ods.search(nearbySearch["searchOsmKey"], nearbySearch.get("searchOsmVal")):
			if not tags.hasAll(ods.getTags(osmFeature), {}, nearbySearch.get("searchWithoutOsmTags")):
				continue
			try:
				nearbyGeoms.append(geometry.xmlToShapely(ods, osmFeature))
			except Exception:
				logger_wdm.warning(f"Skipped feature {osmFeature.tag}/{osmFeature.attrib['id']}", exc_info=True)

		if len(nearbyGeoms) > 0:
			nearbyTree = STRtree(nearbyGeoms)

			# Look over all potential WDM features
			for wdmFeature in wds.search(nearbySearch["searchWdmKey"], nearbySearch.get("searchWdmVal")):
				try:
					wdmGeom = geometry.xmlToShapely(wds, wdmFeature)

					# If feature has nearby matches, add WDM tags
					if geometry.hasOtherFeaturesNear(wdmGeom, nearbyTree, nearbySearch["searchDistance"]):
						for k in nearbySearch["nearbyWdmTags"]:
							# Special case of emergency with semicolon-separated features
							if k == "Emergency" and wds.getTag(wdmFeature, "Emergency", forceRead=True) is not None:
								tags.appendTag(wdmFeature, k, wds.getTag(wdmFeature, "Emergency") + ";" + nearbySearch["nearbyWdmTags"][k])
							else:
								tags.appendTag(wdmFeature, k, nearbySearch["nearbyWdmTags"][k])

						continue

				except Exception:
					logger_wdm.warning(f"Skipped WDM feature {wdmFeature.tag}/{wdmFeature.attrib['id']}", exc_info=True)

	# Clear out ODS searches
	ods.searches = {}

	# Create joining paths between isolated node features and footpaths
	if "path" in withData:
		logger_wdm.info("Post-processing joining nodes to nearby footpaths")
		candidatesNodes =  wds.search("ParkingBay", ftag="node")
		candidatesNodes += wds.search("Entrance", ftag="node")
		candidatesNodes += wds.search("Quay", ftag="node")
		nodesToJoin = []

		for candidateNode in candidatesNodes:
			if not any(wds.getTag(way, "SitePathLink") for way in wds.nodeParentWays.get(candidateNode.attrib['id'], [])):
				nodesToJoin.append(candidateNode)

		if len(nodesToJoin) > 0:
			# Create an index tree with all roads in output dataset
			roads = wds.search("SitePathLink", ["Footpath", "Pavement", "Street", "OpenSpace"], ftag="way")
			roadsTree = STRtree([ geometry.xmlToShapely(wds, r, use4326=True) for r in roads ])

			# For each node to join, find its nearest footpath
			for nodeToJoin in nodesToJoin:
				try:
					nodeToJoinGeom = geometry.xmlToShapely(wds, nodeToJoin, use4326=True)
					nearestRoadId = geometry.getNearestFeature(nodeToJoinGeom, roadsTree, 5)

					# Join found footpath to node
					if nearestRoadId is not None:
						nearestRoad = roads[nearestRoadId]
						joiningWay = geometry.joinNodeToWay(wds, nodeToJoin, nearestRoad)

						# Add same tags
						for tagElem in nearestRoad.iter("tag"):
							if tagElem.attrib["k"] not in mappings.TAG_WDM_NOT_MEANINGFUL:
								joiningWay.append(deepcopy(tagElem))
				except Exception:
					logger_wdm.exception("Skipped joining node to footpath", exc_info=True)

	logger_wdm.info("Post-processing sorting features in XML")
	wds.sort()

	return wds.xml


def appendEntrance(osmFeature: etree._Element, wds: model.WalkDataSet, wdmTags: dict):
	"""Inserts into a WDM dataset a given OSM entrance feature, and converts its tags.

	Parameters
	----------
	osmFeature : lxml.etree._Element
		The OSM feature to read
	wds : model.WalkDataSet
		The WDM container
	wdmTags : dict
		List of general tags to add (categories).
		Note that common tags are handled in the function by calling appendCommonTags and appendCommonEntranceTags

	Returns
	-------
	lxml.etree._Element
		The created entrance in WDM dataset
	"""

	wdmEntrance = geometry.copyOsmGeomToWdm(osmFeature, wds, True)
	osmRels = list(wds.ods.relationsByMember[osmFeature.tag].get(osmFeature.attrib["id"], set()))
	tags.appendCommonTags(wds.ods.getTags(osmFeature), wdmEntrance)
	tags.appendCommonEntranceTags(osmFeature, wds.ods.getTags(osmFeature), wdmEntrance, osmRels)

	for k in wdmTags:
		tags.appendTag(wdmEntrance, k, wdmTags[k])

	return wdmEntrance


def appendQuay(osmFeature: etree._Element, wds: model.WalkDataSet, wdmTags: dict, simplifyGeom: bool):
	"""Inserts into a WDM dataset a given OSM quay feature, and converts its tags.

	Parameters
	----------
	osmFeature : lxml.etree._Element
		The OSM feature to read
	wds : model.WalkDataSet
		The WDM container
	wdmTags : dict
		List of general tags to add (categories).
		Note that common tags are handled in the function by calling appendCommonTags and appendCommonQuayTags
	simplifyGeom : bool
		Set to true to change all input geometries into nodes (first one found)

	Returns
	-------
	lxml.etree._Element
		The created feature in WDM dataset
	"""

	wdmFeature = geometry.copyOsmGeomToWdm(osmFeature, wds, simplifyGeom)
	tags.appendCommonTags(wds.ods.getTags(osmFeature), wdmFeature)
	tags.appendCommonQuayTags(wds.ods.getTags(osmFeature), wdmFeature)

	for k in wdmTags:
		tags.appendTag(wdmFeature, k, wdmTags[k])

	return wdmFeature


def appendStation(osmFeature: etree._Element, wds: model.WalkDataSet, wdmTags: dict):
	"""Inserts into a WDM dataset a given OSM quay feature, and converts its tags.

	Parameters
	----------
	osmFeature : lxml.etree._Element
		The OSM feature to read
	wds : model.WalkDataSet
		The WDM container
	wdmTags : dict
		List of general tags to add (categories).
		Note that common tags are handled in the function by calling appendCommonTags and appendCommonStationTags

	Returns
	-------
	lxml.etree._Element
		The created feature in WDM dataset
	"""

	wdmFeature = geometry.copyOsmGeomToWdm(osmFeature, wds)
	tags.appendCommonTags(wds.ods.getTags(osmFeature), wdmFeature)
	tags.appendCommonPoiTags(wds.ods.getTags(osmFeature), wdmFeature)
	tags.appendCommonStationTags(wds.ods.getTags(osmFeature), wdmFeature)

	for k in wdmTags:
		tags.appendTag(wdmFeature, k, wdmTags[k])

	return wdmFeature


def appendParking(osmFeature: etree._Element, wds: model.WalkDataSet):
	"""Inserts into a WDM dataset a given OSM parking feature, and converts its tags.

	Parameters
	----------
	osmFeature : lxml.etree._Element
		The OSM feature to read
	wds : model.WalkDataSet
		The WDM container

	Returns
	-------
	lxml.etree._Element
		The created feature in WDM dataset
	"""

	wdmFeature = geometry.copyOsmGeomToWdm(osmFeature, wds, True)
	tags.appendCommonTags(wds.ods.getTags(osmFeature), wdmFeature)
	tags.appendCommonParkingTags(wds.ods.getTags(osmFeature), wdmFeature)
	tags.appendTag(wdmFeature, "ParkingBay", "Disabled")

	# Add Width/Length for areas
	wds.getTags(wdmFeature, forceRead=True)
	if (
		osmFeature.tag == "way"
		and wds.getTag(wdmFeature, "Width") is None
		and wds.getTag(wdmFeature, "Length") is None
		and (
			wds.ods.getTag(osmFeature, "capacity:disabled")
			or wds.ods.getTag(osmFeature, "capacity")
		) in [None, "1"]
	):
		osmGeom = geometry.xmlToShapely(wds.ods, osmFeature, use4326=False)
		if isinstance(osmGeom, Polygon):
			w,l = geometry.getGeometryWidthLength(osmGeom)
			tags.appendTag(wdmFeature, "Width", "{:.2f}".format(w))
			tags.appendTag(wdmFeature, "Length", "{:.2f}".format(l))

	return wdmFeature


def appendPointOfInterest(osmFeature: etree._Element, wds: model.WalkDataSet, wdmTags: dict):
	"""Inserts into a WDM dataset a given OSM POI feature, and converts its tags.

	Parameters
	----------
	osmFeature : lxml.etree._Element
		The OSM feature to read
	wds : model.WalkDataSet
		The WDM container
	wdmTags : dict
		List of general tags to add (categories).
		Note that common tags are handled in the function by calling appendCommonTags and appendCommonPoiTags

	Returns
	-------
	lxml.etree._Element
		The created feature in WDM dataset
	"""

	wdmPoi = geometry.copyOsmGeomToWdm(osmFeature, wds)
	osmRels = list(wds.ods.relationsByMember[osmFeature.tag].get(osmFeature.attrib["id"], set()))
	tags.appendCommonTags(wds.ods.getTags(osmFeature), wdmPoi)
	tags.appendCommonPoiTags(wds.ods.getTags(osmFeature), wdmPoi, osmRels)

	for k in wdmTags:
		tags.appendTag(wdmPoi, k, wdmTags[k])

	return wdmPoi


def appendObstacle(osmFeature: etree._Element, wds: model.WalkDataSet, wdmTags: dict):
	"""Inserts into a WDM dataset a given OSM obstacle, and converts its tags.

	Parameters
	----------
	osmFeature : lxml.etree._Element
		The OSM feature to read
	wds : model.WalkDataSet
		The WDM container
	wdmTags : dict
		List of general tags to add (categories).
		Note that common tags are handled in the function by calling appendCommonTags and appendCommonObstacleTags

	Returns
	-------
	lxml.etree._Element
		The created feature in WDM dataset
	"""

	wdmFeature = geometry.copyOsmGeomToWdm(osmFeature, wds, True)
	osmTags = wds.ods.getTags(osmFeature)

	# Done before other tags to handle wheelchair access of toilets override in appendCommonObstacleTags
	for k in wdmTags:
		tags.appendTag(wdmFeature, k, wdmTags[k])

	tags.appendCommonTags(osmTags, wdmFeature)
	tags.appendImpairmentTags(osmTags, wdmFeature)
	tags.appendCommonObstacleTags(osmTags, wdmFeature)

	return wdmFeature


def appendElevator(osmFeature: etree._Element, wds: model.WalkDataSet):
	"""Inserts into a WDM dataset a given OSM elevator, and converts its tags.

	Parameters
	----------
	osmFeature : lxml.etree._Element
		The OSM elevator to read
	wds : model.WalkDataSet
		The WDM container

	Returns
	-------
	lxml.etree._Element
		The created feature in WDM dataset
	"""

	osmId = osmFeature.attrib["id"]
	osmTags = wds.ods.getTags(osmFeature)
	wdmFeature = None

	# Elevator way : simplify and connect paths
	if osmFeature.tag == "way":
		# Contour geometry
		outerNodes = [ wds.ods.nodes[n.attrib["ref"]] for n in osmFeature.iter("nd") if n.attrib["ref"] in wds.ods.nodes ]
		if outerNodes[0] == outerNodes[-1]:
			geomArea = geometry.xmlNodesToShapely(outerNodes, use4326=True) # type: ignore

			# Look for traversing paths
			osmContourNodes = geometry.getAreaUsefulNodes(outerNodes, [osmId], wds.ods)
			traversingWays = geometry.getTraversingWays(geomArea, [osmId], osmContourNodes, wds.ods)
			
			# No traversing ways = simplify elevator as node
			if len(traversingWays) == 0:
				# Compute central node position
				geomAreaCenter = geomArea.centroid
				wdmFeature = E.node(
					id=str(wds.getNextNewElementId("node")),
					lon=f"{geomAreaCenter.x:0.7f}",
					lat=f"{geomAreaCenter.y:0.7f}"
				)
				tags.appendTag(wdmFeature, "Ref:Import:Geometry:Id", osmId)
				wds.insert(wdmFeature)
				
				# Generate links between interesting contour nodes and central node (if any)
				for osmCN in osmContourNodes:
					wdmStartNode = geometry.copyOsmGeomToWdm(osmCN, wds)
					wdmWay = E.way(
						E.nd(ref=wdmStartNode.attrib["id"]),
						E.nd(ref=wdmFeature.attrib["id"]),
						id=str(wds.getNextNewElementId("way"))
					)
					tags.appendTag(wdmWay, "SitePathLink", "Corridor")
					tags.appendTag(wdmWay, "Level", tags.getOsmLevelValue(wds.ods.getTags(osmCN)) or tags.getOsmLevelValue(osmTags))
					wds.insert(wdmWay)

	# Fallback to default import if we don't fall in any specific case
	if wdmFeature is None:
		wdmFeature = geometry.copyOsmGeomToWdm(osmFeature, wds)

	tags.appendTag(wdmFeature, "SitePathLink", "Elevator")
	tags.appendCommonTags(osmTags, wdmFeature)
	tags.appendCommonPathTags(osmTags, wdmFeature)
	tags.appendElevatorTags(osmTags, wdmFeature)

	# AutomaticDoor
	wdmAutoDoor = "Yes"
	for wayNode in osmFeature.iter("nd"):
		node = wds.ods.nodes.get(wayNode.attrib["ref"])
		if node is not None:
			osmAutoDoor = wds.ods.getTag(node, "automatic_door")
			if osmAutoDoor == "no":
				wdmAutoDoor = "No"
				continue
	tags.appendTag(wdmFeature, "AutomaticDoor", wdmAutoDoor)

	return wdmFeature


def appendPath(osmFeature: etree._Element, wds: model.WalkDataSet, splVal: str):
	"""Inserts into a WDM dataset a given OSM path feature, and converts its tags.

	Parameters
	----------
	osmFeature : lxml.etree._Element
		The OSM feature to read
	wds : model.WalkDataSet
		The WDM container
	splVal : str
		Value to set for the SitePathLink WDM tags

	Returns
	-------
	lxml.etree._Element
		The created feature in WDM dataset
	"""

	osmTags = wds.ods.getTags(osmFeature)
	wdmFeature = geometry.copyOsmGeomToWdm(osmFeature, wds)
	tags.appendTag(wdmFeature, "SitePathLink", splVal)
	tags.appendCommonTags(osmTags, wdmFeature)
	tags.appendCommonPathTags(osmTags, wdmFeature)

	# Elevator-specific tags
	if splVal == "Elevator":
		tags.appendElevatorTags(osmTags, wdmFeature)

	# Stairs post-process
	if splVal in ["Stairs", "Escalator"] and osmFeature.tag == "way":
		postProcessPathExtremetyNodes(osmFeature, wds, wdmFeature)

	# AutomaticDoor for elevators
	if splVal == "Elevator" and osmFeature.tag == "way":
		wdmAutoDoor = "Yes"

		for wayNode in osmFeature.iter("nd"):
			node = wds.ods.nodes.get(wayNode.attrib["ref"])
			if node is not None:
				osmAutoDoor = wds.ods.getTag(node, "automatic_door")
				if osmAutoDoor == "no":
					wdmAutoDoor = "No"
					continue

		tags.appendTag(wdmFeature, "AutomaticDoor", wdmAutoDoor)


	# Crossing as way post-process
	if splVal == "Crossing" and osmFeature.tag == "way":
		nodesToCheck = [ wds.ods.nodes.get(wn.attrib["ref"]) for wn in osmFeature.iter("nd") if wds.ods.nodes.get(wn.attrib["ref"]) is not None ]
		postProcessPathExtremetyNodes(osmFeature, wds, wdmFeature, nodesToCheck)
		
		# Check out nodes with potential crossing tags
		for node in nodesToCheck:
			tags.appendCrossingTags(wds.ods.getTags(node), wdmFeature)

		# Look for nodes which could specify the kind of crossing
		wdmCrossing = "Road"
		for node in nodesToCheck:
			# Look for tags
			nodeRailway = wds.ods.getTag(node, "railway")
			if nodeRailway in ["crossing", "level_crossing"]:
				wdmCrossing = "Rail"
				continue

			if nodeRailway in ["tram_crossing", "tram_level_crossing"]:
				wdmCrossing = "UrbanRail"
				continue

			# Look for parent ways
			parentsWays = wds.ods.nodeParentWays[node.attrib['id']]
			for pw in parentsWays:
				if wds.ods.getTag(pw, "railway") in ["tram", "light_rail"]:
					wdmCrossing = "UrbanRail"
					continue
				elif wds.ods.getTag(pw, "railway") is not None:
					wdmCrossing = "Rail"
					continue
				elif wds.ods.getTag(pw, "highway") == "cycleway":
					wdmCrossing = "CycleWay"
					continue
			
			if wdmCrossing != "Road":
				continue

		tags.appendTag(wdmFeature, "Crossing", wdmCrossing)

	return wdmFeature


def appendOpenSpacePart(
	osmId: str,
	osmTags: Dict[str,str],
	outerNodes: List[etree._Element],
	outerWaysIds: List[str],
	wds: model.WalkDataSet
):
	# Find tagged or connected contour nodes
	osmContourNodes = geometry.getAreaUsefulNodes(outerNodes, outerWaysIds, wds.ods)

	# If no contour node is used, import as a classic path
	if len(osmContourNodes) == 0:
		return "append_orig"
	
	# Look for traversing paths
	geomArea = geometry.xmlNodesToShapely(outerNodes, use4326=True) # type: ignore
	traversingWays = geometry.getTraversingWays(geomArea, outerWaysIds, osmContourNodes, wds.ods)

	# Traversing way found
	if len(traversingWays) > 0:
		# Check if each contour node is connected
		contourNodesNotConnected = []
		for osmCN in osmContourNodes:
			# Misses connection
			osmCNPW = wds.ods.nodeParentWays.get(osmCN.attrib["id"], set())
			if next((w for w in osmCNPW if w in traversingWays), None) is None:
				contourNodesNotConnected.append(osmCN)
		
		# All connected: just pass the area
		if len(contourNodesNotConnected) == 0:
			return "skip"
		
		# Compute missing connections
		insideNodes = {}
		for tw in traversingWays:
			twn = tw.findall("nd")
			twn = [ wds.ods.nodes[n.attrib["ref"]] for n in twn ]
			twg = [ (n, geometry.xmlToShapely(wds.ods, n, use4326=True)) for n in twn ]
			twg = [ ng for ng in twg if ng[1].within(geomArea) ]
			for [n,g] in twg:
				insideNodes[n.attrib["id"]] = (n,g)
		
		if len(insideNodes) == 0:
			raise Exception("No inside node to connect")
		
		minIN = None
		minDist = None
		toAdd = []
		for cnnc in contourNodesNotConnected:
			geomCN = geometry.xmlToShapely(wds.ods, cnnc, use4326=True)
			# Find nearest inside node
			for inId, inData in insideNodes.items():
				inXml, inShp = inData
				dist = distance(geomCN, inShp)
				if minIN is None or dist < minDist:
					minIN = inXml
					minDist = dist
			
			# Check if generated way will lie inside area
			geomWdmWay = geometry.xmlNodesToShapely([cnnc, minIN], use4326=True)
			if within(geomWdmWay, geomArea):
				toAdd.append((cnnc, minIN, osmId))
			else:
				return "append_orig"
		
		# Connect
		for t in toAdd:
			wdmStartNode = geometry.copyOsmGeomToWdm(t[0], wds)
			wdmEndNode = geometry.copyOsmGeomToWdm(t[1], wds) # type: ignore
			tags.appendTag(wdmEndNode, "Ref:Import:Geometry:Id", t[2])
			wdmWay = E.way(
				E.nd(ref=wdmStartNode.attrib["id"]),
				E.nd(ref=wdmEndNode.attrib["id"]),
				id=str(wds.getNextNewElementId("way"))
			)
			tags.appendTag(wdmWay, "SitePathLink", "OpenSpace")
			tags.appendCommonTags(osmTags, wdmWay)
			tags.appendCommonPathTags(osmTags, wdmWay)
			wds.insert(wdmWay)
		
		return "mark_imported"

	# No traversing ways: use centroid instead
	else:
		# Compute central node position
		geomAreaCenter = geomArea.centroid
		if not geomAreaCenter.within(geomArea):
			geomAreaCenter = point_on_surface(geomArea)
		wdmCenterNode = E.node(
			id=str(wds.getNextNewElementId("node")),
			lon=f"{geomAreaCenter.x:0.7f}",
			lat=f"{geomAreaCenter.y:0.7f}"
		)

		# Check no generated way will lie outside of place
		for osmCN in osmContourNodes:
			geomWdmWay = geometry.xmlNodesToShapely([osmCN, wdmCenterNode], use4326=True)
			if not within(geomWdmWay, geomArea):
				return "append_orig"
		
		tags.appendTag(wdmCenterNode, "PathJunction", "OpenSpace")
		tags.appendTag(wdmCenterNode, "Ref:Import:Geometry:Id", osmId)
		wds.insert(wdmCenterNode)
		
		# Generate links between interesting contour nodes and central node
		for osmCN in osmContourNodes:
			wdmStartNode = geometry.copyOsmGeomToWdm(osmCN, wds)
			wdmWay = E.way(
				E.nd(ref=wdmStartNode.attrib["id"]),
				E.nd(ref=wdmCenterNode.attrib["id"]),
				id=str(wds.getNextNewElementId("way"))
			)
			tags.appendTag(wdmWay, "SitePathLink", "OpenSpace")
			tags.appendCommonTags(osmTags, wdmWay)
			tags.appendCommonPathTags(osmTags, wdmWay)
			wds.insert(wdmWay)


def appendOpenSpace(osmFeature: etree._Element, wds: model.WalkDataSet):
	"""Creates an interconnected set of paths instead of an open space pedestrian area.

	Parameters
	----------
	osmFeature : lxml.etree._Element
		The OSM feature to read
	wds : model.WalkDataSet
		The WDM container
	"""

	osmTags = wds.ods.getTags(osmFeature)
	outerNodes = []

	if osmFeature.tag == "way":
		outerNodes = [ wds.ods.nodes[n.attrib["ref"]] for n in osmFeature.iter("nd") if n.attrib["ref"] in wds.ods.nodes ]
	elif osmFeature.tag == "relation":
		osmOuter = geometry.getMultipolygonOuterWay(osmFeature, wds.ods)
		if osmOuter.tag == "way":
			outerNodes = [ wds.ods.nodes[n.attrib["ref"]] for n in osmOuter.iter("nd") if n.attrib["ref"] in wds.ods.nodes ]
	
	if len(outerNodes) >= 4:
		postProcess = appendOpenSpacePart(
			f"{osmFeature.tag[0:1]}{osmFeature.attrib['id']}",
			osmTags,
			outerNodes,
			[osmFeature.attrib["id"]],
			wds
		)

		if postProcess == "append_orig":
			appendPath(osmFeature, wds, "OpenSpace")
		elif postProcess in ["skip", "mark_imported"]:
			if osmFeature.attrib["id"] not in wds.importedOsmFeaturesById["way"]:
				wds.importedOsmFeaturesById["way"][osmFeature.attrib["id"]] = set()
			if postProcess == "skip":
				raise model.SkipFeatureProcess()


def appendLinearCrossing(osmRoadSegment: list, wds: model.WalkDataSet):
	"""Creates a perpendicular crossing based on OSM road segment given.

	Parameters
	----------
	osmRoadSegment : list
		The OSM road segment (list of 3 nodes, middle one being a node crossing)
		This may be the result of geometry.getRoadSegmentAroundCrossing function
	wds : model.WalkDataSet
		The WDM container

	Returns
	-------
	lxml.etree._Element
		The created feature in WDM dataset
	"""

	osmN1, osmN2, osmN3 = osmRoadSegment

	# Create the middle node in WDM if not exists
	wdmN2 = geometry.copyOsmNodeToWdm(osmN2, wds)
	wdmN2.find("tag[@k=\"Ref:Import:Id\"]").attrib["k"] = "Ref:Import:Geometry:Id"

	# Compute angles between segments from OSM
	osmAngle: float
	if osmN1 is not None and osmN3 is not None:
		osmAngle1 = geometry.getAngle(osmN1, osmN2)
		osmAngle2 = geometry.getAngle(osmN2, osmN3)
		osmAngle = (osmAngle1 + osmAngle2) / 2
	elif osmN1 is not None:
		osmAngle = geometry.getAngle(osmN1, osmN2)
	elif osmN3 is not None:
		osmAngle = geometry.getAngle(osmN2, osmN3)

	# Create start node in WDM
	wdmN1XY = geometry.projectPointAtAngle(osmN2, (osmAngle + 90) % 360, 3.5)
	wdmN1 = E.node(
		id=str(wds.getNextNewElementId("node")),
		lon=wdmN1XY[0],
		lat=wdmN1XY[1]
	)
	tags.appendTag(wdmN1, "PathJunction", "Crossing")
	wds.insert(wdmN1)

	# Create end node in WDM
	wdmN3XY = geometry.projectPointAtAngle(osmN2, (osmAngle - 90) % 360, 3.5)
	wdmN3 = E.node(
		id=str(wds.getNextNewElementId("node")),
		lon=wdmN3XY[0],
		lat=wdmN3XY[1]
	)
	tags.appendTag(wdmN3, "PathJunction", "Crossing")
	wds.insert(wdmN3)

	# Create crossing way in WDM
	wdmWay = E.way(
		E.nd(ref=wdmN1.attrib["id"]),
		E.nd(ref=wdmN2.attrib["id"]),
		E.nd(ref=wdmN3.attrib["id"]),
		id=str(wds.getNextNewElementId("way"))
	)
	tags.appendTag(wdmWay, "SitePathLink", "Crossing")
	tags.appendRefTags(osmN2, wdmWay)
	tags.appendCommonTags(tags.getAll(osmN2), wdmWay)
	tags.appendCommonPathTags(tags.getAll(osmN2), wdmWay)

	# Move LinearCue tag on way on extremity nodes
	tactileTag = tags.getValue(wdmWay, "LinearCue")
	if tactileTag:
		tags.removeTag(wdmWay, "LinearCue")
		if tactileTag == "GuidingStrip":
			tags.appendTag(wdmN1, "TactileWarningStrip", "Good")
			tags.appendTag(wdmN1, "FixMe:TactileWarningStrip", "Vérifier l’état du marquage de la borne d’éveil à vigilance")
			tags.appendTag(wdmN3, "TactileWarningStrip", "Good")
			tags.appendTag(wdmN3, "FixMe:TactileWarningStrip", "Vérifier l’état du marquage de la borne d’éveil à vigilance")

	wds.insert(wdmWay)

	return wdmWay


def postProcessPathExtremetyNodes(
	osmFeature: etree._Element, wds: model.WalkDataSet, wdmFeature: etree._Element,
	nodesToCheck: Optional[List[etree._Element]] = None
) -> None:
	"""Runs various tasks on ways and their extremity nodes"""

	# Find extremity nodes
	wdmWayNodes = wdmFeature.findall("nd")
	wdmStartNode = wds.nodes[wdmWayNodes[0].attrib["ref"]]
	wdmEndNode = wds.nodes[wdmWayNodes[len(wdmWayNodes)-1].attrib["ref"]]
	osmWayNodes = osmFeature.findall("nd")
	osmStartNode = wds.ods.nodes[osmWayNodes[0].attrib["ref"]]
	osmEndNode = wds.ods.nodes[osmWayNodes[len(osmWayNodes)-1].attrib["ref"]]

	# Add PathJunction=* on extremity nodes
	splType = wds.getTag(wdmFeature, "SitePathLink", forceRead=True)
	tags.appendTag(wdmStartNode, "PathJunction", splType)
	tags.appendTag(wdmEndNode, "PathJunction", splType)

	# Move tactile paving info on extremity nodes
	tags.removeTag(wdmFeature, "LinearCue")
	osmTactilePaving = wds.ods.getTag(osmFeature, "tactile_paving")
	osmIncline = wds.ods.getTag(osmFeature, "incline")

	if osmTactilePaving == "no":
		tags.appendTag(wdmStartNode, "TactileWarningStrip", "None")
		tags.appendTag(wdmEndNode, "TactileWarningStrip", "None")
	elif osmTactilePaving in ["yes", "contrasted"]:
		tags.appendTag(wdmStartNode, "TactileWarningStrip", "Good")
		tags.appendTag(wdmStartNode, "FixMe:TactileWarningStrip", "Vérifier l’état de la bande d'éveil à vigilance")
		tags.appendTag(wdmEndNode, "TactileWarningStrip", "Good")
		tags.appendTag(wdmEndNode, "FixMe:TactileWarningStrip", "Vérifier l’état de la bande d'éveil à vigilance")
	elif osmIncline is not None:
		# Check if incline is usable
		incline = None
		if osmIncline in ["up", "down"]:
			incline = osmIncline
		else:
			slope = tags.getOsmSlopeValue(osmIncline)
			if slope is not None:
				incline = "up" if float(slope) >= 0 else "down"
		
		if osmTactilePaving == "partial" and incline == "up":
			tags.appendTag(wdmEndNode, "TactileWarningStrip", "Good")
			tags.appendTag(wdmEndNode, "FixMe:TactileWarningStrip", "Vérifier l’état de la bande d'éveil à vigilance")
		elif osmTactilePaving == "partial" and incline == "down":
			tags.appendTag(wdmStartNode, "TactileWarningStrip", "Good")
			tags.appendTag(wdmStartNode, "FixMe:TactileWarningStrip", "Vérifier l’état de la bande d'éveil à vigilance")
		elif osmTactilePaving == "incorrect" and incline == "up":
			tags.appendTag(wdmStartNode, "TactileWarningStrip", "Good")
			tags.appendTag(wdmStartNode, "FixMe:TactileWarningStrip", "Vérifier l’état de la bande d'éveil à vigilance")
		elif osmTactilePaving == "incorrect" and incline == "down":
			tags.appendTag(wdmEndNode, "TactileWarningStrip", "Good")
			tags.appendTag(wdmEndNode, "FixMe:TactileWarningStrip", "Vérifier l’état de la bande d'éveil à vigilance")
	elif nodesToCheck is not None:
		# Check if any inner node has tactile paving info
		for node in nodesToCheck:
			nodeTags = wds.ods.getTags(node)
			if nodeTags.get("highway") == "crossing" and "tactile_paving" in nodeTags:
				osmTactilePaving = nodeTags.get("tactile_paving")
				if osmTactilePaving == "no":
					tags.appendTag(wdmStartNode, "TactileWarningStrip", "None")
					tags.appendTag(wdmEndNode, "TactileWarningStrip", "None")
				elif osmTactilePaving in ["yes", "contrasted"]:
					tags.appendTag(wdmStartNode, "TactileWarningStrip", "Good")
					tags.appendTag(wdmStartNode, "FixMe:TactileWarningStrip", "Vérifier l’état de la bande d'éveil à vigilance")
					tags.appendTag(wdmEndNode, "TactileWarningStrip", "Good")
					tags.appendTag(wdmEndNode, "FixMe:TactileWarningStrip", "Vérifier l’état de la bande d'éveil à vigilance")
				break

	# VisualContrast on extremity nodes
	osmStepContrast = wds.ods.getTag(osmFeature, "step:contrast")
	wdmValue = "No" if osmStepContrast == "no" else ("Yes" if osmStepContrast in ["yes", "bad"] else None)
	if wdmValue is not None:
		tags.appendTag(wdmStartNode, "VisualContrast", wdmValue)
		tags.appendTag(wdmEndNode, "VisualContrast", wdmValue)
	
	# VisualObstacle on extremity nodes
	osmCrossingBuffer = wds.ods.getTag(osmFeature, "crossing:buffer_marking")
	if osmCrossingBuffer in ["both", "left", "right", "yes"]:
		tags.appendTag(wdmStartNode, "VisualObstacle", "None")
		tags.appendTag(wdmEndNode, "VisualObstacle", "None")
	
	# Add classic obstacle tags on extremity nodes
	tags.appendImpairmentTags(wds.ods.getTags(osmStartNode), wdmStartNode)
	tags.appendCommonObstacleTags(wds.ods.getTags(osmStartNode), wdmStartNode)
	tags.appendImpairmentTags(wds.ods.getTags(osmEndNode), wdmEndNode)
	tags.appendCommonObstacleTags(wds.ods.getTags(osmEndNode), wdmEndNode)

	# KerbDesign
	if nodesToCheck is not None:
		for node in nodesToCheck:
			nodeTags = wds.ods.getTags(node)
			if nodeTags.get("highway") == "crossing" and "kerb" in nodeTags:
				osmKerb = nodeTags.get("kerb")
				tags.appendTagValueMapping(wdmStartNode, "KerbDesign", mappings.TAG_KERB, osmKerb)
				tags.appendTagValueMapping(wdmEndNode, "KerbDesign", mappings.TAG_KERB, osmKerb)
				if osmKerb == "lowered":
					tags.appendTag(wdmStartNode, "FixMe:KerbDesign", "Vérifier s’il s’agit d’un ressaut abaissé ou d’un bateau")
					tags.appendTag(wdmEndNode, "FixMe:KerbDesign", "Vérifier s’il s’agit d’un ressaut abaissé ou d’un bateau")
				break
	
	# Bollard
	if nodesToCheck is not None:
		for node in nodesToCheck:
			nodeTags = wds.ods.getTags(node)
			if nodeTags.get("highway") == "crossing" and "crossing:bollard" in nodeTags:
				osmBollard = nodeTags.get("crossing:bollard")
				tags.appendTagValueMapping(wdmStartNode, "Bollard", mappings.TAG_YESNO_WITH_CONTRASTED, osmBollard)
				tags.appendTagValueMapping(wdmEndNode, "Bollard", mappings.TAG_YESNO_WITH_CONTRASTED, osmBollard)
				if osmBollard == "contrasted":
					tags.appendTag(wdmStartNode, "Bollard:VisualContrast", "Yes")
					tags.appendTag(wdmEndNode, "Bollard:VisualContrast", "Yes")
				break


def isPedestrianCrossingAllowed(osmNode: etree._Element, wds: model.WalkDataSet):
	"""Checks if a crossing node allows pedestrian to get through

	Parameters
	----------
	osmNode : lxml.etree.Element
		The OSM node to check
	wds : model.WalkDataSet
		The WDM container

	Returns
	-------
	boolean
		True if pedestrian are allowed to get through this crossing
	"""

	nodeId = osmNode.attrib['id']
	parentWays = list(wds.ods.nodeParentWays.get(nodeId, set()))
	nbParentWays = len(parentWays)

	def isRoad(way):
		return wds.ods.getTag(way, "highway") not in ["footway", "path", "cycleway"]
	
	def isFootAllowed(way):
		return (
			wds.ods.getTag(way, "highway") == "footway"
			or (wds.ods.getTag(way, "highway") in ["cycleway", "path"] and wds.ods.getTag(way, "foot") not in [None, "no"])
		)

	# Single parent road
	if nbParentWays == 1:
		pw = parentWays[0]
		return wds.ods.getTag(pw, "highway") != "cycleway" or wds.ods.getTag(pw, "foot") not in [None, "no"]
	
	# Many parents
	else:
		return any(isFootAllowed(pw) for pw in parentWays) or all(isRoad(pw) for pw in parentWays)
