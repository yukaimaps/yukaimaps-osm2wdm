from lxml import etree
from lxml.builder import E
import math
import angles # type: ignore
from shapely import Point, LineString, Polygon, Geometry, STRtree, shortest_line, equals_exact, dwithin # type: ignore
from . import model, converter, tags
from pyproj import Transformer
from pyproj.enums import TransformDirection
from typing import Dict, Optional, List
import networkx as nx # type: ignore
from functools import cmp_to_key # type: ignore


TRANFORM_4326_3857 = Transformer.from_crs(4326, 3857, always_xy=True)
TAU = 2 * math.pi
EQUATORIAL_RADIUS = 6356752.314245179
POLAR_RADIUS = 6378137.0

def copyOsmGeomToWdm(osmFeature: etree._Element, wds: model.WalkDataSet, forceNodeGeom: bool = False):
	"""Copies and OSM feature geometry into a WDM dataset.

	Parameters
	----------
	osmFeature : lxml.etree.Element
		The OSM feature to copy geometry from
	wds : model.WalkDataSet
		The WDM container
	forceNodeGeom : bool
		Transform complex geometries into a single node (defaults to false)

	Returns
	-------
	lxml.etree.Element
		The WDM element corresponding to copied feature
	"""

	if osmFeature.tag == "node":
		return copyOsmNodeToWdm(osmFeature, wds)

	elif osmFeature.tag == "way":
		if forceNodeGeom:
			outerNodes = [ wds.ods.nodes[n.attrib["ref"]] for n in osmFeature.iter("nd") if n.attrib["ref"] in wds.ods.nodes ]
			if outerNodes[0] == outerNodes[-1]:
				geomArea = xmlNodesToShapely(outerNodes, use4326=True) # type: ignore
				geomAreaCenter = geomArea.centroid
				wdmFeature = E.node(
					id=str(wds.getNextNewElementId("node")),
					lon=f"{geomAreaCenter.x:0.7f}",
					lat=f"{geomAreaCenter.y:0.7f}"
				)
				tags.appendTag(wdmFeature, "Ref:Import:Geometry:Id", "w"+osmFeature.attrib["id"])
				tags.appendTag(wdmFeature, "Ref:Import:Source", "OpenStreetMap")
				tags.appendTag(wdmFeature, "Ref:Import:Version", osmFeature.get("version"))
				wds.insert(wdmFeature)
			else :
				osmFeatureFirstNodeRef = osmFeature.find("nd")
				if osmFeatureFirstNodeRef is None:
					raise Exception("This way doesn't have any node: "+osmFeature.attrib["id"])
				osmFeatureFirstNodeId = osmFeatureFirstNodeRef.attrib["ref"]
				osmFeatureFirstNode = wds.ods.nodes[osmFeatureFirstNodeId]
				wdmFeature = copyOsmNodeToWdm(osmFeatureFirstNode, wds)
				# Overwrite ref tags
				tags.appendTag(wdmFeature, "Ref:Import:Id", "n"+osmFeatureFirstNodeId)
				tags.appendTag(wdmFeature, "Ref:Import:Geometry:Id", "w"+osmFeature.attrib["id"])
			if osmFeature.attrib["id"] not in wds.importedSimplifiedOsmFeaturesById["way"]:
				wds.importedSimplifiedOsmFeaturesById["way"][osmFeature.attrib["id"]] = set([wdmFeature])
			else:
				wds.importedSimplifiedOsmFeaturesById["way"][osmFeature.attrib["id"]].add(wdmFeature)
			return wdmFeature

		else:
			return copyOsmWayToWdm(osmFeature, wds)

	elif osmFeature.tag == "relation" and osmFeature.find("tag[@k=\"type\"][@v=\"multipolygon\"]") is not None:
		return copyOsmMultipolygonToWdm(osmFeature, wds)

	elif osmFeature.tag == "relation":
		relType = osmFeature.find("tag[@k=\"type\"]")
		relTypeStr = relType.attrib["v"] if relType is not None else "empty"
		raise Exception("Unsupported relation type: "+relTypeStr)

	else:
		raise Exception("Unsupported geometry type: "+osmFeature.tag)


def copyOsmNodeToWdm(osmNode: etree._Element, wds: model.WalkDataSet):
	"""Copies an OSM node geometry into a WDM dataset.

	Parameters
	----------
	osmNode : lxml.etree.Element
		The OSM node to read
	wds : model.WalkDataSet
		The WDM container

	Returns
	-------
	lxml.etree.Element
		The WDM element corresponding to copied way
	"""

	# Check if OSM node already has been copied into WDM
	if not wds.hasOsmFeatureImported(osmNode):
		wdmNodeId = str(wds.getNextNewElementId("node"))
		wdmNode = E.node(id=wdmNodeId, lat=osmNode.attrib["lat"], lon=osmNode.attrib["lon"])
		tags.appendRefTags(osmNode, wdmNode)
		wds.importedOsmFeaturesById["node"][osmNode.attrib['id']] = set([wdmNode])
		wds.insert(wdmNode)
		wds.osm2wdmNodesIds[osmNode.attrib['id']] = wdmNodeId
		return wdmNode

	else:
		return wds.nodes.get(wds.osm2wdmNodesIds.get(osmNode.attrib['id'], ""), None)


def copyOsmWayToWdm(osmWay: etree._Element, wds: model.WalkDataSet):
	"""Copies a whole OSM way geometry (feature itself and its nodes) into a WDM dataset.

	Parameters
	----------
	osmWay : lxml.etree.Element
		The OSM way to copy
	wds : model.WalkDataSet
		The WDM container

	Returns
	-------
	lxml.etree.Element
		The WDM element corresponding to copied way
	"""

	# Create way element
	wdmWayId = wds.getNextNewElementId("way")
	wdmWay = E.way(id=str(wdmWayId))
	tags.appendRefTags(osmWay, wdmWay)

	# Inject vertex nodes
	for osmWayNode in osmWay.iter("nd"):
		osmNode = wds.ods.nodes[osmWayNode.attrib['ref']]
		osmNodeInWdm = copyOsmNodeToWdm(osmNode, wds)
		wdmWay.append(E.nd(ref=osmNodeInWdm.attrib["id"]))

	# Append to WDM
	wds.insert(wdmWay)
	return wdmWay


def copyOsmMultipolygonToWdm(osmMP: etree._Element, wds: model.WalkDataSet):
	"""Copies a whole OSM multipolygon geometry (only outer) into a WDM dataset.

	Parameters
	----------
	osmMP : lxml.etree.Element
		The OSM multipolygon to copy
	wds : model.WalkDataSet
		The WDM container

	Returns
	-------
	lxml.etree.Element
		The WDM element corresponding to copied multipolygon
	"""

	osmOuter = getMultipolygonOuterWay(osmMP, wds.ods)

	if osmOuter.tag == "node":
		wdmNode = copyOsmNodeToWdm(osmOuter, wds)
		tags.appendRefTags(osmMP, wdmNode)
		return wdmNode

	elif "id" in osmOuter.attrib:
		wdmWay = copyOsmWayToWdm(osmOuter, wds) # type: ignore
		tags.appendRefTags(osmMP, wdmWay)
		return wdmWay
	
	else:
		wdmWayId = wds.getNextNewElementId("way")
		wdmWay = E.way(id=str(wdmWayId))
		tags.appendRefTags(osmMP, wdmWay)
		for nref in osmOuter.iter("nd"):
			osmNode = wds.ods.nodes[nref.attrib["ref"]]
			osmNodeInWdm = copyOsmNodeToWdm(osmNode, wds)
			wdmWay.append(E.nd(ref=osmNodeInWdm.attrib["id"]))

		wds.insert(wdmWay)
		return wdmWay



def getMultipolygonOuterWay(osmMP: etree._Element, ods: model.OsmDataSet) -> etree._Element:
	"""
	Find the best representing outer way for a multipolygon
	
	Parameters
	----------
	osmMP : lxml.etree.Element
		The OSM multipolygon to analyze
	
	Returns
	-------
	lxml.etree.Element
		An OSM-like Element representing the multipolygon
	"""

	# Find outer ways
	osmOuters = osmMP.findall("member[@type=\"way\"][@role=\"outer\"]")
	if len(osmOuters) == 0:
		# Try with unnamed roles
		osmOuters = osmMP.findall("member[@type=\"way\"][@role=\"\"]")
		if len(osmOuters) == 0:
			raise Exception(f"Multipolygon relation {osmMP.attrib['id']} does not have a way with outer role")
	
	osmWays = [ ods.ways.get(osmOuter.attrib["ref"], None) for osmOuter in osmOuters ]
	osmWays = [ w for w in osmWays if w is not None ]
	if len(osmWays) == 0:
		ids = ", ".join([ o.attrib["ref"] for o in osmOuters ])
		raise Exception(f"Multipolygon outer ways not found (IDs: {ids})")
	
	# Look for the best outer way to represent this multipolygon
	osmBestWay = osmWays[0]

	# More than 1 outer: subtle process
	if len(osmWays) > 1:
		# Outers are split between closed and non-closed ones
		osmWaysClosed = []
		osmWaysNonClosed = []
		osmWaysNodes = {}

		for w in osmWays:
			wn = w.findall("nd") # type: ignore
			# If first & last node are the same -> closed
			if wn[0].attrib["ref"] == wn[-1].attrib["ref"]:
				osmWaysClosed.append(w)
			else:
				osmWaysNonClosed.append(w)
			osmWaysNodes[w.attrib["id"]] = wn # type: ignore
		
		# Closed ways available: we use them
		if len(osmWaysClosed) > 0:
			osmBestWay = osmWaysClosed[0]

			# Many closed outers: find the closed way with the most nodes in it
			if len(osmWaysClosed) > 1:
				osmBestWayNbNodes = len(osmWaysNodes[osmBestWay.attrib["id"]]) # type: ignore
				for w in osmWaysClosed[1:]:
					wNbNodes = len(osmWaysNodes[w.attrib["id"]]) # type: ignore
					if wNbNodes > osmBestWayNbNodes:
						osmBestWay = w
						osmBestWayNbNodes = wNbNodes
		
		# Only non-closed ways: that's where the trouble begin
		else:
			# Create a network with start/end nodes of each outer part
			graph = nx.Graph()
			for w in osmWaysNonClosed:
				wn = osmWaysNodes[w.attrib["id"]] # type: ignore
				for i in range(1, len(wn)):
					graph.add_edge(wn[i-1].attrib["ref"], wn[i].attrib["ref"])
			
			# Find "greatest" cycle (with most nodes)
			cycles = nx.cycle_basis(graph)
			if len(cycles) == 0:
				# No cycle : return first found node
				n = ods.nodes[osmWaysNodes[osmWaysNonClosed[0].attrib["id"]][0].attrib["ref"]] # type: ignore
				return n
			
			bestCycle = cycles[0]
			for c in cycles[1:]:
				if len(c) > len(bestCycle):
					bestCycle = c
			bestCycle.append(bestCycle[0])

			# Transform best cycle into a single WDM way
			osmBestWay = E.way()
			for nref in bestCycle:
				osmBestWay.append(E.nd(ref=nref))
	
	return osmBestWay


def getRoadSegmentAroundCrossing(osmNode: etree._Element, wds: model.WalkDataSet):
	"""For a given OSM node (corresponding to a pedestrian crossing),
	get a 3-node segment portion of the road it belongs to.

	It only works if node is on a straightforward road.

	Parameters
	----------
	osmNode : lxml.etree.Element
		The OSM node to check
	wds : model.WalkDataSet
		The WDM container

	Returns
	-------
	list | None
		List of 3 nodes found (second is the input node), or None if node is isolated or on too many ways
	"""

	nodeId = osmNode.attrib['id']

	# Find ways that contains the given node
	parentWays = list(wds.ods.nodeParentWays.get(nodeId, set()))
	nbParentWays = len(parentWays)

	def isFootway(way):
		return wds.ods.getTag(way, "highway") == "footway"

	# 0 or 1 parent
	if nbParentWays <= 1:
		if nbParentWays == 1:
			# Footway
			if isFootway(parentWays[0]):
				return None

			# Classic road
			wayNodes = parentWays[0].findall("nd")
			for pos, wn in enumerate(wayNodes):
				if wn.attrib["ref"] == nodeId:
					firstNodeId = wayNodes[pos-1].attrib["ref"] if pos - 1 >= 0 else None
					lastNodeId = wayNodes[pos+1].attrib["ref"] if pos + 1 < len(wayNodes) else None
					return [
						wds.ods.nodes.get(firstNodeId or "", None),
						osmNode,
						wds.ods.nodes.get(lastNodeId or "", None)
					]

		else:
			return None

	# 2 parents
	elif nbParentWays == 2:
		# Check if node is at start or end of both ways
		parentWays = sorted(parentWays, key=cmp_to_key(model.sortXml))
		w1nodes = parentWays[0].findall("nd")
		w2nodes = parentWays[1].findall("nd")
		isNodeW1Start = w1nodes[0].attrib["ref"] == nodeId
		isNodeW1End = w1nodes[len(w1nodes)-1].attrib["ref"] == nodeId
		isNodeW2Start = w2nodes[0].attrib["ref"] == nodeId
		isNodeW2End = w2nodes[len(w2nodes)-1].attrib["ref"] == nodeId
		if (
			not isFootway(parentWays[0])
			and not isFootway(parentWays[1])
			and (isNodeW1Start ^ isNodeW1End)
			and (isNodeW2Start ^ isNodeW2End)
		): # ^ = XOR
			beforeNodeId = w1nodes[1 if isNodeW1Start else len(w1nodes) - 2].attrib["ref"]
			beforeNode = wds.ods.nodes[beforeNodeId]
			afterNodeId = w2nodes[1 if isNodeW2Start else len(w2nodes) - 2].attrib["ref"]
			afterNode = wds.ods.nodes[afterNodeId]
			return [
				beforeNode,
				osmNode,
				afterNode
			]

		else:
			return None

	# More than 2 parents
	else:
		return None


def getAngle(n1: etree._Element, n2: etree._Element):
	"""Computes the angle between two nodes, in degrees.
	It handles the nodes being over a sphere instead of a plane for better accuracy.

	Parameters
	----------
	n1 : lxml.etree.Element
		The start node
	n2 : lxml.etree.Element
		The end node

	Returns
	-------
	float
		The angle in degrees (0-360°)
	"""

	lon1 = float(n1.attrib["lon"])
	lat1 = float(n1.attrib["lat"])
	lon2 = float(n2.attrib["lon"])
	lat2 = float(n2.attrib["lat"])

	ang = angles.r2d(angles.bear(angles.d2r(lon1), angles.d2r(lat1), angles.d2r(lon2), angles.d2r(lat2)))
	return ang + 360 if ang < 0 else ang


def projectPointAtAngle(n1: etree._Element, angle: float, distance: float):
	"""Gives the coordinates of a new point, projected at given distance and angle from origin node.

	Parameters
	----------
	n1 : lxml.etree.Element
		The origin node
	angle : float
		The angle to project at, in degrees clockwise
	distance : float
		The distance to project at, in meters

	Returns
	-------
	list
		Coordinates of projected point, as [lon, lat] (as fixed decimal string)
	"""

	lat = float(n1.attrib["lat"])
	lon = float(n1.attrib["lon"])
	R = 6378137 # Approx. Earth radius in meters

	# Offsets in meters
	dn = math.cos(math.radians(angle)) * distance
	de = math.sin(math.radians(angle)) * distance

	# Coordinate offsets in radians
	dLat = dn / R
	dLon = de / (R * math.cos(math.pi * lat / 180))

	#OffsetPosition, decimal degrees
	destLat = lat + dLat * 180 / math.pi
	destLon = lon + dLon * 180 / math.pi

	return [f"{destLon:0.7f}", f"{destLat:0.7f}"]


def getAreaUsefulNodes(areaOuterNodes, areaIds, xds):
	"""Find area contour nodes which are either tagged or connected to another linear way

	Parameters
	----------
	areaOuterNodes : etree.Element[]
		List of area contour nodes to check
	areaIds : str[]
		List of way IDs that composes the area
	xds : XmlDataSet
		The XML dataset to use
	
	Returns
	-------
	etree.Element[]
		List of useful area contour nodes
	"""

	areaUsefulNodes = []
	for i, node in enumerate(areaOuterNodes):
		if i == 0: # Skip first node, same as last one
			continue
		if node.find("tag") is not None:
			areaUsefulNodes.append(node)
		elif len(xds.nodeParentWays.get(node.attrib["id"], [])) > 1:
			# Check that any parent way is a linear path
			for npw in xds.nodeParentWays[node.attrib["id"]]:
				if npw.attrib["id"] not in areaIds:
					npwTags = xds.getTags(npw)
					if npwTags.get("highway") is not None and npwTags.get("area") != "yes":
						areaUsefulNodes.append(node)
						continue
	
	return areaUsefulNodes


def getTraversingWays(areaGeom, areaIds, areaUsefulNodes, xds):
	"""Find all ways connected to given area that cut through it.

	Parameters
	----------
	areaGeom : shapely.Geometry
		The geometry area
	areaIds : str[]
		List of way IDs that composes the area
	areaUsefulNodes : etree.Element[]
		List of area contour nodes that are "useful" (tagged or connected to other ways)
	xds : XmlDataSet
		The XML dataset to use
	
	Returns
	-------
	Set[etree.Element]
		Set of way that cut through the area
	"""

	traversingWays = set()
	for usefulNode in areaUsefulNodes:
		pws = xds.nodeParentWays.get(usefulNode.attrib["id"], set())
		for pw in pws:
			if pw.attrib["id"] not in areaIds and xds.getTag(pw, "highway") is not None:
				traversingWays.add(pw)
	for tw in list(traversingWays):
		geomTw = xmlToShapely(xds, tw, use4326=True)
		if not (geomTw.crosses(areaGeom) or areaGeom.covers(geomTw)):
			traversingWays.remove(tw)
	
	return traversingWays


def hasOtherFeaturesNear(ref: Geometry, otherFeatures: STRtree, distance: float):
	"""Checks if reference feature has at least one other feature at given distance

	Parameters
	----------
	ref : shapely.Geometry
		The reference feature
	otherFeatures : shapely.STRtree
		The set of other features to check
	distance : float
		The maximum distance we may find other features from reference feature (in meters)
		Distance = 0 means other feature should be contained in reference feature

	Returns
	-------
	bool
		True if at least one other feature is found nearby reference
	"""

	res = otherFeatures.query_nearest(ref, max_distance=max(distance, 0.0001), return_distance=(distance == 0), all_matches=False)

	if distance == 0 and len(res[0]) == 0:
		return False
	elif distance > 0 and len(res) == 0:
		return False
	else:
		# Check distance if query was 0
		if distance == 0:
			return res[1][0] == 0
		else:
			return True


def getNearestFeature(ref: Geometry, otherFeatures: STRtree, distance: float):
	"""Finds the nearest feature under given distance around given reference feature

	Parameters
	----------
	ref : shapely.Geometry
		The reference feature (in EPSG:4326)
	otherFeatures : shapely.STRtree
		The set of other features to look for (in EPSG:4326)
	distance : float
		The maximum distance we may find other features from reference feature (in meters)

	Returns
	-------
	int
		The index of found feature, or None if no result found
	"""

	distLat = distance / (TAU * POLAR_RADIUS / 360)
	distLon = distance / (TAU * EQUATORIAL_RADIUS / 360) / abs(math.cos(ref.centroid.y * (math.pi / 180)))
	distDeg = max(distLat, distLon)

	indices, distances = otherFeatures.query_nearest(ref, max_distance=distDeg, return_distance=True, all_matches=True)
	
	if len(indices) == 0:
		return None
	else:
		return indices[0]


def getProject(use4326: bool):
	def project(lon, lat):
		if use4326:
			return [float(lon), float(lat)]
		else:
			return TRANFORM_4326_3857.transform(lon, lat)
	return project


def xmlNodesToShapely(xmlNodes: List[etree._Element], use4326: bool = False):
	"""Transforms a list of XML nodes into a Shapely geometry

	Parameters
	----------
	xmlNodes : List[lxml.etree.Element]
		The list of XML nodes to use
	use4326 : bool
		Set to true to have output coordinates in WGS84 (EPSG:4326) instead of Pseudo-Mercator (EPSG:3857)

	Returns
	-------
	shapely.Geometry
		The generated geometry
	"""

	project = getProject(use4326)
	coords = []

	# Retrieve node from their reference in way
	for node in xmlNodes:
		if node is not None:
			coords.append(project(node.attrib["lon"], node.attrib["lat"]))

	# Transform according to definition
	if xmlNodes[0].attrib["id"] == xmlNodes[-1].attrib["id"]:
		return Polygon(coords)
	else:
		return LineString(coords)


def xmlToShapely(xds: model.XmlDataSet, xmlFeature: etree._Element, use4326: bool = False):
	"""Transforms an XML feature into a Shapely geometry

	Parameters
	----------
	xds : model.XmlDataSet
		The XML dataset
	xmlFeature : lxml.etree.Element
		The XML feature to copy geometry from
	use4326 : bool
		Set to true to have output coordinates in WGS84 (EPSG:4326) instead of Pseudo-Mercator (EPSG:3857)

	Returns
	-------
	shapely.Geometry
		The generated geometry
	"""

	project = getProject(use4326)

	if xmlFeature.tag == "node":
		x, y = project(xmlFeature.attrib["lon"], xmlFeature.attrib["lat"])
		return Point(x, y)

	elif xmlFeature.tag == "way":
		xmlNodes = [xds.nodes[n.attrib["ref"]] for n in xmlFeature.findall("nd") if n.attrib["ref"] in xds.nodes]
		if len(xmlNodes) < 2:
			print(etree.tostring(xmlFeature))
			raise Exception("Incomplete way: "+xmlFeature.attrib["id"])
		return xmlNodesToShapely(xmlNodes, use4326)

	else:
		raise Exception("Unsupported feature type: "+xmlFeature.tag)


def joinNodeToWay(wds: model.WalkDataSet, wdmNode: etree._Element, wdmWay: etree._Element):
	"""Creates a linking way between given node and way, and adds it to the WDM dataset

	Parameters
	----------
	wds : model.WalkDataSet
		The WDM container
	wdmNode : lxml.etree.Element
		The node to link
	wdmWay : lxml.etree.Element
		The way to join the node to

	Returns
	-------
	lxml.etree.Element
		The new linking way
	"""

	PROXIMITY_THRESHOLD = 0.01
	wdmNodeGeom = xmlToShapely(wds, wdmNode)
	wdmWayGeom = xmlToShapely(wds, wdmWay)

	# Find the shortest line between node and way
	joinLine = shortest_line(wdmNodeGeom, wdmWayGeom)
	wayNewNodeGeom = Point(joinLine.coords[1])

	# Look for appropriate list of coordinates from way
	wdmWayGeomCoords = None
	if wdmWayGeom.geom_type == "Polygon":
		wdmWayGeomCoords = wdmWayGeom.exterior.coords
	elif wdmWayGeom.geom_type in ["LinearRing", "LineString"]:
		wdmWayGeomCoords = wdmWayGeom.coords
	else:
		raise Exception("Unsupported way geometry")

	# Find where to inject the new node
	wayNewNode = None

	for wayNodeId in range(0, len(wdmWayGeomCoords)-1):
		n1 = Point(wdmWayGeomCoords[wayNodeId])
		n2 = Point(wdmWayGeomCoords[wayNodeId+1])

		# New node is exactly over start node of this segment
		if wayNodeId == 0 and equals_exact(n1, wayNewNodeGeom, PROXIMITY_THRESHOLD):
			wayNewNode = wayNodeId
			break

		# New node is exactly over end node of this segment
		elif equals_exact(n2, wayNewNodeGeom, PROXIMITY_THRESHOLD):
			wayNewNode = wayNodeId+1
			break

		# Check if new node is at the middle of this segment
		elif dwithin(LineString([n1, n2]), wayNewNodeGeom, PROXIMITY_THRESHOLD):
			wayNewNode = -(wayNodeId+1)
			break

	if wayNewNode is None:
		raise Exception("Can't find joining node on linestring")

	# Create joining line in WDM
	wdmWayNodes = wdmWay.findall("nd")
	wdmJoinLine = None

	# - Reuse existing nodes
	if wayNewNode >= 0:
		wdmJoinLine = E.way(
			E.nd(ref=wdmNode.attrib["id"]),
			E.nd(ref=wdmWayNodes[wayNewNode].attrib["ref"]),
			id=str(wds.getNextNewElementId("way"))
		)

	# - Create a new node in dest way
	else:
		# New node
		x, y = TRANFORM_4326_3857.transform(wayNewNodeGeom.x, wayNewNodeGeom.y, direction=TransformDirection.INVERSE)
		wdmWayNewNode = E.node(
			id=str(wds.getNextNewElementId("node")),
			lon=f"{x:0.7f}",
			lat=f"{y:0.7f}"
		)
		wds.insert(wdmWayNewNode)

		# Add ref in way
		wdmWay.insert(wdmWay.index(wdmWayNodes[-wayNewNode]), E.nd(ref=wdmWayNewNode.attrib["id"]))

		# Create join line
		wdmJoinLine = E.way(
			E.nd(ref=wdmNode.attrib["id"]),
			E.nd(ref=wdmWayNewNode.attrib["id"]),
			id=str(wds.getNextNewElementId("way"))
		)

	wds.insert(wdmJoinLine)

	return wdmJoinLine


def getGeometryWidthLength(geom: Geometry):
	"""Computes width and length of a geometry.

	Based on minimum bounding box, as illustrated here: https://gis.stackexchange.com/a/359025

	Parameters
	----------
	geom : shapely.Geometry
		The feature geometry (in EPSG:3857)

	Returns
	-------
	list[number]
		[width, length]
	"""

	bbox = geom.minimum_rotated_rectangle
	x,y = bbox.exterior.coords.xy

	p1 = TRANFORM_4326_3857.transform(x[0], y[0], direction=TransformDirection.INVERSE)
	p2 = TRANFORM_4326_3857.transform(x[1], y[1], direction=TransformDirection.INVERSE)
	p3 = TRANFORM_4326_3857.transform(x[2], y[2], direction=TransformDirection.INVERSE)

	def geoLatToMeters(dLat):
		return dLat * (TAU * POLAR_RADIUS / 360)

	def geoLonToMeters(dLon, atLat):
		if abs(atLat) >= 90:
			return 0
		return dLon * (TAU * EQUATORIAL_RADIUS / 360) * abs(math.cos(atLat * (math.pi / 180)))

	def geoSphericalDistance(a, b):
		x = geoLonToMeters(a[0] - b[0], (a[1] + b[1]) / 2)
		y = geoLatToMeters(a[1] - b[1])
		return math.sqrt((x * x) + (y * y))

	dists = (geoSphericalDistance(p1, p2), geoSphericalDistance(p2, p3))
	return [min(dists), max(dists)]
