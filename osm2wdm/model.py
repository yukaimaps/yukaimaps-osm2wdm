from dataclasses import dataclass, field
from lxml.builder import E # type: ignore
from lxml import etree
from typing import Dict, List, Set, Optional
from functools import cmp_to_key
from collections import OrderedDict
from . import tags
import logging
import math


def sortXml(e1, e2):
	"""Sorts OSM XML children nodes by type and ID"""
	if e1.tag == e2.tag:
		return -1 if int(e1.attrib['id']) < int(e2.attrib['id']) else 1
	elif e1.tag == "node":
		return -1
	elif e2.tag == "node":
		return 1
	elif e1.tag == "relation":
		return 1
	elif e2.tag == "relation":
		return -1
	else:
		return 0


@dataclass
class XmlDataSet:
	"""XML dataset container with helpers"""

	xml: etree._Element = field(default_factory=lambda: E.osm())
	nodes: OrderedDict[str, etree._Element] = field(default_factory=lambda: OrderedDict())
	ways: OrderedDict[str, etree._Element] = field(default_factory=lambda: OrderedDict())
	relations: OrderedDict[str, etree._Element] = field(default_factory=lambda: OrderedDict())
	tags: Dict[str, Dict[str, Dict[str,str]]] = field(default_factory=lambda: {"node": {}, "way": {}, "relation": {}})
	nodeParentWays: OrderedDict[str, Set[etree._Element]] = field(default_factory=lambda: OrderedDict())
	searches: Dict[str,List[etree._Element]] = field(default_factory=lambda: {})

	def __post_init__(self):
		# Put XML children in indexes
		for child in self.xml:
			if "id" in child.attrib:
				cid = child.attrib["id"]
				if child.tag == "node":
					self.nodes[cid] = child
				elif child.tag == "way":
					self.ways[cid] = child
				if child.tag == "relation":
					self.relations[cid] = child
		
		self.processNodesParentWays()
	
	def processNodesParentWays(self):
		for way in self.ways.values():
			for nd in way.iter("nd"):
				ndRef = nd.attrib["ref"]
				if not ndRef in self.nodeParentWays:
					self.nodeParentWays[ndRef] = set([way])
				else:
					self.nodeParentWays[ndRef].add(way)
	
	def getTag(self, feature: etree._Element, k: str, forceRead: bool = False):
		"""Find tag value for given feature"""
		return self.getTags(feature, forceRead).get(k)
	
	def getTags(self, feature: etree._Element, forceRead: bool = False):
		"""Get all tags for given feature (indexed)"""
		# Check if tags for this feature has been read already
		if forceRead or feature.attrib["id"] not in self.tags[feature.tag]:
			self.tags[feature.tag][feature.attrib["id"]] = tags.getAll(feature)
		return self.tags[feature.tag][feature.attrib["id"]]

	def search(self, k: str, v: Optional[str] = None, ftag: Optional[str] = None):
		"""Look for all features with matching k=v tag"""
		if k not in self.searches:
			self.searches[k] = self.xml.findall(tags.findallRule(k))
		if not ftag:
			return [ f for f in self.searches[k] if tags.hasAll(self.getTags(f), {k: v or '*'}) ]
		else:
			return [ f for f in self.searches[k] if f.tag == ftag and tags.hasAll(self.getTags(f), {k: v or '*'}) ]


@dataclass
class OsmDataSet(XmlDataSet):
	"""OSM XML dataset reader"""

	relationsByMember: Dict[str,Dict[str,Set[etree._Element]]] = field(default_factory=lambda: {"node": {}, "way": {}, "relation": {}})

	def __post_init__(self):
		XmlDataSet.__post_init__(self)

		# Index relation members
		for rel in self.relations.values():
			for relMember in rel.iter("member"):
				mref = relMember.attrib["ref"]
				if mref not in self.relationsByMember[relMember.attrib["type"]]:
					self.relationsByMember[relMember.attrib["type"]][mref] = set()
				self.relationsByMember[relMember.attrib["type"]][mref].add(rel)

	def isInRelation(self, feature: etree._Element, relationTags: dict[str, str]):
		"""Checks if a given feature is member of a certain type of relation (based on its tags)

		Parameters
		----------
		feature: lxml.etree._Element
			The feature to search for
		relationTags: dict[str, str]
			Tags that relation should have

		Returns
		-------
		bool
			True if feature is part of at least one matching relation
		"""

		relations = list(self.relationsByMember[feature.tag].get(feature.attrib["id"], set()))

		for r in relations:
			if tags.hasAll(self.getTags(r), relationTags):
				return True

		return False


@dataclass
class WalkDataSet(XmlDataSet):
	"""WDM XML dataset container with write helpers"""
	ods: OsmDataSet = field(default_factory=lambda: OsmDataSet(E.osm()))
	ids: Dict[str, int] = field(default_factory=lambda: {"node": 0, "way": 0, "relation": 0})
	importedOsmFeaturesById: Dict[str, Dict[str, Set[etree._Element]]] = field(default_factory=lambda: {"node": OrderedDict(), "way": OrderedDict(), "relation": OrderedDict()})
	importedSimplifiedOsmFeaturesById: Dict[str, Dict[str, Set[etree._Element]]] = field(default_factory=lambda: {"node": OrderedDict(), "way": OrderedDict(), "relation": OrderedDict()})
	osm2wdmNodesIds: OrderedDict[str,str] = field(default_factory=lambda: OrderedDict())

	def sort(self):
		self.xml[:] = sorted(self.xml, key=cmp_to_key(sortXml))

	def hasOsmFeatureImported(self, osmFeature: etree._Element, checkSimplifiedGeometries: bool = False, checkTags: bool = False) -> bool:
		"""Checks if a given OSM feature already has been imported in WDM dataset

		Parameters
		----------
		osmFeature : lxml.etree._Element
			The OSM feature to check
		checkSimplifiedGeometries : bool
			Should the test also look for simplified geometries (defaults to False)
		checkTags : bool
			Should the test verify that feature in WDM has meaningful tags (defaults to False)

		Returns
		-------
		bool
			True if OSM feature has already been imported
		"""

		# Classic features
		if osmFeature.attrib['id'] in self.importedOsmFeaturesById[osmFeature.tag]:
			if not checkTags:
				return True
			else:
				foundFeatures = self.importedOsmFeaturesById[osmFeature.tag][osmFeature.attrib['id']]
				foundFeature = next((f for f in foundFeatures if tags.hasMeaningfulWDMTags(f)), None)
				return foundFeature is not None
		# Simplified features
		elif checkSimplifiedGeometries and osmFeature.tag != "node" and osmFeature.attrib['id'] in self.importedSimplifiedOsmFeaturesById[osmFeature.tag]:
			if not checkTags:
				return True
			else:
				foundFeatures = self.importedSimplifiedOsmFeaturesById[osmFeature.tag][osmFeature.attrib['id']]
				foundFeature = next((f for f in foundFeatures if tags.hasMeaningfulWDMTags(f)), None)
				return foundFeature is not None
		else:
			return False

	def getNextNewElementId(self, elementType: str) -> int:
		"""Gets an ID for a new WDM element to append.

		Parameters
		----------
		elementType : str
			The type of element to check (node, way, relation)

		Returns
		-------
		int
			The new ID to use
		"""

		self.ids[elementType] -= 1
		return self.ids[elementType]

	def insert(self, feature):
		"""Adds given feature in WDM dataset

		Parameters
		----------
		feature: lxml.etree._Element
			The feature to add
		"""

		# Add OSM ID into list of imported features
		osmId = feature.find("tag[@k=\"Ref:Import:Id\"]")
		if osmId is not None:
			osmId = osmId.attrib["v"]
			osmType = "node" if osmId[0:1] == "n" else ("way" if osmId[0:1] == "w" else "relation")
			if osmId not in self.importedOsmFeaturesById[osmType]:
				self.importedOsmFeaturesById[osmType][osmId[1:]] = set([feature])
			else:
				self.importedOsmFeaturesById[osmType][osmId[1:]].add(feature)
		
		geomOsmId = feature.find("tag[@k=\"Ref:Import:Geometry:Id\"]")
		if geomOsmId is not None:
			geomOsmId = geomOsmId.attrib["v"]
			geomOsmType = "node" if geomOsmId[0:1] == "n" else ("way" if geomOsmId[0:1] == "w" else "relation")
			if osmId not in self.importedSimplifiedOsmFeaturesById[geomOsmType]:
				self.importedSimplifiedOsmFeaturesById[geomOsmType][geomOsmId[1:]] = set([feature])
			else:
				self.importedSimplifiedOsmFeaturesById[geomOsmType][geomOsmId[1:]].add(feature)

		# Add feature to indexes
		if feature.tag == "node":
			self.nodes[feature.attrib["id"]] = feature
		elif feature.tag == "way":
			self.ways[feature.attrib["id"]] = feature
		elif feature.tag == "relation":
			self.relations[feature.attrib["id"]] = feature

		self.xml.append(feature)


@dataclass
class Progress:
	logger: logging.Logger
	label: str
	total: int
	current: int = 0
	last_shown: Optional[int] = None

	def __post_init__(self):
		if self.total == 0:
			self.total = 1
		self.print()

	def tick(self):
		self.current += 1
		self.print()
	
	def print(self):
		pct = math.floor(self.current/self.total*100)
		if (self.total < 100 and pct % 2 == 0) or pct % 5 == 0:
			if self.last_shown is None or self.last_shown < pct:
				self.last_shown = pct
				self.logger.info(f"{self.label}: {pct}% ({self.current}/{self.total})")


class SkipFeatureProcess(Exception):
	"""Volontary exception to skip processing of a certain feature"""
