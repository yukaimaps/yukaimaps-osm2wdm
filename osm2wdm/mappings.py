#
# This file contains rules and mappings for converting
# OSM features into WDM
#

from typing import Dict, List, Any


######################################
# Tag values mappings
#

TAG_YESNOBAD: Dict[str, str] = {"yes": "Yes", "designated": "Yes", "limited": "Limited", "bad": "Limited", "no": "No"}
TAG_YESNO: Dict[str, str] = {"yes": "Yes", "no": "No"}
TAG_YESNO_WITH_CONTRASTED: Dict[str, str] = {"yes": "Yes", "no": "No", "contrasted": "Yes"}
TAG_SMOOTHNESS: Dict[str, str] = {
	"excellent": "Good", "good": "Good",
	"intermediate": "Worn",
	"bad": "Discomfortable", "very_bad": "Discomfortable",
	"horrible": "Hazardous", "very_horrible": "Hazardous", "impassable": "Hazardous"
}
TAG_SURFACE: Dict[str, str] = {
	"asphalt": "Asphalt", "tarmac": "Asphalt",
	"concrete": "Concrete", "concrete:plates": "Concrete", "concrete:lanes": "Concrete", "cement": "Concrete",
	"wood": "Wood", "compacted": "Sand",
	"dirt": "Earth", "ground": "Earth", "unpaved": "Earth", "sand": "Earth",
	"earth": "Earth", "clay": "Earth", "mud": "Earth", "stepping_stone": "Earth",
	"snow": "Earth", "soil": "Earth",
	"gravel": "Gravel", "fine_gravel": "Gravel", "pebblestone": "Gravel", "chipseal": "Gravel",
	"grass": "Grass", "artificial_turf": "Grass",
	"plastic": "PlasticMatting", "linoleum": "PlasticMatting",
	"metal": "SteelPlate", "metal_grid": "SteelPlate",
	"stone": "Stone", "rock": "Stone", "rocks": "Stone", "bare_rock": "Stone",
	"paving_stones": "PavingStones", "sett": "PavingStones", "cobblestone": "PavingStones",
	"unhewn_cobblestone": "PavingStones", "paving_stones:30": "PavingStones", "bricks": "PavingStones",
	"carpet": "Carpet", "rubber": "Rubber", "grass_paver": "FibreglassGrating",
 	"tartan": "Other", "woodchips": "Other", "decoturf": "Other", "acrylic": "Other"
}
TAG_STEP_CONTRAST: Dict[str, str] = {"yes": "Yes", "bad": "Yes", "no": "No"}
TAG_AUTOMATIC_DOOR: Dict[str, str] = {
	"no": "No", "slowdown_button": "Yes", "continuous": "Yes",
	"floor": "Yes", "motion": "Yes", "yes": "Yes",
}
TAG_DOORHANDLE: Dict[str, str] = {
	"knob": "Knob", "lever": "Lever", "crash_bar": "CrashBar", "landing": "Landing",
	"button": "Button", "grab_rail": "GrabRail", "pull": "GrabRail", "vertical": "Vertical",
	"window_lever": "WindowLever", "no": "None", "*": "Other"
}
TAG_KERB: Dict[str, str] = {"raised": "Raised", "rolled": "Raised", "lowered": "Lowered", "flush": "Flush", "no": "None"}


######################################
# Tag values listings
#

TAG_CROSSING_MARKINGS_FOR_VISUAL_GUIDANCE: List[str] = [
	"surface", "lines", "zebra:double", "ladder",
	"ladder:skewed", "ladder:paired", "dashes",
	"dots", "pictograms"
]
TAG_WDM_NOT_MEANINGFUL: List[str] = [
	"Ref:Import:Id", "Ref:Import:Geometry:Id", "Ref:Import:Source", "Ref:Import:Version"
]

######################################
# Features mappings
#

POI_OSM_TO_WDM: List[Dict[str,Any]] = [
	# Sports
	{ "osm_key": "sport", "osm_val": "climbing", "wdm_tags": { "PointOfInterest": "Sport", "Sport": "ClimbingCenter" } },
	{ "osm_key": "sport", "osm_val": "swimming", "wdm_tags": { "PointOfInterest": "Sport", "Sport": "SwimmingCenter" } },
	{ "osm_key": "leisure", "osm_val": "sports_centre", "wdm_tags": { "PointOfInterest": "Sport", "Sport": "SportCenter" } },
	{ "osm_key": "leisure", "osm_val": "sports_hall", "wdm_tags": { "PointOfInterest": "Sport", "Sport": "SportCenter" } },
	{ "osm_key": "leisure", "osm_val": "fitness_centre", "wdm_tags": { "PointOfInterest": "Sport", "Sport": "FitnessCenter" } },
	{ "osm_key": "leisure", "osm_val": "ice_rink", "wdm_tags": { "PointOfInterest": "Sport", "Sport": "IceCenter" } },
	{ "osm_key": "leisure", "osm_val": "stadium", "wdm_tags": { "PointOfInterest": "Sport", "Sport": "Stadium" } },
	{ "osm_key": "leisure", "osm_val": "horse_riding", "wdm_tags": { "PointOfInterest": "Sport", "Sport": "HorseRidingCenter" } },
	{ "osm_key": "leisure", "osm_val": "bowling_alley", "wdm_tags": { "PointOfInterest": "Sport", "Sport": "BowlingCenter" } },
	{ "osm_key": "leisure", "osm_val": "dance", "wdm_tags": { "PointOfInterest": "Sport", "Sport": "DancingCenter" } },
	{ "osm_key": "leisure", "osm_val": "golf_course", "wdm_tags": { "PointOfInterest": "Sport", "Sport": "GolfingCenter" } },

	# Game
	{ "osm_key": "leisure", "osm_val": "adult_gaming_centre", "wdm_tags": { "PointOfInterest": "Game", "Game": "AdultGamingCenter" } },
	{ "osm_key": "amenity", "osm_val": "casino", "wdm_tags": { "PointOfInterest": "Game", "Game": "AdultGamingCenter" } },
	{ "osm_key": "leisure", "osm_val": "amusement_arcade", "wdm_tags": { "PointOfInterest": "Game", "Game": "ArcadeCenter" } },
	{ "osm_key": "leisure", "osm_val": "escape_game", "wdm_tags": { "PointOfInterest": "Game", "Game": "EscapeRoom" } },
	{ "osm_key": "leisure", "osm_val": "hackerspace", "wdm_tags": { "PointOfInterest": "Game", "Game": "Hackerspace" } },
	{ "osm_key": "leisure", "osm_val": "resort", "wdm_tags": { "PointOfInterest": "Game", "Game": "Resort" } },
	{ "osm_key": "tourism", "osm_val": "theme_park", "wdm_tags": { "PointOfInterest": "Game", "Game": "ThemePark" } },

	# Education
	{ "osm_key": "school:FR", "osm_val": "maternelle", "wdm_tags": { "PointOfInterest": "Education", "Education": "EarlyChildhood" } },
	{ "osm_key": "school:FR", "osm_val": "élémentaire", "wdm_tags": { "PointOfInterest": "Education", "Education": "Elementary" } },
	{ "osm_key": "school:FR", "osm_val": "primaire", "wdm_tags": { "PointOfInterest": "Education", "Education": "Primary" } },
	{ "osm_key": "school:FR", "osm_val": "collège", "wdm_tags": { "PointOfInterest": "Education", "Education": "MiddleSchool" } },
	{ "osm_key": "school:FR", "osm_val": "lycée", "wdm_tags": { "PointOfInterest": "Education", "Education": "HighSchool" } },
	{ "osm_key": "school:FR", "osm_val": "secondaire", "wdm_tags": { "PointOfInterest": "Education", "Education": "Secondary" } },
	{ "osm_key": "amenity", "osm_val": "university", "wdm_tags": { "PointOfInterest": "Education", "Education": "University" } },
	{ "osm_key": "amenity", "osm_val": "college", "wdm_tags": { "PointOfInterest": "Education", "Education": "College" } },
	{ "osm_key": "amenity", "osm_val": "kindergaten", "wdm_tags": { "PointOfInterest": "Education", "Education": "Kindergarten" } },
	{ "osm_key": "amenity", "osm_val": "school", "without_osm_key": "school:FR", "wdm_tags": { "PointOfInterest": "Education" } },

	# Religion
	{ "osm_key": "religion", "osm_val": "buddhist", "wdm_tags": { "PointOfInterest": "Religion", "Religion": "Buddhist" } },
	{ "osm_key": "religion", "osm_val": "christian", "wdm_tags": { "PointOfInterest": "Religion", "Religion": "Christian" } },
	{ "osm_key": "religion", "osm_val": "hindu", "wdm_tags": { "PointOfInterest": "Religion", "Religion": "Hindu" } },
	{ "osm_key": "religion", "osm_val": "jewish", "wdm_tags": { "PointOfInterest": "Religion", "Religion": "Jewish" } },
	{ "osm_key": "religion", "osm_val": "muslim", "wdm_tags": { "PointOfInterest": "Religion", "Religion": "Muslim" } },
	{ "osm_key": "religion", "osm_val": "shinto", "wdm_tags": { "PointOfInterest": "Religion", "Religion": "Shinto" } },
	{ "osm_key": "religion", "osm_val": "sikh", "wdm_tags": { "PointOfInterest": "Religion", "Religion": "Sikh" } },
	{ "osm_key": "religion", "osm_val": "taoist", "wdm_tags": { "PointOfInterest": "Religion", "Religion": "Taoist" } },
	{ "osm_key": "amenity", "osm_val": "place_of_worship", "without_osm_key": "religion", "wdm_tags": { "PointOfInterest": "Religion" } },

	# Theaters
	{ "osm_key": "amenity", "osm_val": "theatre", "wdm_tags": { "PointOfInterest": "Theater", "Theater": "Theater" } },
	{ "osm_key": "amenity", "osm_val": "cinema", "wdm_tags": { "PointOfInterest": "Theater", "Theater": "Cinema" } },
	{ "osm_key": "amenity", "osm_val": "community_centre", "wdm_tags": { "PointOfInterest": "Theater", "Theater": "CommunityCentre" } },

	# Museums
	{ "osm_key": "tourism", "osm_val": "museum", "wdm_tags": { "PointOfInterest": "Museum", "Museum": "Museum" } },
	{ "osm_key": "tourism", "osm_val": "aquarium", "wdm_tags": { "PointOfInterest": "Museum", "Museum": "Aquarium" } },
	{ "osm_key": "tourism", "osm_val": "zoo", "wdm_tags": { "PointOfInterest": "Museum", "Museum": "Zoo" } },

	# Exhibitions
	{ "osm_key": "tourism", "osm_val": "arts_centre", "wdm_tags": { "PointOfInterest": "Exhibition", "Exhibition": "ArtsCentre" } },
	{ "osm_key": "tourism", "osm_val": "gallery", "wdm_tags": { "PointOfInterest": "Exhibition", "Exhibition": "Gallery" } },

	# Libraries
	{ "osm_key": "amenity", "osm_val": "library", "wdm_tags": { "PointOfInterest": "Library", "Library": "Books" } },
	{ "osm_key": "amenity", "osm_val": "toy_library", "wdm_tags": { "PointOfInterest": "Library", "Library": "Games" } },

	# Restaurant
	{ "osm_key": "amenity", "osm_val": "pub", "wdm_tags": { "PointOfInterest": "Restaurant", "Restaurant": "Pub" } },
	{ "osm_key": "amenity", "osm_val": "fast_food", "wdm_tags": { "PointOfInterest": "Restaurant", "Restaurant": "FastFood" } },
	{ "osm_key": "amenity", "osm_val": "bar", "wdm_tags": { "PointOfInterest": "Restaurant", "Restaurant": "Bar" } },
	{ "osm_key": "amenity", "osm_val": "cafe", "wdm_tags": { "PointOfInterest": "Restaurant", "Restaurant": "Cafe" } },
	{ "osm_key": "amenity", "osm_val": "food_court", "wdm_tags": { "PointOfInterest": "Restaurant", "Restaurant": "FoodCourt" } },
	{ "osm_key": "amenity", "osm_val": "ice_cream", "wdm_tags": { "PointOfInterest": "Restaurant", "Restaurant": "IceCream" } },
	{ "osm_key": "amenity", "osm_val": "restaurant", "wdm_tags": { "PointOfInterest": "Restaurant", "Restaurant": "Restaurant" } },

	# Healthcare
	{ "osm_key": "healthcare", "osm_val": "laboratory", "wdm_tags": { "PointOfInterest": "Healthcare", "Healthcare": "Laboratory" } },
	{ "osm_key": "healthcare", "osm_val": "sample_collection", "wdm_tags": { "PointOfInterest": "Healthcare", "Healthcare": "Laboratory" } },
	{ "osm_key": "amenity", "osm_val": "doctors", "wdm_tags": { "PointOfInterest": "Healthcare", "Healthcare": "Doctor" } },
	{ "osm_key": "amenity", "osm_val": "dentist", "wdm_tags": { "PointOfInterest": "Healthcare", "Healthcare": "Doctor" } },
	{ "osm_key": "healthcare", "osm_val": "doctor", "wdm_tags": { "PointOfInterest": "Healthcare", "Healthcare": "Doctor" } },
	{ "osm_key": "amenity", "osm_val": "hospital", "wdm_tags": { "PointOfInterest": "Healthcare", "Healthcare": "Hospital" } },
	{ "osm_key": "healthcare", "osm_val": "hospital", "wdm_tags": { "PointOfInterest": "Healthcare", "Healthcare": "Hospital" } },
	{ "osm_key": "amenity", "osm_val": "clinic", "wdm_tags": { "PointOfInterest": "Healthcare", "Healthcare": "Clinic" } },
	{ "osm_key": "healthcare", "osm_val": "clinic", "wdm_tags": { "PointOfInterest": "Healthcare", "Healthcare": "Clinic" } },

	# Senior & DisabledCare
	{ "osm_key": "social_facility", "osm_val": "group_home", "wdm_tags": { "Lodging": "GroupHome" } },
	{ "osm_key": "social_facility", "osm_val": "assisted_living", "wdm_tags": { "Lodging": "AssistedLiving" } },
	{ "osm_key": "social_facility", "osm_val": "nursing_home", "wdm_tags": { "Lodging": "NursingHome" } },

	# Hotels
	{ "osm_key": "tourism", "osm_val": "apartment", "wdm_tags": { "PointOfInterest": "Lodging", "Lodging": "Apartment" } },
	{ "osm_key": "tourism", "osm_val": "camp_site", "wdm_tags": { "PointOfInterest": "Lodging", "Lodging": "CampSite" } },
	{ "osm_key": "tourism", "osm_val": "chalet", "wdm_tags": { "PointOfInterest": "Lodging", "Lodging": "Chalet" } },
	{ "osm_key": "tourism", "osm_val": "hostel", "wdm_tags": { "PointOfInterest": "Lodging", "Lodging": "Hostel" } },
	{ "osm_key": "tourism", "osm_val": "hotel", "wdm_tags": { "PointOfInterest": "Lodging", "Lodging": "Hotel" } },
	{ "osm_key": "tourism", "osm_val": "motel", "wdm_tags": { "PointOfInterest": "Lodging", "Lodging": "Motel" } },
	{ "osm_key": "tourism", "osm_val": "wilderness_hut", "wdm_tags": { "PointOfInterest": "MountainShelter", "MountainShelter": "Wilderness" } },
	{ "osm_key": "tourism", "osm_val": "alpine_hut", "wdm_tags": { "PointOfInterest": "MountainShelter", "MountainShelter": "Alpine" } },

	# Government
	{ "osm_key": "amenity", "osm_val": "townhall", "wdm_tags": { "PointOfInterest": "Government", "Government": "Townhall" } },
	{ "osm_key": "government", "osm_val": "local_authority", "wdm_tags": { "PointOfInterest": "Government", "Government": "LocalAuthority" } },
	{ "osm_key": "government", "osm_val": "parliament", "wdm_tags": { "PointOfInterest": "Government", "Government": "Parliament" } },
	{ "osm_key": "government", "osm_val": "prefecture", "wdm_tags": { "PointOfInterest": "Government", "Government": "Prefecture" } },
	{ "osm_key": "government", "osm_val": "social_welfare", "wdm_tags": { "PointOfInterest": "Government", "Government": "SocialWelfare" } },
	{ "osm_key": "government", "osm_val": "social_security", "wdm_tags": { "PointOfInterest": "Government", "Government": "SocialSecurity" } },
	{ "osm_key": "government", "osm_val": "tax", "wdm_tags": { "PointOfInterest": "Government", "Government": "Tax" } },
	{ "osm_key": "government", "osm_val": "audit", "wdm_tags": { "PointOfInterest": "Government", "Government": "Audit" } },
	{ "osm_key": "amenity", "osm_val": "police", "wdm_tags": { "PointOfInterest": "Government", "Government": "Police" } },
	{ "osm_key": "amenity", "osm_val": "post_office", "wdm_tags": { "PointOfInterest": "Government", "Government": "Post" } },
	{ "osm_key": "amenity", "osm_val": "courthouse", "wdm_tags": { "PointOfInterest": "Government", "Government": "CourtHouse" } },
	{ "osm_key": "amenity", "osm_val": "chamber_of_commerce", "wdm_tags": { "PointOfInterest": "Government", "Government": "ChamberOfCommerce" } },
	{ "osm_key": "government", "osm_val": "customs", "wdm_tags": { "PointOfInterest": "Government", "Government": "Customs" } },
	{ "osm_key": "office", "osm_val": "diplomatic", "wdm_tags": { "PointOfInterest": "Government", "Government": "Diplomatic" } },
	{ "osm_key": "office", "osm_val": "government", "wdm_tags": { "PointOfInterest": "Government"} },

	# ParkingLots
	{ "osm_key": "amenity", "osm_val": "parking", "wdm_tags": { "PointOfInterest": "ParkingLots" } },

	# Banks
	{ "osm_key": "amenity", "osm_val": "bank", "wdm_tags": { "PointOfInterest": "Bank" } },

	# Shops
	{ "osm_key": "office", "osm_val": "estate_agent", "wdm_tags": { "Shop": "EstateAgent", "PointOfInterest": "Shop" } },
	{ "osm_key": "office", "osm_val": "guide", "wdm_tags": { "Shop": "Guide", "PointOfInterest": "Shop" } },
	{ "osm_key": "office", "osm_val": "insurance", "wdm_tags": { "Shop": "Insurance", "PointOfInterest": "Shop" } },
	{ "osm_key": "office", "osm_val": "moving_company", "wdm_tags": { "Shop": "MovingCompany", "PointOfInterest": "Shop" } },
	{ "osm_key": "amenity", "osm_val": "boat_rental", "wdm_tags": { "Shop": "BoatRental", "PointOfInterest": "Shop" } },
	{ "osm_key": "amenity", "osm_val": "bureau_de_change", "wdm_tags": { "Shop": "BureauDeChange", "PointOfInterest": "Shop" } },
	{ "osm_key": "amenity", "osm_val": "car_rental", "wdm_tags": { "Shop": "CarRental", "PointOfInterest": "Shop" } },
	{ "osm_key": "amenity", "osm_val": "car_wash", "wdm_tags": { "Shop": "CarWash", "PointOfInterest": "Shop" } },
	{ "osm_key": "amenity", "osm_val": "crematorium", "wdm_tags": { "Shop": "Crematorium", "PointOfInterest": "Shop" } },
	{ "osm_key": "amenity", "osm_val": "driving_school", "wdm_tags": { "Shop": "DrivingSchool", "PointOfInterest": "Shop" } },
	{ "osm_key": "amenity", "osm_val": "fuel", "wdm_tags": { "Shop": "Fuel", "PointOfInterest": "Shop" } },
	{ "osm_key": "amenity", "osm_val": "internet_cafe", "wdm_tags": { "Shop": "InternetCafe", "PointOfInterest": "Shop" } },
	{ "osm_key": "amenity", "osm_val": "nightclub", "wdm_tags": { "Shop": "Nightclub", "PointOfInterest": "Shop" } },
	{ "osm_key": "amenity", "osm_val": "pharmacy", "wdm_tags": { "Shop": "Pharmacy", "PointOfInterest": "Shop" } },
	{ "osm_key": "amenity", "osm_val": "vehicle_inspection", "wdm_tags": { "Shop": "VehicleInspection", "PointOfInterest": "Shop" } },
	{ "osm_key": "office", "osm_val": "employment_agency", "wdm_tags": { "Shop": "EmploymentAgency", "PointOfInterest": "Shop" } },
	{ "osm_key": "shop", "wdm_tags": { "PointOfInterest": "Shop" } },
]

QUAY_OSMBUS_TO_WDM: List[Dict[str,str]] = [
	{ "osmType": "bus", "wdmQuay": "Bus" },
	{ "osmType": "coach", "wdmQuay": "Coach" },
	{ "osmType": "trolleybus", "wdmQuay": "TrolleyBus" },
]

QUAY_OSMRAIL_TO_WDM: List[Dict[str,Any]] = [
	# Tram
	{ "osmType": "tram", "onStandalone": False, "wdmQuay": "Tram", "wdmInsideSpace": False },
	{ "osmType": "light_rail", "onStandalone": False, "wdmQuay": "Tram", "wdmInsideSpace": False },

	# Train
	{ "osmType": "train", "onStandalone": True, "modeTagOnStandalone": True, "wdmQuay": "Rail", "wdmInsideSpace": True },

	# Metro
	{ "osmType": "subway", "onStandalone": True, "modeTagOnStandalone": True, "wdmQuay": "Metro", "wdmInsideSpace": True },
	{ "osmType": "monorail", "onStandalone": True, "modeTagOnStandalone": True, "wdmQuay": "Metro", "wdmInsideSpace": True },
]

QUAY_OSMPT_TO_WDM: List[Dict[str,Any]] = [
	# Bus
	{ "osmType": "bus", "simplify": True, "wdmQuay": "Bus", "wdmInsideSpace": False },
	{ "osmType": "coach", "simplify": True, "wdmQuay": "Coach", "wdmInsideSpace": False },
	{ "osmType": "trolleybus", "simplify": True, "wdmQuay": "TrolleyBus", "wdmInsideSpace": False },

	# Ferry
	{ "osmType": "ferry", "simplify": True, "wdmQuay": "Water", "wdmInsideSpace": False },

	# Tram
	{ "osmType": "tram", "simplify": False, "wdmQuay": "Tram", "wdmInsideSpace": False },
	{ "osmType": "light_rail", "simplify": False, "wdmQuay": "Tram", "wdmInsideSpace": False },

	# Train
	{ "osmType": "train", "simplify": False, "wdmQuay": "Rail", "wdmInsideSpace": True },

	# Metro
	{ "osmType": "subway", "simplify": False, "wdmQuay": "Metro", "wdmInsideSpace": True },
	{ "osmType": "monorail", "simplify": False, "wdmQuay": "Metro", "wdmInsideSpace": True },

	# Funicular
	{ "osmType": "funicular", "osmTags": None, "simplify": True, "wdmQuay": "Funicular", "wdmInsideSpace": False },

	# CableWay
	{ "osmType": "aerialway", "osmTags": None, "simplify": True, "wdmQuay": "CableWay", "wdmInsideSpace": False }
]

STATION_OSM_TO_WDM: List[Dict[str,Any]] = [
	# public_transport=station
	{ "osm_key": "public_transport", "osm_val": "station", "mappings": [
		# Bus
		{ "osmTags": {"station": "bus"}, "wdmType": "BusStation" },
		{ "osmTags": {"bus": "yes"}, "wdmType": "BusStation" },
		{ "osmTags": {"coach": "yes"}, "wdmType": "CoachStation" },
		# Metro
		{ "osmTags": {"station": "subway"}, "wdmType": "MetroStation" },
		{ "osmTags": {"station": "monorail"}, "wdmType": "MetroStation" },
		{ "osmTags": {"subway": "yes"}, "wdmType": "MetroStation" },
		{ "osmTags": {"subway": "monorail"}, "wdmType": "MetroStation" },
		# Tram
		{ "osmTags": {"station": "tram"}, "wdmType": "TramStation" },
		{ "osmTags": {"station": "light_rail"}, "wdmType": "TramStation" },
		# Rail
		{ "osmTags": {"station": "train"}, "wdmType": "RailStation" },
		{ "osmTags": {"train": "yes"}, "wdmType": "RailStation" },
		# Others
		{ "osmTags": {"station": "ferry"}, "wdmType": "FerryPort" },
		{ "osmTags": {"ferry": "yes"}, "wdmType": "FerryPort" },
		{ "osmTags": {"station": "aerialway" }, "wdmType": "LiftStation" },
		{ "osmTags": {"aerialway": "yes" }, "wdmType": "LiftStation" },
	]},

	# railway=station
	{ "osm_key": "railway", "osm_val": "station", "mappings": [
		{ "osmTags": {"station": "subway"}, "wdmType": "MetroStation" },
		{ "osmTags": {"station": "monorail"}, "wdmType": "MetroStation" },
		{ "osmTags": {"subway": "yes"}, "wdmType": "MetroStation" },
		{ "osmTags": {"subway": "monorail"}, "wdmType": "MetroStation" },
		{ "osmTags": {"station": "tram"}, "wdmType": "TramStation" },
		{ "osmTags": {"station": "light_rail"}, "wdmType": "TramStation" },
		{ "osmTags": {"light_rail": "yes"}, "wdmType": "TramStation" },
		{ "osmTags": {"tram": "yes"}, "wdmType": "TramStation" },
		{ "osmTags": {"station": "train"}, "wdmType": "RailStation" },
		{ "osmTags": {"train": "yes"}, "wdmType": "RailStation" },
	]},

	# railway=halt
	{ "osm_key": "railway", "osm_val": "halt", "mappings": [
		{ "osmTags": {"station": "train"}, "wdmType": "RailStation" },
		{ "osmTags": {"train": "yes"}, "wdmType": "RailStation" },
	]},

	# amenity=bus_station
	{ "osm_key": "amenity", "osm_val": "bus_station", "mappings": [
		{ "osmTags": {}, "wdmType": "BusStation" },
	]},

	# aerialway=station
	{ "osm_key": "aerialway", "osm_val": "station", "mappings": [
		{ "osmTags": {}, "wdmType": "LiftStation" },
	]},

	# aeroway=aerodrome
	{ "osm_key": "aeroway", "osm_val": "aerodrome", "mappings": [
		{ "osmTags": { "aerodrome:type": "regional" }, "wdmType": "Airport" },
		{ "osmTags": { "aerodrome:type": "international" }, "wdmType": "Airport" }
	]},
]

OBSTACLE_OSM_TO_WDM: List[Dict[str,Any]] = [
	{
		"osm_key": "kerb", "osm_val": "raised",
		"obstacleTags": { "Obstacle": "Kerb", "ObstacleType": "Surface" },
		"obstacleLocation": "on_path"
	},
	{
		"osm_key": "amenity", "osm_val": "toilets",
		"wdmTags": { "Amenity": "Toilets" },
		"obstacleTags": { "Obstacle": "Toilets", "ObstacleType": "Ground" },
		"obstacleLocation": "near_path"
	},
	{
		"osm_key": "amenity", "osm_val": "shower",
		"wdmTags": { "Amenity": "Toilets" },
		"obstacleTags": { "Obstacle": "Toilets", "ObstacleType": "Ground" },
		"obstacleLocation": "near_path"
	},
	{
		"osm_key": "amenity", "osm_val": "shelter",
		"wdmTags": { "Amenity": "Shelter" },
		"obstacleTags": { "Obstacle": "Shelter" },
		"obstacleLocation": "near_path"
	},
	{
		"osm_key": "emergency", "osm_val": "phone",
		"wdmTags": { "Amenity": "EmergencyPhone" },
		"obstacleTags": { "Obstacle": "Yes", "ObstacleType": "Ground" },
		"obstacleLocation": "near_path"
	},
	# Ticket office SHOULD stay before reception desk
	{
		"osm_key": "tickets:public_transport",
		"wdmTags": { "Amenity": "TicketOffice" }
	},
	{
		"osm_key": "amenity", "osm_val": "reception_desk",
		"wdmTags": { "Amenity": "ReceptionDesk" }
	},
	{
		"osm_key": "information", "osm_val": "office",
		"wdmTags": { "Amenity": "ReceptionDesk" }
	},
	{
		"osm_key": "information", "osm_val": "visitor_centre",
		"wdmTags": { "Amenity": "ReceptionDesk" }
	},
	{
		"osm_key": "information",
		"wdmTags": { "Amenity": "Sign" },
		"obstacleTags": { "Obstacle": "Yes", "ObstacleType": "Ground" },
		"obstacleLocation": "near_path"
	},
	{
		"osm_key": "amenity", "osm_val": "post_box",
		"wdmTags": { "Amenity": "PostBox" },
		"obstacleTags": { "Obstacle": "PostBox", "ObstacleType": "Ground" }
	},
	{
		"osm_key": "emergency", "osm_val": "defibrillator",
		"wdmTags": { "Amenity": "Defibrillator" }
	},
	{
		"osm_key": "man_made", "osm_val": "utility_pole",
		"obstacleTags": { "Obstacle": "Pole", "ObstacleType": "Ground" },
		"obstacleLocation": "near_path"
	},
		{
		"osm_key": "power", "osm_val": "pole",
		"obstacleTags": { "Obstacle": "Pole", "ObstacleType": "Ground" },
		"obstacleLocation": "near_path"
	},
	{
		"osm_key": "amenity", "osm_val": "bench",
		"wdmTags": { "Amenity": "Seating" },
		"obstacleTags": { "Obstacle": "Bench", "ObstacleType": "Ground" },
		"obstacleLocation": "near_path"
	},
	{
		"osm_key": "amenity", "osm_val": "waste_basket",
		"wdmTags": { "Amenity": "RubbishDisposal" },
		"obstacleTags": { "Obstacle": "Yes", "ObstacleType": "Ground" },
		"obstacleLocation": "near_path"
	},
	{
		"osm_key": "amenity", "osm_val": "trolley_bay",
		"wdmTags": { "Amenity": "TrolleyStand" },
		"obstacleTags": { "Obstacle": "Yes" },
		"obstacleLocation": "near_path"
	},
	{
		"osm_key": "amenity", "osm_val": "ticket_validator",
		"wdmTags": { "Amenity": "TicketValidator" }
	},
	{
		"osm_key": "barrier", "osm_val": "turnstile",
		"wdmTags": { "Amenity": "TicketValidator" },
		"obstacleTags": { "Obstacle": "Turnstile", "ObstacleType": "Pass" },
		"obstacleLocation": "near_path"
	},
	{
		"osm_key": "amenity", "osm_val": "luggage_locker",
		"wdmTags": { "Amenity": "LuggageLocker" },
		"obstacleTags": { "Obstacle": "Yes" },
		"obstacleLocation": "near_path"
	},
	{
		"osm_key": "vending", "osm_val": "public_transport_tickets",
		"wdmTags": { "Amenity": "TicketVendingMachine" },
		"obstacleTags": { "Obstacle": "Yes", "ObstacleType": "Ground" },
		"obstacleLocation": "near_path"
	},
	{
		"osm_key": "amenity", "osm_val": "meeting_point",
		"wdmTags": { "Amenity": "MeetingPoint" },
		"obstacleTags": { "Obstacle": "Yes", "ObstacleType": "Ground" },
		"obstacleLocation": "near_path"
	},
	{
		"osm_key": "natural", "osm_val": "tree",
		"obstacleTags": { "Obstacle": "Vegetation", "ObstacleType": "Ground" },
		"obstacleLocation": "near_path"
	},
	{
		"osm_key": "barrier", "osm_val": "planter",
		"obstacleTags": { "Obstacle": "Vegetation", "ObstacleType": "Ground" },
		"obstacleLocation": "near_path"
	},
	{
		"osm_key": "highway", "osm_val": "street_lamp",
		"obstacleTags": { "Obstacle": "Pole", "Electrified": "Yes" },
		"obstacleLocation": "near_path"
	},
	{
		"osm_key": "barrier", "osm_val": "bollard",
		"obstacleTags": { "Obstacle": "Bollard", "ObstacleType": "Ground" },
		"obstacleLocation": "near_path"
	},
	{
		"osm_key": "barrier", "osm_val": "block",
		"obstacleTags": { "Obstacle": "Bollard", "ObstacleType": "Ground" },
		"obstacleLocation": "near_path"
	},
	{
		"osm_key": "man_made", "osm_val": "street_cabinet",
		"obstacleTags": { "Obstacle": "StreetFurniture", "ObstacleType": "Ground" },
		"obstacleLocation": "near_path"
	},
	{
		"osm_key": "man_made", "osm_val": "planter",
		"obstacleTags": { "Obstacle": "Planter", "ObstacleType": "Ground" },
		"obstacleLocation": "near_path"
	},
	{
		"osm_key": "man_made", "osm_val": "manhole",
		"obstacleTags": { "Obstacle": "DrainHole", "ObstacleType": "Ground" },
		"obstacleLocation": "near_path"
	},
	{
		"osm_key": "barrier", "osm_val": "cycle_barrier",
		"obstacleTags": { "Obstacle": "CycleBarrier", "ObstacleType": "Pass" },
		"obstacleLocation": "near_path"
	},
	{
		"osm_key": "barrier", "osm_val": "motorcycle_barrier",
		"obstacleTags": { "Obstacle": "CycleBarrier", "ObstacleType": "Pass" },
		"obstacleLocation": "near_path"
	},
	{
		"osm_key": "barrier", "osm_val": "full-height_turnstile",
		"obstacleTags": { "Obstacle": "Turnstile", "ObstacleType": "Pass" },
		"obstacleLocation": "near_path"
	},
	{
		"osm_key": "barrier", "osm_val": "horse_stile",
		"obstacleTags": { "Obstacle": "Turnstile", "ObstacleType": "Pass" },
		"obstacleLocation": "near_path"
	},
	{
		"osm_key": "barrier", "osm_val": "kissing_gate",
		"obstacleTags": { "Obstacle": "Turnstile", "ObstacleType": "Pass" },
		"obstacleLocation": "near_path"
	},
	{
		"osm_key": "barrier", "osm_val": "stile",
		"obstacleTags": { "Obstacle": "Turnstile", "ObstacleType": "Pass" },
		"obstacleLocation": "near_path"
	},
	{
		"osm_key": "barrier", "osm_val": "height_restrictor",
		"obstacleTags": { "Obstacle": "Yes", "ObstacleType": "Overhanging" },
		"obstacleLocation": "near_path"
	},
	{
		"osm_key": "barrier", "osm_val": "bar",
		"obstacleTags": { "Obstacle": "Yes", "ObstacleType": "Ground" },
		"obstacleLocation": "near_path"
	},
	{
		"osm_key": "advertising", "osm_val": "column",
		"obstacleTags": { "Obstacle": "AdvertisingSupport", "ObstacleType": "Ground"},
		"obstacleLocation": "near_path"
	},
	{
		"osm_key": "advertising", "osm_val": "poster_box",
		"obstacleTags": { "Obstacle": "AdvertisingSupport", "ObstacleType": "Ground"},
		"obstacleLocation": "near_path"
	},
		{
		"osm_key": "recycling_type", "osm_val": "container",
		"obstacleTags": { "Obstacle": "RecyclingContainer", "ObstacleType": "Ground"},
		"obstacleLocation": "near_path"
	},	
	{
		"osm_key": "advertising", "osm_val": "billboard",
		"obstacleTags": { "Obstacle": "AdvertisingSupport" },
		"obstacleLocation": "near_path"
	},	
	{
		"osm_key": "obstacle:wheelchair", "osm_val": "yes",
		"obstacleTags": { "Obstacle": "Yes" },
		"obstacleLocation": "near_path"
	},
]

PATH_OSM_TO_WDM: List[Dict[str,Any]] = [
	{ "osmTags": { "highway": "*", "footway": "sidewalk" }, "wdmSpl": "Pavement" },
	{ "osmTags": { "highway": "*", "footway": "crossing" }, "wdmSpl": "Crossing" },
	{ "osmTags": { "highway": "*", "crossing": "*" }, "withoutOsmTags": { "cycleway": "crossing", "crossing": "no" }, "wdmSpl": "Crossing" },
	{ "osmTags": { "highway": "*", "path": "crossing" }, "wdmSpl": "Crossing" },
	{ "osmTags": { "highway": "crossing" }, "wdmSpl": "Crossing" },
	{ "osmTags": { "highway": "corridor" }, "wdmSpl": "Corridor" },
	{ "osmTags": { "highway": "steps", "conveying": "*" }, "withoutOsmTags": { "conveying": "no" }, "wdmSpl": "Escalator" },
	{ "osmTags": { "highway": "*", "conveying": "*" }, "withoutOsmTags": { "conveying": "no" }, "wdmSpl": "Travelator" },
	{ "osmTags": { "highway": "steps" }, "wdmSpl": "Stairs" },
	{ "osmTags": { "highway": "footway", "incline": ["up", "down"] }, "wdmSpl": "Ramp" },
	{ "osmTags": { "highway": ["footway","path","steps","pedestrian","living_street"], "tunnel": "building_passage" }, "wdmSpl": "Concourse" },
	{ "osmTags": { "highway": "platform" }, "wdmSpl": "Quay" },
	{ "osmTags": { "highway": ["footway","path","steps","pedestrian","living_street"], "tunnel": "yes" }, "wdmSpl": "Corridor" },
	{ "osmTags": { "highway": "*", "indoor": "yes" }, "wdmSpl": "Hall" },
	{ "osmTags": { "highway": ["footway","pedestrian"], "area": "yes" }, "wdmSpl": "OpenSpace" },
	{ "osmTags": { "highway": "footway", "footway": "traffic_island" }, "wdmSpl": "Pavement" },
	{ "osmTags": { "highway": ["living_street", "pedestrian"] }, "wdmSpl": "Street" },
	{ "osmTags": { "highway": ["footway", "path"] }, "wdmSpl": "Footpath" },
	{ "osmTags": { "highway": "cycleway", "foot": ["yes", "designated"], "segregated": "yes" }, "wdmSpl": "Pavement" },
	{ "osmTags": { "highway": "cycleway", "foot": ["yes", "designated"], "segregated": "no" }, "wdmSpl": "Footpath" }
]

ROAD_SIDEWALK_OSM_TO_WDM: List[Dict[str,Any]] = [
	{ "osmTags": { "highway": "*", "foot": "*" }, "withoutOsmTags": { "foot": "no" }, "wdmSpl": "Footpath" },
	{ "osmTags": { "highway": "*", "sidewalk": "*" }, "withoutOsmTags": { "sidewalk": ["no", "none", "separate"] }, "wdmSpl": "Pavement" },
	{ "osmTags": { "highway": "*", "sidewalk:right": "*" }, "withoutOsmTags": { "sidewalk:right": ["no", "none", "separate"] }, "wdmSpl": "Pavement" },
	{ "osmTags": { "highway": "*", "sidewalk:left": "*" }, "withoutOsmTags": { "sidewalk:left": ["no", "none", "separate"] }, "wdmSpl": "Pavement" }
]


######################################
# Post-process mappings
#

NEARBY_SEARCHES: List[Dict[str,Any]] = [
	# Parking on POI
	{
		"searchOsmKey": "amenity",
		"searchOsmVal": "parking",
		"searchWdmKey": "PointOfInterest",
		"searchDistance": 100,
		"nearbyWdmTags": { "NearbyCarPark": "Yes" }
	},

	# Bicycle parking on POI
	{
		"searchOsmKey": "amenity",
		"searchOsmVal": "bicycle_parking",
		"searchWdmKey": "PointOfInterest",
		"searchDistance": 50,
		"nearbyWdmTags": { "NearbyCyclePark": "Yes" }
	},

	# Emergency features on POI
	{
		"searchOsmKey": "emergency",
		"searchOsmVal": "phone",
		"searchWdmKey": "PointOfInterest",
		"searchDistance": 0,
		"nearbyWdmTags": { "Emergency": "SOSPoint" }
	},
	{
		"searchOsmKey": "amenity",
		"searchOsmVal": "fire_station",
		"searchWdmKey": "PointOfInterest",
		"searchDistance": 0,
		"nearbyWdmTags": { "Emergency": "Fire" }
	},
	{
		"searchOsmKey": "amenity",
		"searchOsmVal": "police",
		"searchWdmKey": "PointOfInterest",
		"searchDistance": 0,
		"nearbyWdmTags": { "Emergency": "Police" }
	},

	# Visual info on POI or public transports
	{
		"searchOsmKey": "information",
		"searchWithoutOsmTags": { "information": ["office", "visitor_center"] },
		"searchWdmKey": "PointOfInterest",
		"searchDistance": 0,
		"nearbyWdmTags": { "VisualSigns": "Yes" }
	},
	{
		"searchOsmKey": "passenger_information_display",
		"searchWithoutOsmTags": { "passenger_information_display": "no" },
		"searchWdmKey": "PointOfInterest",
		"searchWdmVal": "StopPlace",
		"searchDistance": 0,
		"nearbyWdmTags": { "VisualDisplays": "Yes" }
	},
	{
		"searchOsmKey": "departures_board",
		"searchWithoutOsmTags": { "departures_board": "no" },
		"searchWdmKey": "PointOfInterest",
		"searchWdmVal": "StopPlace",
		"searchDistance": 0,
		"nearbyWdmTags": { "VisualDisplays": "Yes" }
	},
	{
		"searchOsmKey": "information",
		"searchOsmVal": "terminal",
		"searchWdmKey": "PointOfInterest",
		"searchWdmVal": "StopPlace",
		"searchDistance": 0,
		"nearbyWdmTags": { "VisualDisplays": "Yes" }
	},
	{
		"searchOsmKey": "board_type",
		"searchOsmVal": "public_transport",
		"searchWdmKey": "PointOfInterest",
		"searchWdmVal": "StopPlace",
		"searchDistance": 0,
		"nearbyWdmTags": { "LargePrintTimetable": "Yes" }
	},
	{
		"searchOsmKey": "departures_board",
		"searchOsmVal": "timetable",
		"searchWdmKey": "PointOfInterest",
		"searchWdmVal": "StopPlace",
		"searchDistance": 0,
		"nearbyWdmTags": { "LargePrintTimetable": "Yes" }
	},
	{
		"searchOsmKey": "passenger_information_display",
		"searchWithoutOsmTags": { "passenger_information_display": "no" },
		"searchWdmKey": "PointOfInterest",
		"searchWdmVal": "StopPlace",
		"searchDistance": 0,
		"nearbyWdmTags": { "RealTimeDepartures": "Yes" }
	},
	{
		"searchOsmKey": "departures_board",
		"searchOsmVal": "realtime",
		"searchWdmKey": "PointOfInterest",
		"searchWdmVal": "StopPlace",
		"searchDistance": 0,
		"nearbyWdmTags": { "RealTimeDepartures": "Yes" }
	},

	# Boarding help on public transport
	{
		"searchOsmKey": "service:SNCF:acces_plus",
		"searchOsmVal": "yes",
		"searchWdmKey": "PointOfInterest",
		"searchWdmVal": "StopPlace",
		"searchDistance": 0,
		"nearbyWdmTags": { "Assistance": "Boarding" }
	},

	# Wifi on POI
	{
		"searchOsmKey": "internet_access:fee",
		"searchOsmVal": "no",
		"searchWithoutOsmTags": { "internet_access": "no" },
		"searchWdmKey": "PointOfInterest",
		"searchDistance": 0,
		"nearbyWdmTags": { "WiFi": "Free" }
	},
	{
		"searchOsmKey": "internet_access",
		"searchWithoutOsmTags": { "internet_access": "no" },
		"searchWdmKey": "PointOfInterest",
		"searchDistance": 0,
		"nearbyWdmTags": { "WiFi": "Yes" }
	},
]


######################################
#
# Used OSM tags (for Osmium)
#

# Those ones are listed manually as they only appear in converter.py, not this file.
USED_OSM_TAGS = {
	"aerialway": "*",
	"railway": set(["subway_entrance", "platform"]),
	"indoor": set(["door"]),
	"entrance": "*",
	"door": "*",
	"amenity": set(["vending_machine", "parking_space"]),
	"highway": set(["bus_stop"]),
	"public_transport": set(["platform"]),
	"tourism": set(["information"]),
}

OSM_MAIN_KEYS = [
    "aerialway", "aeroway", "amenity", "barrier", "building", "door", "emergency",
	"entrance", "government", "healthcare", "highway", "historic", "indoor", "information",
	"landuse", "leisure", "man_made", "natural", "office", "place", "power", "public_transport",
	"railway", "religion", "shop", "social_facility", "sport", "tourism", "water", "waterway"
]

def appendTagToSet(k, v):
	if k not in OSM_MAIN_KEYS:
		return

	if v is None:
		v = "*"

	if isinstance(v, list):
		for subv in v:
			appendTagToSet(k, subv)
	else:
		if k in USED_OSM_TAGS:
			if v == "*":
				USED_OSM_TAGS[k] = "*"
			elif USED_OSM_TAGS[k] != "*":
				USED_OSM_TAGS[k].add(v)
		elif v == "*":
			USED_OSM_TAGS[k] = "*"
		else:
			USED_OSM_TAGS[k] = set([v])

# Tags read from mappings above
for m in POI_OSM_TO_WDM + STATION_OSM_TO_WDM + OBSTACLE_OSM_TO_WDM:
	appendTagToSet(m["osm_key"], m.get("osm_val"))

for m in PATH_OSM_TO_WDM + ROAD_SIDEWALK_OSM_TO_WDM:
	for k in m["osmTags"]:
		appendTagToSet(k, m["osmTags"][k])

for m in NEARBY_SEARCHES:
	appendTagToSet(m["searchOsmKey"], m.get("searchOsmVal"))
