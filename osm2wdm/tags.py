from lxml import etree
from lxml.builder import E
from math import tan, radians
from . import mappings
import traceback
import logging
import re
from typing import List, Optional, Dict


def findallRule(k: str, v: Optional[str] = None, ftag: Optional[str] = None) -> str:
	"""Get the findall expression to find OSM features with given tag"""
	tagRule = ".//"
	if ftag:
		tagRule += f"{ftag}/"
	tagRule += f"tag[@k='{k}']"
	if v:
		tagRule += f"[@v='{v}']"
	tagRule += ".."
	return tagRule


def isOsmOperatedBy(elemTags: Dict[str,str], name: str, wikidata: str):
	"""Checks if an OSM feature is marked as operated by given organization

	Parameters
	----------
	elemTags: dict
		The element tags
	name: str
		The readable operator name
	wikidata: str
		The Wikidata operator ID

	Returns
	-------
	bool
		True if this feature is operated by given organization
	"""

	osmWikidata = elemTags.get("operator:wikidata") or elemTags.get("brand:wikidata") or elemTags.get("network:wikidata") or elemTags.get("wikidata")

	if osmWikidata is not None and osmWikidata == wikidata:
		return True

	osmName = elemTags.get("operator") or elemTags.get("brand") or elemTags.get("network") or elemTags.get("name")

	return osmWikidata is None and osmName is not None and osmName == name


def hasAll(elemTags: Dict[str,str], searchTags: Dict[str,str], excludedTags: Optional[Dict[str,str]] = None) -> bool:
	"""Check if a feature has all wanted tags defined

	Parameters
	----------
	elemTags: dict
		The element tags to search into
	searchTags: dict
		The tags that should appear on feature
	excludeTags: dict
		The tags that should NOT appear on feature
	
	Returns
	-------
	bool
		True if all tags are set and no excluded tag is set
	"""

	# Are all wanted tags there
	for k,v in searchTags.items():
		elemV = elemTags.get(k)
		if type(v) == list:
			if elemV not in v: # type: ignore
				return False
		elif v == "*":
			if elemV is None:
				return False
		else:
			if elemV != v:
				return False
	
	# Are there any excluded tag
	if excludedTags is not None:
		for k,v in excludedTags.items():
			elemV = elemTags.get(k)
			if type(v) == list:
				if elemV in v: # type: ignore
					return False
			elif v == "*":
				if elemV is not None:
					return False
			else:
				if elemV == v:
					return False

	return True


def getAll(elem: etree._Element):
	"""Get all tags for this element"""
	tags = {}
	for t in elem.iter("tag"):
		if len(t.attrib.get("k", "")) > 0 and len(t.attrib.get("v", "")) > 0:
			tags[t.attrib["k"]] = t.attrib["v"]
	return tags


def getValue(elem: etree._Element, key: str):
	"""Get the value of the wanted tag of a feature (if set)

	Parameters
	----------
	elem: lxml.etree._Element
		The element to search into
	key: str
		The tag key

	Returns
	-------
	str or None
		Tag value if key exists
	"""

	if elem is None:
		return None

	tag = elem.find(f"tag[@k=\"{key}\"]")

	return tag.attrib["v"] if tag is not None else None


def getOsmSizeValue(elemTags: Dict[str,str], keys: list[str]):
	"""Computes the size value from a list of OSM keys

	Parameters
	----------
	elem : lxml.etree._Element
		The element to search into
	keys : list[str]
		The tag to look for, with preferred first

	Returns
	-------
	str | None
		The computed size value
	"""

	size = None
	rgx = re.compile(r"^\s*(?P<val>\d+(\.\d+)?)(\s*(?P<unit>c?m))?\s*$")

	for k in keys:
		osmVal = elemTags.get(k)

		# Check if value is readable
		if osmVal is not None:
			m = rgx.match(osmVal)
			if m:
				try:
					val = float(m.group('val'))
					unit = m.group('unit')
					if unit in ["m", None]:
						size = "{:.2f}".format(val)
					elif unit == "cm":
						size = "{:.2f}".format(val / 100)
				except ValueError:
					continue

			if size is not None:
				return size

	return size


def getOsmWeightValue(weightVal: str):
	"""Computes the WDM weight value in kg based on an OSM weight tag

	Parameters
	----------
	weightVal : str | None
		The input OSM weight value

	Returns
	-------
	str | None
		The computed weight value in kg
	"""

	if weightVal is None:
		return None

	weight = None
	rgx = re.compile(r"^\s*(?P<val>\d+(\.\d+)?)(\s*(?P<unit>(t|kg)))?\s*$")

	m = rgx.match(weightVal)
	if m:
		try:
			val = float(m.group('val'))
			unit = m.group('unit')
			if unit in ["t", None]:
				weight = val * 1000
			elif unit == "kg":
				weight = val
		except ValueError:
			return None

	return str(round(weight)) if weight is not None else None


def getOsmLevelValue(tags: Dict[str,str]):
	"""Get the level value from OSM feature (based on level=* and repeat_on=*)"""

	osmLevel = tags.get("level")
	osmRepeatOn = tags.get("repeat_on")
	if osmLevel and osmRepeatOn:
		return osmLevel + ";" + osmRepeatOn
	elif osmLevel:
		return osmLevel
	elif osmRepeatOn:
		return osmRepeatOn
	else:
		return None


def getWdmPedestrianLightsValue(elemTags: Dict[str,str]):
	"""Finds the value for WDM PedestrianLights based on given OSM feature

	Parameters
	----------
	feature : lxml.etree._Element
		The OSM feature to check

	Returns
	-------
	str
		The WDM PedestrianLights value, or None if no value found
	"""

	osmCrossing = elemTags.get("crossing")
	if (
		osmCrossing == "traffic_signals"
		or elemTags.get("flashing_lights") == "yes"
		or elemTags.get("crossing:light") == "yes"
		or elemTags.get("crossing:signals") == "yes"
		or elemTags.get("button_operated") == "yes"
	):
		return "Yes"
	elif (
		elemTags.get("flashing_lights") == "no"
		or elemTags.get("crossing:light") == "no"
		or elemTags.get("crossing:signals") == "no"
		or osmCrossing == "uncontrolled"
		or osmCrossing == "unmarked"
	):
		return "No"
	else:
		return None


def getOsmSlopeValue(inclineVal: Optional[str]):
	"""Converts an incline OSM value into a WDM Slope value

	Parameters
	----------
	inclineVal : str | None
		The input OSM incline value

	Returns
	-------
	str | None
		The WDM Slope computed value
	"""

	if inclineVal in [None, "up", "down"]:
		return None

	slope = None
	rgx = re.compile(r"^\s*(?P<val>-?\d+(\.\d+)?)(\s*(?P<unit>[%°]))?\s*$")

	m = rgx.match(inclineVal) # type: ignore
	if m:
		try:
			val = float(m.group('val'))
			unit = m.group('unit')
			if unit == "%":
				slope = val
			elif unit == "°":
				slope = tan(radians(val))*100
		except ValueError:
			return None

	return "{:.2f}".format(abs(slope)) if slope is not None else None


def appendTag(elem: etree._Element, k: str, v: Optional[str] = None, override: Optional[bool] = True):
	"""Appends a tag sub-element into given Element if tag is set

	Parameters
	----------
	elem: lxml.etree._Element
		The element to append tag into
	k: str
		The tag key
	v: str
		The tag value
	override: bool
		Should we override existing value if any (defaults to True)
	"""

	# If value is defined
	if v:
		v = v.strip()
		if len(v) > 0:
			# Check if tag doesn't exist in element
			existingTag = elem.find(f"tag[@k=\"{k}\"]")

			if existingTag is None:
				elem.append(E.tag(k=k, v=v))
			elif override:
				existingTag.attrib["v"] = v


def removeTag(elem: etree._Element, k: str):
	"""Removes a tag from Element if it is set.

	Parameters
	----------
	elem: lxml.etree._Element
		The element to append tag into
	k: str
		The tag key
	"""

	for tagElem in elem.findall(f"tag[@k=\"{k}\"]"):
		elem.remove(tagElem)


def appendTagValueMapping(wdmElem: etree._Element, wdmKey: str, mapping: dict[str, str], osmVal: Optional[str], override: Optional[bool] = True):
	"""Appends a tag sub-element into given WDM Element, based on OSM value and a value mapping

	Parameters
	----------
	wdmElem : etree._Element
		The element to append tag into
	wdmKey : str
		The WDM tag key
	mapping : dict[str,str]
		List of equivalences between OSM values (key) and WDM values (val)
	osmVal : str
		Read OSM value from an OSM element
	"""

	if osmVal in mapping:
		appendTag(wdmElem, wdmKey, mapping[osmVal], override)
	elif "*" in mapping and osmVal is not None:
		appendTag(wdmElem, wdmKey, mapping["*"], override)


def appendRefTags(osm: etree._Element, wdm: etree._Element):
	"""Appends reference and version tags to WDM Element based on OSM Element

	Parameters
	----------
	osm : lxml.etree._Element
		The OSM XML element
	wdm : lxml.etree._Element
		The XML element representing a single WDM feature
	"""

	appendTag(wdm, "Ref:Import:Source", "OpenStreetMap")
	appendTag(wdm, "Ref:Import:Id", osm.tag[0:1] + (osm.get("id") or ""))
	appendTag(wdm, "Ref:Import:Version", osm.get("version"))


def appendImpairmentTags(osmTags: Dict[str,str], wdm: etree._Element):
	"""Appends wheelchair/visual/audible impairment tags to WDM Element based on OSM Element

	Parameters
	----------
	osmTags : dict
		The OSM tags
	wdm : lxml.etree._Element
		The XML element representing a single WDM feature
	"""

	appendTagValueMapping(wdm, "WheelchairAccess", mappings.TAG_YESNOBAD, osmTags.get("wheelchair"))

	# TactileWarningStrip
	osmTactile = osmTags.get("tactile_paving")
	appendTagValueMapping(wdm, "TactileWarningStrip", {"no": "None", "yes": "Good", "contrasted": "Good"}, osmTactile)
	if osmTactile in ["yes", "contrasted"]:
		appendTag(wdm, "FixMe:TactileWarningStrip", "Vérifier l’état du marquage de la borne d’éveil à vigilance")

	appendLightingTag(osmTags, wdm)

	# AudibleSignals
	wdmAud = None
	if osmTags.get("departures_board:speech_output") not in [None, "no"]:
		wdmAud = "Yes"
	elif osmTags.get("passenger_information_display:speech_output") not in [None, "no"]:
		wdmAud = "Yes"
	elif osmTags.get("announcement") not in [None, "no"]:
		wdmAud = "Yes"
	elif osmTags.get("speech_output") not in [None, "no"]:
		wdmAud = "Yes"
	elif osmTags.get("departures_board:speech_output") == "no":
		wdmAud = "No"
	elif osmTags.get("passenger_information_display:speech_output") == "no":
		wdmAud = "No"
	elif osmTags.get("announcement") == "no":
		wdmAud = "No"
	elif osmTags.get("speech_output") == "no":
		wdmAud = "No"

	appendTag(wdm, "AudibleSignals", wdmAud)

	# VisualSigns
	wdmVis = None
	if osmTags.get("departures_board") not in [None, "no"]:
		wdmVis = "Yes"
	elif osmTags.get("passenger_information_display") not in [None, "no"]:
		wdmVis = "Yes"

	appendTag(wdm, "VisualSigns", wdmVis)


def appendAddressTags(osmTags: Dict[str,str], wdm: etree._Element, osmRelations: list[etree._Element] = []):
	"""Appends address tags to WDM Element based on OSM Element

	Parameters
	----------
	osmTags : lxml.etree._Element
		The XML element representing a single OSM feature
	wdm : lxml.etree._Element
		The XML element representing a single WDM feature
	osmRelations : list[lxml.etree._Element]
		Relations having the given OSM element as a member
	"""

	osmAddrNb = osmTags.get("addr:housenumber") or osmTags.get("contact:housenumber")
	osmAddrStreet = osmTags.get("addr:street") or osmTags.get("contact:street")
	osmRelPostcode = None

	# Look in relations for street name
	if osmAddrNb is not None and osmAddrStreet is None:
		for rel in osmRelations:
			if len(rel.findall("tag[@k=\"type\"][@v=\"street\"]") + rel.findall("tag[@k=\"type\"][@v=\"associatedStreet\"]")) == 1:
				osmAddrStreet = getValue(rel, "name")
				osmRelPostcode = getValue(rel, "addr:postcode")
				if osmAddrStreet is not None:
					continue

	wdmAddr = f"{osmAddrNb} {osmAddrStreet}" if osmAddrNb is not None and osmAddrStreet is not None else osmAddrStreet
	appendTag(wdm, "AddressLine", wdmAddr)
	appendTag(wdm, "PostCode", osmTags.get("addr:postcode") or osmTags.get("contact:postcode") or osmRelPostcode)


def appendLightingTag(osmTags: Dict[str,str], wdm: etree._Element):
	"""Appends lighting tags to WDM Element based on OSM Element

	Parameters
	----------
	osmTags : dict
		The OSM tags
	wdm : lxml.etree._Element
		The XML element representing a single WDM feature
	"""

	osmLit = osmTags.get("lit")
	osmLitP = osmTags.get("lit:perceived")

	if osmLit is not None:
		appendTag(wdm, "Lighting", "No" if osmLit == "no" else "Yes")
	elif osmLitP is not None:
		appendTagValueMapping(wdm, "Lighting", {"good": "Yes", "daylike": "Yes", "none": "No", "minimal": "No", "*": "Bad"}, osmLitP)


def appendTiltSideTag(osmTags: Dict[str,str], wdm: etree._Element):
	"""Appends Tilt and TiltSide tags to WDM Element based on OSM Element

	Parameters
	----------
	osmTags : dict
		The OSM tags
	wdm : lxml.etree._Element
		The XML element representing a single WDM feature
	"""

	osmInclineAcross = osmTags.get("incline:across")
	if osmInclineAcross is not None:
		appendTag(wdm, "Tilt", getOsmSlopeValue(osmInclineAcross))
		appendTag(wdm, "TiltSide", "Left" if osmInclineAcross.strip().startswith("-") else "Right")


def appendToiletsTags(osmTags: Dict[str,str], wdm: etree._Element):
	"""Appends Toilets:* & Staffing tags to WDM Element based on OSM Element

	Parameters
	----------
	osmTags : dict
		The OSM tags
	wdm : lxml.etree._Element
		The XML element representing a single WDM feature
	"""

	# Toilets:BabyChange
	if osmTags.get("changing_table") == "yes" and osmTags.get("changing_table:wheelchair") == "yes":
		appendTag(wdm, "Toilets:BabyChange", "Yes")
	elif osmTags.get("changing_table") == "yes":
		appendTag(wdm, "Toilets:BabyChange", "Bad")
	elif osmTags.get("changing_table") == "no":
		appendTag(wdm, "Toilets:BabyChange", "No")

	# Shower
	osmShower = osmTags.get("shower")
	if osmShower == "no":
		appendTag(wdm, "Toilets:Shower", "No")
	elif osmShower is not None:
		appendTag(wdm, "Toilets:Shower", "Yes")
	
	appendTagValueMapping(wdm, "Toilets:HandWashing", mappings.TAG_YESNO, osmTags.get("toilets:handwashing"))
	appendTagValueMapping(wdm, "Toilets:DrinkingWater", mappings.TAG_YESNO, osmTags.get("drinking_water"))

	# Toilets:Position
	wdmToiletsPosition = []
	osmToiletsPosition = osmTags.get("toilets:position", "")

	if "seated" in osmToiletsPosition:
		wdmToiletsPosition.append("Seated")
	if "urinal" in osmToiletsPosition:
		wdmToiletsPosition.append("Urinal")
	if "squat" in osmToiletsPosition:
		wdmToiletsPosition.append("Squat")
	
	if len(wdmToiletsPosition) > 0:
		appendTag(wdm, "Toilets:Position", ";".join(wdmToiletsPosition))

	# Staffing
	osmSupervised = osmTags.get("supervised")
	if osmSupervised == "no":
		appendTag(wdm, "Staffing", "No")
	elif osmSupervised == "yes":
		appendTag(wdm, "Staffing", "FullTime")
	elif osmSupervised is not None:
		appendTag(wdm, "Staffing", "PartTime")


def appendElevatorTags(osmTags: Dict[str,str], wdm: etree._Element):
	"""Appends elevator-related tags to WDM Element

	Parameters
	----------
	osmTags : dict
		The OSM tags
	wdm : lxml.etree._Element
		The XML element representing a single WDM feature
	"""

	appendTag(wdm, "InternalWidth", getOsmSizeValue(osmTags, ["maxwidth:physical", "width", "est_width"]))
	appendTag(wdm, "Depth", getOsmSizeValue(osmTags, ["maxlength:physical", "length", "est_length"]))
	appendTagValueMapping(wdm, "Attendant", {"no": "No", "*": "Yes"}, osmTags.get("supervised"))
	appendTag(wdm, "PublicCode", osmTags.get("local_ref") or osmTags.get("ref"))
	appendTag(wdm, "MagneticInductionLoop", toWDMCase(osmTags.get("audio_loop") or osmTags.get("hearing_loop")))

	# RaisedButtons
	wdmRaisedButtons = None
	if osmTags.get("tactile_writing") == "no":
		wdmRaisedButtons = "None"
	elif osmTags.get("tactile_writing") == "yes":
		wdmRaisedButtons = "Raised"
	elif osmTags.get("tactile_writing:embossed_printed_letters") not in ["no", None]:
		wdmRaisedButtons = "Raised"
	elif osmTags.get("tactile_writing:embossed_printed_letters:fr") not in ["no", None]:
		wdmRaisedButtons = "Raised"
	elif osmTags.get("tactile_writing:engraved_printed_letters") not in ["no", None]:
		wdmRaisedButtons = "Raised"
	elif osmTags.get("tactile_writing:braille") not in ["no", None]:
		wdmRaisedButtons = "Braille"
	elif osmTags.get("tactile_writing:braille:fr") not in ["no", None]:
		wdmRaisedButtons = "Braille"

	appendTag(wdm, "RaisedButtons", wdmRaisedButtons)

	# AudioAnnouncements
	osmSpeechOutput = osmTags.get("speech_output")
	wdmAudioAnnouncements = None
	if osmSpeechOutput == "no":
		wdmAudioAnnouncements = "No"
	elif osmSpeechOutput not in ["no", None]:
		wdmAudioAnnouncements = "Yes"
	elif osmTags.get("speech_output:fr") not in ["no", None]:
		wdmAudioAnnouncements = "Yes"

	appendTag(wdm, "AudioAnnouncements", wdmAudioAnnouncements)

	# MirrorOnOppositeSide
	if osmTags.get("elevator:mirror") == "no" or osmTags.get("mirror") == "no":
		appendTag(wdm, "MirrorOnOppositeSide", "No")
	elif osmTags.get("elevator:mirror") not in [None,"no"] or osmTags.get("mirror") not in [None,"no"]:
		appendTag(wdm, "MirrorOnOppositeSide", "Yes")


def appendCommonTags(osmTags: Dict[str,str], wdm: etree._Element):
	"""Appends generic tags to WDM Element

	Parameters
	----------
	osmTags : dict
		The OSM tags
	wdm : lxml.etree._Element
		The XML element representing a single WDM feature
	"""

	appendTag(wdm, "Name", osmTags.get("name"))
	appendTag(wdm, "Description", osmTags.get("description"))
	appendTag(wdm, "FixMe", osmTags.get("fixme"))
	appendTag(wdm, "WheelchairAccess:Description", osmTags.get("wheelchair:description") or osmTags.get("wheelchair:description:fr"))
	appendTag(wdm, "VisualSigns:Description", osmTags.get("deaf:description") or osmTags.get("deaf:description:fr"))
	appendTag(wdm, "AudibleSignals:Description", osmTags.get("blind:description") or osmTags.get("blind:description:fr"))
	appendTag(wdm, "SeatCount", osmTags.get("seats") or osmTags.get("capacity:seats"))
	appendTagValueMapping(wdm, "BackRest", mappings.TAG_YESNO, osmTags.get("backrest"))

	# Image
	osmImg = osmTags.get("image")
	osmCommons = osmTags.get("wikimedia_commons")
	osmPnx = osmTags.get("panoramax")
	if osmImg is not None:
		appendTag(wdm, "Image", osmImg)
	elif osmCommons is not None:
		appendTag(wdm, "Image", f"https://commons.wikimedia.org/wiki/{osmCommons}")
	elif osmPnx is not None:
		appendTag(wdm, "Image", f"https://api.panoramax.xyz/api/pictures/{osmPnx}/sd.jpg")


def appendCommonEntranceTags(osm: etree._Element, osmTags: Dict[str,str], wdm: etree._Element, osmRelations: list[etree._Element] = []):
	"""Appends generic tags to WDM Element read from an OSM entrance

	Parameters
	----------
	osm : etree.Element
		The OSM feature
	osmTags : dict
		The OSM tags
	wdm : lxml.etree._Element
		The XML element representing a single WDM feature
	osmRelations : list[lxml.etree._Element]
		Relations having the given OSM element as a member
	"""

	appendTag(wdm, "PublicCode", osmTags.get("ref"))
	appendTag(wdm, "Width", getOsmSizeValue(osmTags, ["width", "door:width", "wheelchair:entrance_width", "est_width"]))
	appendTag(wdm, "Height", getOsmSizeValue(osmTags, ["height"]))
	appendTag(wdm, "KerbHeight", getOsmSizeValue(osmTags, ["kerb:height", "wheelchair:step_height"]))
	appendImpairmentTags(osmTags, wdm)
	appendTagValueMapping(wdm, "AutomaticDoor", mappings.TAG_AUTOMATIC_DOOR, osmTags.get("automatic_door"))
	osmDoor = osmTags.get("door")
	if osmDoor != "yes":
		appendTagValueMapping(
			wdm, "Door",
			{"hinged": "Hinged", "sliding": "Sliding", "revolving": "Revolving", "swinging": "Swing", "no": "None", "*": "Other"},
			osmDoor
		)
	appendTagValueMapping(wdm, "DoorHandle:Inside", mappings.TAG_DOORHANDLE, osmTags.get("door:handle:inside") or osmTags.get("door:handle"))
	appendTagValueMapping(wdm, "DoorHandle:Outside", mappings.TAG_DOORHANDLE, osmTags.get("door:handle:outside") or osmTags.get("door:handle"))
	appendAddressTags(osmTags, wdm, osmRelations)

	# EntrancePassage
	osmBarrier = osmTags.get("barrier")
	wdmEntrancePassage = None

	if osmDoor not in [None, "no"]:
		wdmEntrancePassage = "Door"
	elif osmBarrier == "gate":
		wdmEntrancePassage = "Gate"
	elif osmBarrier not in [None, "no"]:
		wdmEntrancePassage = "Barrier"
	elif osmDoor == "no":
		wdmEntrancePassage = "Opening"

	appendTag(wdm, "EntrancePassage", wdmEntrancePassage)

	# IsEntry, IsExit
	osmEntrance = osmTags.get("entrance")
	wdmEntry = "Yes"
	wdmExit = "Yes"

	if osmEntrance in ["exit", "emergency"]:
		wdmEntry = "No"
	elif osmEntrance == "entrance":
		wdmExit = "No"
	elif len(osmRelations) > 0:
		# Check if in stop_area relation with a exit/entry_only role
		for rel in osmRelations:
			if rel.find("tag[@k=\"public_transport\"][@v=\"stop_area\"]") is not None:
				relMember = rel.find(f"member[@type=\"{osm.tag}\"][@ref=\"{osm.attrib['id']}\"]")
				if relMember is not None:
					relMemberRole = relMember.attrib["role"]
					if relMemberRole == "exit_only":
						wdmEntry = "No"
					elif relMemberRole == "entry_only":
						wdmExit = "No"

	appendTag(wdm, "IsEntry", wdmEntry)
	appendTag(wdm, "IsExit", wdmExit)

	# GlassDoor
	osmMaterial = osmTags.get("material")
	if osmMaterial is not None:
		osmEntranceCB = osmTags.get("entrance:coloured_band")
		wdmGlass = None

		if osmMaterial != "glass":
			wdmGlass = "No"
		elif osmEntranceCB == "no":
			wdmGlass = "Bad"
		elif osmEntranceCB not in [None, "no"]:
			wdmGlass = "Yes"
		else:
			wdmGlass = "Yes"
			appendTag(wdm, "FixMe:GlassDoor", "Vérifier la présence de marquage ou d’autre système de repérage des parois transparentes")

		appendTag(wdm, "GlassDoor", wdmGlass)


def appendCommonPoiTags(osmTags: Dict[str,str], wdm: etree._Element, osmRelations: list[etree._Element] = []):
	"""Appends generic tags to WDM Element read from an OSM point of interest

	Parameters
	----------
	osmTags : dict
		The OSM tags
	wdm : lxml.etree._Element
		The XML element representing a single WDM feature
	osmRelations : list[lxml.etree._Element]
		Relations having the given OSM element as a member
	"""

	appendImpairmentTags(osmTags, wdm)
	appendTag(wdm, "Ref:FR:SIRET", osmTags.get("ref:FR:SIRET"))
	appendTag(wdm, "Phone", osmTags.get("contact:phone") or osmTags.get("phone") or osmTags.get("phone:FR"))
	appendTag(wdm, "Website", osmTags.get("contact:website") or osmTags.get("website"))
	appendAddressTags(osmTags, wdm, osmRelations)

	# WiFi
	osmInternet = osmTags.get("internet_access")
	osmInternetFee = osmTags.get("internet_access:fee")
	osmWifi = osmTags.get("wifi")
	wdmWifi = None

	if (osmInternet in ["wlan", "yes", "wifi"] and osmInternetFee == "no") or osmWifi == "free":
		wdmWifi = "Free"
	elif osmInternet in ["wlan", "yes", "wifi"] or osmWifi == "yes":
		wdmWifi = "Yes"
	elif osmInternet == "no" or osmWifi == "no":
		wdmWifi = "No"

	appendTag(wdm, "WiFi", wdmWifi)

	# Toilets
	if osmTags.get("toilets:wheelchair") == "yes":
		appendTag(wdm, "Toilets", "Yes")
	elif osmTags.get("toilets") == "yes":
		appendTag(wdm, "Toilets", "Bad")
	elif osmTags.get("toilets") == "no":
		appendTag(wdm, "Toilets", "No")

	appendToiletsTags(osmTags, wdm)


def appendCommonQuayTags(osmTags: Dict[str,str], wdm: etree._Element):
	"""Appends generic tags to WDM Element read from an OSM quay

	Parameters
	----------
	osmTags : dict
		The OSM tags
	wdm : lxml.etree._Element
		The XML element representing a single WDM feature
	"""

	appendTag(wdm, "PublicCode", osmTags.get("local_ref") or osmTags.get("ref"))
	appendImpairmentTags(osmTags, wdm)
	appendTagValueMapping(wdm, "Outdoor", {"yes": "Covered"}, osmTags.get("covered"))
	appendTagValueMapping(wdm, "Shelter", mappings.TAG_YESNO, osmTags.get("shelter"))
	appendTagValueMapping(wdm, "Seating", mappings.TAG_YESNO, osmTags.get("bench"))
	appendTagValueMapping(wdm, "RubbishDisposal", mappings.TAG_YESNO, osmTags.get("bin"))

	operatorRefs = ["Tallis", "Mobigo"]
	for operator in operatorRefs:
		osmVal = osmTags.get(f"ref:FR:{operator}")
		if osmVal:
			appendTag(wdm, "Ref:Export:Id", f"FR:Quay:{osmVal}:")
			break


def appendCommonStationTags(osmTags: Dict[str,str], wdm: etree._Element):
	"""Appends generic tags to WDM Element read from an OSM public transport station

	Parameters
	----------
	osmTags : dict
		The OSM tags
	wdm : lxml.etree._Element
		The XML element representing a single WDM feature
	"""

	appendTagValueMapping(wdm, "Assistance", {"yes": "Boarding"}, osmTags.get("service:SNCF:acces_plus"))

	# AudioInformation
	wdmAud = None
	if osmTags.get("departures_board:speech_output") not in [None, "no"]:
		wdmAud = "Yes"
	elif osmTags.get("passenger_information_display:speech_output") not in [None, "no"]:
		wdmAud = "Yes"
	elif osmTags.get("announcement") not in [None, "no"]:
		wdmAud = "Yes"
	elif osmTags.get("departures_board:speech_output") == "no":
		wdmAud = "No"
	elif osmTags.get("passenger_information_display:speech_output") == "no":
		wdmAud = "No"
	elif osmTags.get("announcement") == "no":
		wdmAud = "No"

	appendTag(wdm, "AudioInformation", wdmAud)

	# VisualDisplays
	wdmVis = None
	if osmTags.get("departures_board") not in [None, "no", "timetable"]:
		wdmVis = "Yes"
	elif osmTags.get("passenger_information_display") not in [None, "no"]:
		wdmVis = "Yes"

	appendTag(wdm, "VisualDisplays", wdmVis)

	# LargePrintTimetable
	if osmTags.get("departures_board") == "timetable":
		appendTag(wdm, "LargePrintTimetable", "Yes")

	# RealTimeDepartures
	wdmRTD = None
	if osmTags.get("departures_board") == "realtime":
		wdmRTD = "Yes"
	elif osmTags.get("passenger_information_display") not in [None, "no"]:
		wdmRTD = "Yes"

	appendTag(wdm, "RealTimeDepartures", wdmRTD)

	# Ref:Export:Id
	osmUic = osmTags.get("ref:FR:uic8")
	if osmUic:
		appendTag(wdm, "Ref:Export:Id", f"FR:StopPlace:StopArea_OCE{osmUic}_rail:")


def appendCommonParkingTags(osmTags: Dict[str,str], wdm: etree._Element):
	"""Appends generic tags to WDM Element read from an OSM parking bay

	Parameters
	----------
	osmTags : dict
		The OSM tags
	wdm : lxml.etree._Element
		The XML element representing a single WDM feature
	"""

	appendTagValueMapping(wdm, "FlooringMaterial", mappings.TAG_SURFACE, osmTags.get("surface"))
	appendTagValueMapping(wdm, "Flooring", mappings.TAG_SMOOTHNESS, osmTags.get("smoothness"))
	appendTagValueMapping(
		wdm, "BayGeometry",
		{"perpendicular": "Orthogonal", "diagonal": "Angled", "parallel": "Parallel"},
		osmTags.get("orientation")
	)
	appendTagValueMapping(wdm, "ParkingVisibility", {"no": "Unmarked"}, osmTags.get("markings"))
	appendLightingTag(osmTags, wdm)
	appendTag(wdm, "Slope", getOsmSlopeValue(osmTags.get("incline", "")))
	appendTiltSideTag(osmTags, wdm)

	# VehicleRecharging
	osmCharging = osmTags.get("capacity:charging")
	if osmCharging is not None:
		appendTag(wdm, "VehicleRecharging", "No" if osmCharging == "0" else "Yes")

	# Length
	appendTag(wdm, "Length", getOsmSizeValue(osmTags, ["length", "parking_space:length"]))

	# Width
	appendTag(wdm, "Width", getOsmSizeValue(osmTags, ["width", "parking_space:width"]))

	# WheelchairAccess (if not accessible)
	osmWheelchair = osmTags.get("wheelchair")
	if osmWheelchair != "yes":
		appendTagValueMapping(wdm, "WheelchairAccess", mappings.TAG_YESNOBAD, osmWheelchair)


def appendCommonObstacleTags(osmTags: Dict[str,str], wdm: etree._Element):
	"""Appends generic tags to WDM Element read from an OSM obstacle

	Parameters
	----------
	osmTags : dict
		The OSM tags
	wdm : lxml.etree._Element
		The XML element representing a single WDM feature
	"""

	appendTag(wdm, "Length", getOsmSizeValue(osmTags, ["length", "est_length", "spacing"]))
	appendTag(wdm, "Width", getOsmSizeValue(osmTags, ["width", "est_width"]))
	appendTag(wdm, "RemainingWidth", getOsmSizeValue(osmTags, ["maxwidth:physical", "maxwidth", "opening"]))
	appendTag(wdm, "Height", getOsmSizeValue(osmTags, ["height", "est_height", "maxheight"]))
	appendTag(wdm, "Altitude", osmTags.get("ele"))
	appendTag(wdm, "PublicCode", osmTags.get("ref") or osmTags.get("local_ref"))
	appendTag(wdm, "MagneticInductionLoop", toWDMCase(osmTags.get("audio_loop") or osmTags.get("hearing_loop")))
	appendToiletsTags(osmTags, wdm)

	# KerbDesign
	osmKerb = osmTags.get("kerb")
	appendTagValueMapping(wdm, "KerbDesign", mappings.TAG_KERB, osmKerb)
	if osmKerb == "lowered":
		appendTag(wdm, "FixMe:KerbDesign", "Vérifier s’il s’agit d’un ressaut abaissé ou d’un bateau")

	# Bollard
	osmBollard = osmTags.get("crossing:bollard")
	appendTagValueMapping(wdm, "Bollard", mappings.TAG_YESNO_WITH_CONTRASTED, osmBollard)
	if osmBollard == "contrasted":
		appendTag(wdm, "Bollard:VisualContrast", "Yes")

	# KerbHeight
	osmKerbHeight = getOsmSizeValue(osmTags, ["kerb:height", "height", "est_height"])

	if osmKerbHeight is not None:
		appendTag(wdm, "KerbHeight", osmKerbHeight)
	elif osmKerb in ["raised", "lowered", "flush"]:
		appendTagValueMapping(wdm, "KerbHeight", {"raised": "0.05", "lowered": "0.03", "flush": "0.01"}, osmKerb)
		appendTag(wdm, "FixMe:KerbHeight", "Vérifier la hauteur du ressaut")

	# FreeToUse
	osmFee = osmTags.get("fee")
	osmCharge = osmTags.get("charge")

	if osmFee == "no":
		appendTag(wdm, "FreeToUse", "Yes")
	elif osmFee not in [None, "no"] or osmCharge is not None:
		appendTag(wdm, "FreeToUse", "No")

	# WheelchairAccess (specific toilets case)
	if getValue(wdm, "WheelchairAccess") is None and getValue(wdm, "Amenity") == "Toilets":
		appendTagValueMapping(wdm, "WheelchairAccess", mappings.TAG_YESNOBAD, osmTags.get("toilets:wheelchair"))


def appendCrossingTags(osmTags: Dict[str,str], wdm: etree._Element):
	"""Appends various tags for crossings

	Parameters
	----------
	osmTags : dict
		The OSM tags
	wdm : lxml.etree._Element
		The XML element representing a single WDM feature
	"""

	appendTag(wdm, "PedestrianLights", getWdmPedestrianLightsValue(osmTags), override=False)
	appendTagValueMapping(wdm, "CrossingIsland", mappings.TAG_YESNOBAD, osmTags.get("crossing:island"), override=False)

	# CrossingBarrier
	osmCrossingBarrier = osmTags.get("crossing:barrier")
	if osmCrossingBarrier == "no":
		appendTag(wdm, "CrossingBarrier", "No", override=False)
	elif osmCrossingBarrier is not None:
		appendTag(wdm, "CrossingBarrier", "Yes", override=False)

	# ZebraCrossing
	osmCrossing = osmTags.get("crossing")
	osmCrossingMarkings = osmTags.get("crossing:markings", "")
	osmCrossingRef = osmTags.get("crossing_ref")
	wdmZebraCrossing = None
	if osmCrossing == "unmarked":
		wdmZebraCrossing = "None"
	elif osmCrossingMarkings in ["no", "surface", "dots", "dashes", "pictograms"]:
		wdmZebraCrossing = "None"
	elif "zebra" in osmCrossingMarkings or osmCrossingRef == "zebra":
		wdmZebraCrossing = "Good"

	appendTag(wdm, "ZebraCrossing", wdmZebraCrossing, override=False)
	if wdmZebraCrossing == "Good":
		appendTag(wdm, "FixMe:ZebraCrossing", "Vérifier l’état du marquage de ce passage piéton", override=False)
	
	# AcousticCrossingAids
	wdmAcousticCrossingAids = None
	if osmTags.get("traffic_signals:sound") not in ["no", None]:
		wdmAcousticCrossingAids = "Good"
	elif osmTags.get("crossing:bell") not in ["no", None]:
		wdmAcousticCrossingAids = "Good"
	elif osmTags.get("traffic_signals:sound") == "no":
		wdmAcousticCrossingAids = "None"
	elif osmTags.get("crossing:bell") == "no":
		wdmAcousticCrossingAids = "None"

	appendTag(wdm, "AcousticCrossingAids", wdmAcousticCrossingAids, override=False)
	if wdmAcousticCrossingAids == "Good":
		appendTag(wdm, "FixMe:AcousticCrossingAids", "Vérifier l’état du dispositif d’aide sonore de ce passage piéton", override=False)

	# VibratingCrossingAids
	wdmVibratingCrossingAids = None
	if osmTags.get("traffic_signals:vibration") not in ["no", None]:
		wdmVibratingCrossingAids = "Good"
	elif osmTags.get("traffic_signals:floor_vibration") not in ["no", None]:
		wdmVibratingCrossingAids = "Good"
	elif osmTags.get("traffic_signals:vibration") == "no":
		wdmVibratingCrossingAids = "None"
	elif osmTags.get("traffic_signals:floor_vibration") == "no":
		wdmVibratingCrossingAids = "None"

	appendTag(wdm, "VibratingCrossingAids", wdmVibratingCrossingAids, override=False)
	if wdmVibratingCrossingAids == "Good":
		appendTag(wdm, "FixMe:VibratingCrossingAids", "Vérifier l'état du dispositif délivrant des vibrations de ce passage piéton", override=False)


def appendCommonPathTags(osmTags: Dict[str,str], wdm: etree._Element):
	"""Appends generic tags to WDM Element read from an OSM path

	Parameters
	----------
	osmTags : dict
		The OSM tags
	wdm : lxml.etree._Element
		The XML element representing a single WDM feature
	"""

	wdmSpl = getValue(wdm, "SitePathLink")

	appendTagValueMapping(wdm, "WheelchairAccess", mappings.TAG_YESNOBAD, osmTags.get("wheelchair"))
	appendTiltSideTag(osmTags, wdm)
	appendTag(wdm, "Slope", getOsmSlopeValue(osmTags.get("incline")))
	widthTags = ["door:width"] if osmTags.get("highway") == "elevator" else ["width", "est_width"]
	appendTag(wdm, "Width", getOsmSizeValue(osmTags, widthTags))
	appendTagValueMapping(wdm, "FlooringMaterial", mappings.TAG_SURFACE, osmTags.get("surface"))
	appendTagValueMapping(wdm, "Flooring", mappings.TAG_SMOOTHNESS, osmTags.get("smoothness"))
	appendLightingTag(osmTags, wdm)
	appendTagValueMapping(wdm, "CrossingIsland", mappings.TAG_YESNOBAD, osmTags.get("crossing:island"))

	# CrossingBarrier
	osmCrossingBarrier = osmTags.get("crossing:barrier")
	if osmCrossingBarrier == "no":
		appendTag(wdm, "CrossingBarrier", "No")
	elif osmCrossingBarrier is not None:
		appendTag(wdm, "CrossingBarrier", "Yes")

	appendTag(wdm, "Level", getOsmLevelValue(osmTags))
	appendTag(wdm, "StepHeight", getOsmSizeValue(osmTags, ["step:height"]))
	appendTag(wdm, "StepLength", getOsmSizeValue(osmTags, ["step:length"]))
	appendTagValueMapping(wdm, "StepCondition", {"even": "Even", "uneven": "Uneven", "rough": "Rough"}, osmTags.get("step:condition"))
	appendTagValueMapping(wdm, "Conveying", {"reversible": "Reversible", "backward": "Backward", "*": "Forward"}, osmTags.get("conveying"))
	appendTag(wdm, "MaxWeight", getOsmWeightValue(osmTags.get("maxweight", "")))

	# HighwayType
	osmHighway = osmTags.get("highway")
	wdmHighway = None

	if osmTags.get("footway") in ["sidewalk", "crossing"]:
		wdmHighway = "Street"
	elif osmHighway == "living_street":
		wdmHighway = "LivingStreet"
	elif osmHighway in ["pedestrian", "footway", "path"]:
		wdmHighway = "Pedestrian"
	elif osmHighway == "cycleway":
		wdmHighway = "Bicycle"
	elif osmTags.get("source:maxspeed") == "FR:zone30" or osmTags.get("zone:maxspeed") == "FR:30":
		wdmHighway = "LimitedSpeedStreet"

	appendTag(wdm, "HighwayType", wdmHighway)

	# StructureType
	wdmStructure = None

	if osmTags.get("tunnel") == "yes":
		wdmStructure = "Tunnel"
	elif osmTags.get("covered") == "yes":
		wdmStructure = "Underpass"
	elif osmTags.get("bridge") == "yes":
		wdmStructure = "Overpass"
	elif osmTags.get("indoor") == "yes":
		wdmStructure = "Corridor"

	appendTag(wdm, "StructureType", wdmStructure)

	# Outdoor
	wdmOutdoor = None if wdmSpl == "Elevator" else "Yes"

	if osmTags.get("indoor") == "yes" or osmTags.get("tunnel") == "yes":
		wdmOutdoor = "No"
	elif osmTags.get("tunnel") == "building_passage" or osmTags.get("covered") == "yes":
		wdmOutdoor = "Covered"

	appendTag(wdm, "Outdoor", wdmOutdoor)

	# Incline (only to know if goes up or down)
	osmIncline = osmTags.get("incline")
	wdmIncline = None

	if osmIncline == "up":
		wdmIncline = "Up"
	elif osmIncline == "down":
		wdmIncline = "Down"
	elif osmIncline is None:
		pass
	else:
		try:
			osmInclineVal = float(osmIncline.replace("%", "").replace("°", "").replace(" ", ""))
			if osmInclineVal > 0:
				wdmIncline = "Up"
			elif osmInclineVal < 0:
				wdmIncline = "Down"
		except ValueError:
			logging.warning("Invalid incline value: "+osmIncline)

	appendTag(wdm, "Incline", wdmIncline)

	# LinearCue
	osmTactilePaving = osmTags.get("tactile_paving")
	osmGuideStrips = osmTags.get("guide_strips")
	wdmTactileGuiding = None

	if osmTactilePaving in ["yes", "contrasted"]:
		appendTag(wdm, "LinearCue", "GuidingStrip")
	elif osmGuideStrips is not None:
		appendTag(wdm, "LinearCue", "GuidingStrip")

	# ZebraCrossing, AcousticCrossingAids, VibratingCrossingAids
	appendCrossingTags(osmTags, wdm)

	# VisualGuidanceBands
	osmCrossingMarkings = osmTags.get("crossing:markings")
	wdmVisualGuidanceBands = None
	if osmTactilePaving == "no" and osmCrossingMarkings in ["no", None]:
		wdmVisualGuidanceBands = "No"
	elif osmTactilePaving in ["yes", "constrated"]:
		wdmVisualGuidanceBands = "Yes"
	elif osmCrossingMarkings is not None and len([v in osmCrossingMarkings for v in mappings.TAG_CROSSING_MARKINGS_FOR_VISUAL_GUIDANCE]) > 0:
		wdmVisualGuidanceBands = "Yes"

	appendTag(wdm, "VisualGuidanceBands", wdmVisualGuidanceBands)

	# Handrail
	wdmHandrail = None
	osmHandrail = osmTags.get("handrail")
	if osmHandrail == "no":
		wdmHandrail = "None"
	elif osmHandrail in ["yes", "both"]:
		wdmHandrail = "Both"
	elif osmHandrail == "left" or osmTags.get("handrail:left") not in ["no", None]:
		wdmHandrail = "Left"
	elif osmHandrail == "right" or osmTags.get("handrail:right") not in ["no", None]:
		wdmHandrail = "Right"
	elif osmHandrail == "center" or osmTags.get("handrail:center") not in ["no", None]:
		wdmHandrail = "Center"

	appendTag(wdm, "Handrail", wdmHandrail)

	# Handrail:TactileWriting
	wdmHandrailTactileWriting = None
	if osmTags.get("tactile_writing") == "no":
		wdmHandrailTactileWriting = "No"
	if osmTags.get("tactile_writing") == "yes":
		wdmHandrailTactileWriting = "Yes"
	if osmTags.get("tactile_writing:embossed_printed_letters") not in ["no", None]:
		wdmHandrailTactileWriting = "Yes"
	if osmTags.get("tactile_writing:embossed_printed_letters:fr") not in ["no", None]:
		wdmHandrailTactileWriting = "Yes"
	if osmTags.get("tactile_writing:engraved_printed_letters") not in ["no", None]:
		wdmHandrailTactileWriting = "Yes"
	if osmTags.get("tactile_writing:braille") not in ["no", None]:
		wdmHandrailTactileWriting = "Yes"
	if osmTags.get("tactile_writing:braille:fr") not in ["no", None]:
		wdmHandrailTactileWriting = "Yes"

	appendTag(wdm, "Handrail:TactileWriting", wdmHandrailTactileWriting)

	# StepCount
	osmStepCount = osmTags.get("step_count") or osmTags.get("est_step_count")
	if osmStepCount is not None:
		try:
			stepCount = int(osmStepCount)
			if stepCount > 0:
				appendTag(wdm, "StepCount", str(stepCount))
		except ValueError:
			pass
	
	# StairRamp
	wdmStairRamp = None
	if osmTags.get("ramp") in ["no", "separate"]:
		wdmStairRamp = "None"
	elif osmTags.get("ramp:bicycle") not in ["no", None]:
		wdmStairRamp = "Bicycle"
	elif osmTags.get("ramp:luggage") not in ["no", None]:
		wdmStairRamp = "Luggage"
	elif osmTags.get("ramp:stroller") not in ["no", None]:
		wdmStairRamp = "Stroller"
	elif (
		osmTags.get("ramp") == "yes"
		and osmTags.get("ramp:bicycle") is None
		and osmTags.get("ramp:luggage") is None
		and osmTags.get("ramp:stroller") is None
	):
		wdmStairRamp = "Other"

	appendTag(wdm, "StairRamp", wdmStairRamp)


def toWDMCase(txt: Optional[str]):
	"""Converts input text into full-title case"""
	if not isinstance(txt, str):
		return txt

	parts = txt.split("_")
	res = ""

	for p in parts:
		if len(p) < 2:
			res += p.title()
		else:
			res += p[0].upper() + p[1:]

	return res


def hasMeaningfulWDMTags(feature):
	"""Check if given WDM feature has meaningful tags (not import ref)

	Parameters
	----------
	feature : lxml.etree._Element
		The WDM feature

	Returns
	-------
	bool
		True if feature has meaningful tags
	"""

	for tag in feature.iter("tag"):
		if tag.attrib["k"] not in mappings.TAG_WDM_NOT_MEANINGFUL:
			return True
	return False
