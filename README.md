# Osm2Wdm

This is the tool that imports OpenStreetMap data to WDM.

## Usage

### Install

Dependencies:

- Python >= 3.7

Launch following commands for running the converter:

```bash
# Init Python environment
python -m venv ./env
source ./env/bin/activate
pip install -e .
```

### Running

To filter a PBF file before running:

```bash
osmium tags-filter \
	--progress \
	-O -o output.xml \
	input.osm.pbf \
	`python -c "import osm2wdm.converter; print(osm2wdm.converter.getOsmiumFilter())"`
```

To run conversion over a OSM XML file in command-line:

```bash
python -c "import osm2wdm.converter as c; c.writeXMLFile(c.transformOsmIntoWDM(c.readXMLFile('input.osm.xml')), 'output.wdm.xml')"
```

Or as a Python script:

```python
import osm2wdm.converter as c

inputXML = c.readXMLFile('input.osm.xml')
outputWDM = c.transformOsmIntoWDM(inputXML)
c.writeXMLFile(outputWDM, 'output.wdm.xml')
```

### Testing

```bash
pip install -e .[dev]
pytest
```

### Performance tests

For testing performance, you can create a small `test.py` file:

```python
import osm2wdm.converter as c
osmXml = c.readXMLFile('input.xml')
wdmXml = c.transformOsmIntoWDM(osmXml)
c.writeXMLFile(wdmXml, 'output.xml')
```

And then, do some profiling:

```bash
python -m cProfile -o test.prof test.py
pip install snakeviz
snakeviz test.prof
```

## Useful links

* [WDM](https://gitlab.com/yukaimaps/yukaidi-tagging-schema/-/blob/main/doc/Walk_data_model.md): documentation of the Walk Data Model (WDM) used here
