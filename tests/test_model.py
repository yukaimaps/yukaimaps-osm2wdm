import pytest
from lxml import etree
from lxml.builder import E
from osm2wdm.model import sortXml, XmlDataSet, OsmDataSet, WalkDataSet
from collections import OrderedDict
from . import test_tags

def test_sortXml_same_tag_by_id():
    e1 = E.node(id="1")
    e2 = E.node(id="2")
    assert sortXml(e1, e2) == -1
    assert sortXml(e2, e1) == 1

def test_sortXml_different_tags():
    e1 = E.node(id="1")
    e2 = E.way(id="2")
    e3 = E.relation(id="3")
    assert sortXml(e1, e2) == -1
    assert sortXml(e2, e1) == 1
    assert sortXml(e3, e1) == 1
    assert sortXml(e2, e3) == -1

def test_XmlDataSet_indexFeatures():
    xml = E.osm()
    node = etree.SubElement(xml, "node", id="1")
    way = etree.SubElement(xml, "way", id="2")
    relation = etree.SubElement(xml, "relation", id="3")

    dataset = XmlDataSet(xml)

    assert dataset.nodes == {"1": node}
    assert dataset.ways == {"2": way}
    assert dataset.relations == {"3": relation}

def test_XmlDataSet_search_key_value():
    node1 = E.node(E.tag(k="amenity", v="school"), id="1")
    node2 = E.node(E.tag(k="amenity", v="hospital"), id="2")
    node3 = E.node(E.tag(k="amenity", v="school"), id="3")
    xml = E.osm(node1, node2, node3)

    dataset = OsmDataSet(xml)
    result = dataset.search("amenity", "school")

    assert len(result) == 2
    assert node1 in result
    assert node3 in result


def test_XmlDataSet_search_key_only():
    node1 = E.node(E.tag(k="amenity", v="school"), id="1")
    node2 = E.node(E.tag(k="amenity", v="hospital"), id="2")
    node3 = E.node(E.tag(k="amenity", v="school"), id="3")
    xml = E.osm(node1, node2, node3)

    dataset = XmlDataSet(xml)
    result = dataset.search("amenity", None)

    assert len(result) == 3
    assert node1 in result
    assert node2 in result
    assert node3 in result


def test_XmlDataSet_search_cached_key_value():
    node1 = E.node(E.tag(k="amenity", v="school"), id="1")
    node2 = E.node(E.tag(k="amenity", v="hospital"), id="2")
    node3 = E.node(E.tag(k="amenity", v="school"), id="3")
    xml = E.osm(node1, node2, node3)

    dataset = XmlDataSet(xml)
    _ = dataset.search("amenity", "school")  # First search to populate the cache

    # Now, modifying the XML to ensure cache is used
    node4 = etree.SubElement(xml, "node", E.tag(k="amenity", v="school"), id="4")

    result = dataset.search("amenity", "school")

    assert len(result) == 2  # Should return 2 because the cache is used
    assert node1 in result
    assert node3 in result
    assert node4 not in result


def test_XmlDataSet_search_cached_key_only():
    node1 = E.node(E.tag(k="amenity", v="school"), id="1")
    node2 = E.node(E.tag(k="amenity", v="hospital"), id="2")
    node3 = E.node(E.tag(k="amenity", v="school"), id="3")
    xml = E.osm(node1, node2, node3)

    dataset = XmlDataSet(xml)
    _ = dataset.search("amenity", None)  # First search to populate the cache

    # Now, modifying the XML to ensure cache is used
    node4 = etree.SubElement(xml, "node", E.tag(k="amenity", v="school"), id="4")

    result = dataset.search("amenity", None)

    assert len(result) == 3  # Should return 3 because the cache is used
    assert node1 in result
    assert node2 in result
    assert node3 in result
    assert node4 not in result


def test_OsmDataSet_indexFeatures():
    xml = E.osm()
    node = etree.SubElement(xml, "node", id="1")
    way = etree.SubElement(xml, "way", id="2")
    nd = etree.SubElement(way, "nd", ref="1")
    relation = etree.SubElement(xml, "relation", id="3")
    member = etree.SubElement(relation, "member", type="node", ref="1")

    dataset = OsmDataSet(xml)

    assert dataset.nodes == {"1": node}
    assert dataset.ways == {"2": way}
    assert dataset.relations == {"3": relation}
    assert dataset.nodeParentWays == {"1": {way}}
    assert dataset.relationsByMember == {
        "node": {"1": {relation}},
        "way": {},
        "relation": {}
    }


def test_OsmDataSet_isInRelation_true():
	osmFeature = E.node(id="1")
	osmRel = test_tags.setOsmTags(E.relation(
		E.member(type="node", ref="1", role=""),
		id="1"
	), {"type": "route", "route": "bus"})
	ods = OsmDataSet(E.osm(osmFeature, osmRel))

	assert ods.isInRelation(osmFeature, {"route": "bus"})


def test_OsmDataSet_isInRelation_tagsNotMatch():
	osmFeature = E.node(id="1")
	osmRel = test_tags.setOsmTags(E.relation(
		E.member(type="node", ref="1", role=""),
		id="1"
	), {"type": "route", "route": "coach"})
	ods = OsmDataSet(E.osm(osmFeature, osmRel))

	assert not ods.isInRelation(osmFeature, {"route": "bus"})


def test_OsmDataSet_isInRelation_notInRelation():
	osmFeature = E.node(id="1")
	osmRel = test_tags.setOsmTags(E.relation(
		E.member(type="way", ref="1", role=""),
		id="1"
	), {"type": "route", "route": "bus"})
	ods = OsmDataSet(E.osm(osmFeature, osmRel))

	assert not ods.isInRelation(osmFeature, {"route": "bus"})


def test_WalkDataSet_sort():
    xml = E.osm()
    node1 = etree.SubElement(xml, "node", id="2")
    node2 = etree.SubElement(xml, "node", id="1")
    way = etree.SubElement(xml, "way", id="3")
    relation = etree.SubElement(xml, "relation", id="4")

    dataset = WalkDataSet(xml)
    dataset.sort()

    sorted_ids = [child.attrib['id'] for child in dataset.xml]
    assert sorted_ids == ["1", "2", "3", "4"]


def test_WalkDataSet_hasOsmFeatureImported_false():
	osm = E.node(lat="47.7", lon="-1.12", id="1")
	wds = WalkDataSet()
	assert not wds.hasOsmFeatureImported(osm)


def test_WalkDataSet_hasOsmFeatureImported_true():
	osm = E.node(lat="47.7", lon="-1.12", id="1")
	wdmNode = E.node(E.tag(k="Ref:Import:Id", v="n1"), id="1")
	wds = WalkDataSet(
		E.osm(wdmNode),
		importedOsmFeaturesById = {"node": {"1": set([wdmNode])}, "way": {}, "relation": {}}
	)
	assert wds.hasOsmFeatureImported(osm)


def test_WalkDataSet_hasOsmFeatureImported_type():
	osm = E.node(lat="47.7", lon="-1.12", id="1")
	wds = WalkDataSet(
		E.osm(E.way(E.tag(k="Ref:Import:Id", v="w1"), id="1"))
	)
	assert not wds.hasOsmFeatureImported(osm)


def test_WalkDataSet_hasOsmFeatureImported_simpl():
	osm = E.way(lat="47.7", lon="-1.12", id="1")
	wdmNode = E.node(E.tag(k="Ref:Import:Geometry:Id", v="w1"), id="1")
	wds = WalkDataSet(
		E.osm(wdmNode),
		importedSimplifiedOsmFeaturesById = {"node": {}, "way": {"1": set([wdmNode])}, "relation": {}}
	)
	assert wds.hasOsmFeatureImported(osm, True)
	assert not wds.hasOsmFeatureImported(osm)


def test_WalkDataSet_hasOsmFeatureImported_tags():
	osm = E.node(lat="47.7", lon="-1.12", id="1")
	wdmNode = E.node(E.tag(k="Ref:Import:Id", v="n1"), id="1")
	wds = WalkDataSet(
		E.osm(wdmNode),
		importedOsmFeaturesById = {"node": {"1": set([wdmNode])}, "way": {}, "relation": {}}
	)
	assert wds.hasOsmFeatureImported(osm)
	assert wds.hasOsmFeatureImported(osm, checkTags = False)
	assert not wds.hasOsmFeatureImported(osm, checkTags = True)

	wdmNode = E.node(E.tag(k="Ref:Import:Id", v="n1"), E.tag(k="Quay", v="Bus"), id="1")
	wds2 = WalkDataSet(
		E.osm(wdmNode),
		importedOsmFeaturesById = {"node": {"1": set([wdmNode])}, "way": {}, "relation": {}}
	)
	assert wds2.hasOsmFeatureImported(osm)
	assert wds2.hasOsmFeatureImported(osm, checkTags = False)
	assert wds2.hasOsmFeatureImported(osm, checkTags = True)


def test_WalkDataSet_getNextNewElementId():
	wds = WalkDataSet()
	assert wds.getNextNewElementId("node") == -1
	assert wds.getNextNewElementId("way") == -1
	assert wds.getNextNewElementId("relation") == -1
	assert wds.getNextNewElementId("node") == -2
	assert wds.getNextNewElementId("relation") == -2
	assert wds.getNextNewElementId("relation") == -3
	assert wds.getNextNewElementId("way") == -2



def test_WalkDataSet_insert_emptyWdm():
	wds = WalkDataSet()
	n1 = E.node(lat="1", lon="1", id="1")
	wds.insert(n1)
	assert len(wds.xml) == 1
	assert wds.xml[0] == n1


def test_WalkDataSet_insert_nodesOnly():
	n1 = E.node(lat="1", lon="1", id="1")
	n2 = E.node(lat="1.01", lon="1", id="2")
	n3 = E.node(lat="1.02", lon="1", id="3")
	wds = WalkDataSet(E.osm(n1, n2))
	wds.insert(n3)
	assert len(wds.xml) == 3
	assert wds.xml[2] == n3


def test_WalkDataSet_insert_nodesWays():
	n1 = E.node(lat="1", lon="1", id="1")
	n2 = E.node(lat="1.01", lon="1", id="2")
	n3 = E.node(E.tag(k="Ref:Import:Id", v="n-1"), lat="1.02", lon="1", id="3")
	w1 = E.way(E.nd(ref="1"), E.nd(ref="2"), id="1")
	wds = WalkDataSet(
		E.osm(n1, n2, w1)
	)
	wds.insert(n3)
	wds.sort()
	print(etree.tostring(wds.xml))
	assert len(wds.xml) == 4
	assert wds.xml[0] == n1
	assert wds.xml[1] == n2
	assert wds.xml[2] == n3
	assert wds.xml[3] == w1
	assert wds.importedOsmFeaturesById == {"node": {"-1": set([wds.xml[2]])}, "way": {}, "relation": {}}


def test_WalkDataSet_insert_ways():
	n1 = E.node(lat="1", lon="1", id="1")
	n2 = E.node(lat="1.01", lon="1", id="2")
	n3 = E.node(lat="1.02", lon="1", id="3")
	w1 = E.way(E.nd(ref="1"), E.nd(ref="2"), id="1")
	w2 = E.way(E.nd(ref="2"), E.nd(ref="3"), id="2")
	wds = WalkDataSet(E.osm(n1, n2, n3, w1))
	wds.insert(w2)
	assert len(wds.xml) == 5
	assert wds.xml[0] == n1
	assert wds.xml[1] == n2
	assert wds.xml[2] == n3
	assert wds.xml[3] == w1
	assert wds.xml[4] == w2
