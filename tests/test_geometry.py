import pytest
from lxml import etree
from lxml.builder import E
from osm2wdm import geometry, converter, model
from . import test_tags
from shapely import Point, LineString, STRtree, Polygon # type: ignore
from random import randint
import datetime


def test_copyOsmGeomToWdm_firstNode():
	osmFeature = E.way(E.nd(ref="1"), E.nd(ref="2"), E.nd(ref="3"), E.nd(ref="1"), id="42")
	osm = E.osm(
		E.node(lat="0", lon="0", id="1"),
		E.node(lat="0", lon="1", id="2"),
		E.node(lat="1", lon="1", id="3"),
		osmFeature
	)

	wds = model.WalkDataSet(ods=model.OsmDataSet(osm))
	res = geometry.copyOsmGeomToWdm(osmFeature, wds, True)
	print(etree.tostring(res))

	assert len(wds.xml) == 1
	assert wds.xml[0] == res
	assert res.tag == "node"
	assert res.attrib["lon"] == "0.6666667"
	assert res.attrib["lat"] == "0.3333333"
	#test_tags.assertTag(res, "Ref:Import:Id", "n1")
	test_tags.assertTag(res, "Ref:Import:Geometry:Id", "w42")


def test_copyOsmGeomToWdm_unsupported_reltype():
	osmFeature = E.relation(E.tag(k="type", v="bla"), id="1")
	osm = E.osm(
		osmFeature
	)

	wds = model.WalkDataSet(ods=model.OsmDataSet(osm))

	with pytest.raises(Exception) as e_info:
		geometry.copyOsmGeomToWdm(osmFeature, wds)

	assert str(e_info.value) == "Unsupported relation type: bla"


def test_copyOsmGeomToWdm_unsupported_featuretype():
	osmFeature = E.nothing(id="1")
	osm = E.osm(
		osmFeature
	)

	wds = model.WalkDataSet(ods=model.OsmDataSet(osm))

	with pytest.raises(Exception) as e_info:
		geometry.copyOsmGeomToWdm(osmFeature, wds)

	assert str(e_info.value) == "Unsupported geometry type: nothing"


def test_copyOsmNodeToWdm_basic():
	osmNode = E.node(lat="47.7", lon="-1.12", id="1")
	osm = E.osm(osmNode)

	wds = model.WalkDataSet(ods=model.OsmDataSet(osm))
	res = geometry.copyOsmNodeToWdm(osmNode, wds)

	resNodes = wds.xml.findall("node")
	assert len(resNodes) == 1
	resNode = resNodes[0]
	assert resNode.attrib["lat"] == "47.7"
	assert resNode.attrib["lon"] == "-1.12"
	assert resNode.attrib["id"] == "-1"
	assert res.attrib["id"] == "-1"
	test_tags.assertTag(resNode, "Ref:Import:Id", "n1")


def test_copyOsmNodeToWdm_reuse():
	osmNode = E.node(lat="47.7", lon="-1.12", id="1")
	osm = E.osm(osmNode)

	wdmN1 = test_tags.setOsmTags(E.node(lat="47.7", lon="-1.12", id="-1"), {"Ref:Import:Id": "n1"})
	wds = model.WalkDataSet(
		E.osm(wdmN1),
		ods=model.OsmDataSet(osm),
		importedOsmFeaturesById={"node": {"1": set([wdmN1])}, "way": {}, "relation": {}},
		osm2wdmNodesIds={"1":"-1"}
	)
	res = geometry.copyOsmNodeToWdm(osmNode, wds)

	resNodes = wds.xml.findall("node")
	assert len(resNodes) == 1
	resNode = resNodes[0]
	assert resNode.attrib["lat"] == "47.7"
	assert resNode.attrib["lon"] == "-1.12"
	assert resNode.attrib["id"] == "-1"
	assert res.attrib["id"] == "-1"
	test_tags.assertTag(resNode, "Ref:Import:Id", "n1")


def test_copyOsmNodeToWdm_sort():
	osmNode = E.node(lat="47.7", lon="-1.12", id="1")
	osm = E.osm(osmNode)

	wds = model.WalkDataSet(E.osm(
		E.node(lat="-1", lon="-1", id="1"),
		E.node(lat="-1.01", lon="-1.01", id="2"),
		E.way(E.nd(ref="1"), E.nd(ref="2"), id="1")
	), ods=model.OsmDataSet(osm))

	res = geometry.copyOsmNodeToWdm(osmNode, wds)
	wds.sort()
	#print(etree.tostring(wds.xml))
	assert len(wds.xml) == 4
	assert wds.xml[0] == res
	assert wds.xml[0].attrib["id"] == "-1"
	assert wds.xml[1].tag == "node"
	assert wds.xml[1].attrib["id"] == "1"
	assert wds.xml[2].tag == "node"
	assert wds.xml[2].attrib["id"] == "2"
	assert wds.xml[3].tag == "way"
	assert wds.xml[3].attrib["id"] == "1"


def test_copyOsmWayToWdm_basic():
	osmWay = E.way(
		E.nd(ref="1"),
		E.nd(ref="2"),
		E.nd(ref="3"),
		E.nd(ref="4"),
		E.nd(ref="1"),
		id="1"
	)
	osm = E.osm(
		E.node(lat="0", lon="0", id="1"),
		E.node(lat="1", lon="0", id="2"),
		E.node(lat="1", lon="1", id="3"),
		E.node(lat="0", lon="1", id="4"),
		osmWay
	)

	wds = model.WalkDataSet(ods=model.OsmDataSet(osm))

	res = geometry.copyOsmWayToWdm(osmWay, wds)
	resWays = wds.xml.findall("way")
	resNodes = wds.xml.findall("node")

	assert len(resNodes) == 4
	assert [ n.attrib["id"] for n in resNodes ] == [ "-1", "-2", "-3", "-4" ]
	assert len(resWays) == 1
	resWay = resWays[0]
	assert resWay.attrib["id"] == "-1"
	assert res.attrib["id"] == "-1"

	resWayNds = resWay.findall("nd")
	assert len(resWayNds) == 5
	assert [ n.attrib["ref"] for n in resWayNds ] == [ "-1", "-2", "-3", "-4", "-1" ]


def test_copyOsmWayToWdm_existingNodes():
	osmWay = E.way(
		E.nd(ref="1"),
		E.nd(ref="2"),
		E.nd(ref="3"),
		E.nd(ref="1"),
		id="1"
	)
	osmN1 = E.node(lat="0", lon="0", id="1")
	osmN2 = E.node(lat="1", lon="1", id="2")
	osmN3 = E.node(lat="0", lon="1", id="3")
	osm = E.osm(osmN1, osmN2, osmN3, osmWay)

	wdmN1 = test_tags.setOsmTags(E.node(lat="0", lon="0", id="-1"), {"PointOfInterest": "Shop", "Ref:Import:Id": "n1"})
	wds = model.WalkDataSet(
		E.osm(wdmN1),
		ods=model.OsmDataSet(osm),
		importedOsmFeaturesById={"node": {"1": set([wdmN1])}, "way": {}, "relation": {}},
		ids={"node": -1, "way": 0, "relation": 0},
		osm2wdmNodesIds={"1":"-1"}
	)

	res = geometry.copyOsmWayToWdm(osmWay, wds)
	resWays = wds.xml.findall("way")
	resNodes = wds.xml.findall("node")

	assert len(resNodes) == 3
	assert [ n.attrib["id"] for n in resNodes ] == [ "-1", "-2", "-3" ]
	assert len(resWays) == 1
	resWay = resWays[0]
	assert resWay.attrib["id"] == "-1"
	assert res.attrib["id"] == "-1"

	resWayNds = resWay.findall("nd")
	assert len(resWayNds) == 4
	assert [ n.attrib["ref"] for n in resWayNds ] == [ "-1", "-2", "-3", "-1" ]

	resExistingNode = wds.xml.find("node[@id=\"-1\"]")
	test_tags.assertTag(resExistingNode, "PointOfInterest", "Shop")


def test_copyOsmMultipolygonToWdm_outer_only():
	osmN1 = E.node(lat="0", lon="0", id="1")
	osmN2 = E.node(lat="1", lon="1", id="2")
	osmN3 = E.node(lat="0", lon="1", id="3")
	osmW1 = E.way(
		E.nd(ref="1"),
		E.nd(ref="2"),
		E.nd(ref="3"),
		E.nd(ref="1"),
		id="1"
	)
	osmR1 = E.relation(
		E.tag(k="type", v="multipolygon"),
		E.tag(k="public_transport", v="station"),
		E.member(type="way", ref="1", role="outer"),
		id="1"
	)
	osm = E.osm(osmN1, osmN2, osmN3, osmW1, osmR1)

	wds = model.WalkDataSet(ods=model.OsmDataSet(osm))
	res = geometry.copyOsmMultipolygonToWdm(osmR1, wds)
	resRels = wds.xml.findall("relation")
	resWays = wds.xml.findall("way")
	resNodes = wds.xml.findall("node")

	assert len(resRels) == 0
	assert len(resWays) == 1
	assert len(resNodes) == 3

	assert [ n.attrib["id"] for n in resNodes ] == [ "-1", "-2", "-3" ]
	assert resWays[0].attrib["id"] == "-1"
	test_tags.assertTag(resWays[0], "Ref:Import:Id", "r1")

	resWayNds = resWays[0].findall("nd")
	assert len(resWayNds) == 4
	assert [ n.attrib["ref"] for n in resWayNds ] == [ "-1", "-2", "-3", "-1" ]


def test_copyOsmMultipolygonToWdm_norole():
	osmN1 = E.node(lat="0", lon="0", id="1")
	osmN2 = E.node(lat="1", lon="1", id="2")
	osmN3 = E.node(lat="0", lon="1", id="3")
	osmW1 = E.way(
		E.nd(ref="1"),
		E.nd(ref="2"),
		E.nd(ref="3"),
		E.nd(ref="1"),
		id="1"
	)
	osmR1 = E.relation(
		E.tag(k="type", v="multipolygon"),
		E.tag(k="public_transport", v="station"),
		E.member(type="way", ref="1", role=""),
		id="1"
	)
	osm = E.osm(osmN1, osmN2, osmN3, osmW1, osmR1)

	wds = model.WalkDataSet(ods=model.OsmDataSet(osm))
	res = geometry.copyOsmMultipolygonToWdm(osmR1, wds)
	resRels = wds.xml.findall("relation")
	resWays = wds.xml.findall("way")
	resNodes = wds.xml.findall("node")

	assert len(resRels) == 0
	assert len(resWays) == 1
	assert len(resNodes) == 3

	assert [ n.attrib["id"] for n in resNodes ] == [ "-1", "-2", "-3" ]
	assert resWays[0].attrib["id"] == "-1"
	test_tags.assertTag(resWays[0], "Ref:Import:Id", "r1")

	resWayNds = resWays[0].findall("nd")
	assert len(resWayNds) == 4
	assert [ n.attrib["ref"] for n in resWayNds ] == [ "-1", "-2", "-3", "-1" ]


def test_copyOsmMultipolygonToWdm_2_closed_outers():
	osmN1 = E.node(lat="0", lon="0", id="1")
	osmN2 = E.node(lat="1", lon="1", id="2")
	osmN3 = E.node(lat="0", lon="1", id="3")
	osmN4 = E.node(lat="10", lon="10", id="4")
	osmN5 = E.node(lat="11", lon="11", id="5")
	osmN6 = E.node(lat="10", lon="11", id="6")
	osmN7 = E.node(lat="10", lon="9", id="7")
	osmW1 = E.way(
		E.nd(ref="1"),
		E.nd(ref="2"),
		E.nd(ref="3"),
		E.nd(ref="1"),
		id="1"
	)
	osmW2 = E.way(
		E.nd(ref="4"),
		E.nd(ref="5"),
		E.nd(ref="6"),
		E.nd(ref="7"),
		E.nd(ref="4"),
		id="2"
	)
	osmR1 = E.relation(
		E.tag(k="type", v="multipolygon"),
		E.tag(k="public_transport", v="station"),
		E.member(type="way", ref="1", role="outer"),
		E.member(type="way", ref="2", role="outer"),
		id="1"
	)
	osm = E.osm(
		osmN1, osmN2, osmN3, osmN4, osmN5, osmN6, osmN7,
		osmW1, osmW2,
		osmR1
	)

	wds = model.WalkDataSet(ods=model.OsmDataSet(osm))
	res = geometry.copyOsmMultipolygonToWdm(osmR1, wds)
	resRels = wds.xml.findall("relation")
	resWays = wds.xml.findall("way")
	resNodes = wds.xml.findall("node")

	assert len(resRels) == 0
	assert len(resWays) == 1
	assert len(resNodes) == 4

	assert [ n.attrib["id"] for n in resNodes ] == [ "-1", "-2", "-3", "-4" ]
	assert resWays[0].attrib["id"] == "-1"
	test_tags.assertTag(resWays[0], "Ref:Import:Id", "r1")

	resWayNds = resWays[0].findall("nd")
	assert len(resWayNds) == 5
	assert [ n.attrib["ref"] for n in resWayNds ] == [ "-1", "-2", "-3", "-4", "-1" ]


def test_copyOsmMultipolygonToWdm_2_nonclosed_outers():
	osmN1 = E.node(lat="0", lon="0", id="1")
	osmN2 = E.node(lat="1", lon="1", id="2")
	osmN3 = E.node(lat="0", lon="1", id="3")
	osmN4 = E.node(lat="0.5", lon="0.5", id="4")
	osmW1 = E.way(
		E.nd(ref="1"),
		E.nd(ref="2"),
		E.nd(ref="3"),
		id="1"
	)
	osmW2 = E.way(
		E.nd(ref="3"),
		E.nd(ref="4"),
		E.nd(ref="1"),
		id="2"
	)
	osmR1 = E.relation(
		E.tag(k="type", v="multipolygon"),
		E.tag(k="public_transport", v="station"),
		E.member(type="way", ref="1", role="outer"),
		E.member(type="way", ref="2", role="outer"),
		id="1"
	)
	osm = E.osm(
		osmN1, osmN2, osmN3, osmN4,
		osmW1, osmW2,
		osmR1
	)

	wds = model.WalkDataSet(ods=model.OsmDataSet(osm))
	res = geometry.copyOsmMultipolygonToWdm(osmR1, wds)
	resRels = wds.xml.findall("relation")
	resWays = wds.xml.findall("way")
	resNodes = wds.xml.findall("node")

	assert len(resRels) == 0
	assert len(resWays) == 1
	assert len(resNodes) == 4

	assert [ n.attrib["id"] for n in resNodes ] == [ "-1", "-2", "-3", "-4" ]
	assert resWays[0].attrib["id"] == "-1"
	test_tags.assertTag(resWays[0], "Ref:Import:Id", "r1")

	resWayNds = resWays[0].findall("nd")
	assert len(resWayNds) == 5
	assert [ n.attrib["ref"] for n in resWayNds ] == [ "-1", "-2", "-3", "-4", "-1" ]


def test_copyOsmMultipolygonToWdm_many_nonclosed_outers():
	osmN2520324069 = E.node(id='2520324069', lat='46.8799747', lon='4.7610399')
	osmN2520324070 = E.node(id='2520324070', lat='46.8799884', lon='4.76112')
	osmN2520324073 = E.node(id='2520324073', lat='46.8802573', lon='4.76113')
	osmN2520324076 = E.node(id='2520324076', lat='46.8804149', lon='4.7610609')
	osmN2520324077 = E.node(id='2520324077', lat='46.8804263', lon='4.7611073')
	osmN2520324079 = E.node(id='2520324079', lat='46.8805909', lon='4.7609636')
	osmN2520324081 = E.node(id='2520324081', lat='46.8806019', lon='4.7610284')
	osmN2520324084 = E.node(id='2520324084', lat='46.8813971', lon='4.7604255')
	osmN2520324086 = E.node(id='2520324086', lat='46.8816308', lon='4.7604679')
	osmN2520324087 = E.node(id='2520324087', lat='46.8816391', lon='4.7605104')
	osmN3488734504 = E.node(id='3488734504', lat='46.8810242', lon='4.7607763')
	osmN3488734510 = E.node(id='3488734510', lat='46.8810573', lon='4.7605634')
	osmN3488734524 = E.node(id='3488734524', lat='46.8814453', lon='4.7604342')
	osmN5820885934 = E.node(id='5820885934', lat='46.8804186', lon='4.7610777')
	osmN9663189232 = E.node(id='9663189232', lat='46.8808862', lon='4.7606387')
	osmN9663189233 = E.node(id='9663189233', lat='46.8805968', lon='4.7607661')
	osmN11364570736 = E.node(id='11364570736', lat='46.8810863', lon='4.7607495')
	osmW341778928 = E.way(
		E.nd(ref='3488734524'),
		E.nd(ref='2520324084'),
		E.nd(ref='3488734510'),
		id='341778928'
	)
	osmW244757370 = E.way(
		E.nd(ref='3488734510'),
		E.nd(ref='9663189232'),
		E.nd(ref='9663189233'),
		E.nd(ref='2520324069'),
		E.nd(ref='2520324070'),
		E.nd(ref='2520324073'),
		E.nd(ref='2520324076'),
		E.nd(ref='5820885934'),
		E.nd(ref='2520324077'),
		E.nd(ref='2520324081'),
		E.nd(ref='2520324079'),
		id='244757370'
	)
	osmW341778922 = E.way(
		E.nd(ref='2520324079'),
		E.nd(ref='3488734504'),
		id='341778922'
	)
	osmW341778929 = E.way(
		E.nd(ref='3488734504'),
		E.nd(ref='11364570736'),
		E.nd(ref='2520324087'),
		E.nd(ref='2520324086'),
		E.nd(ref='3488734524'),
		id='341778929'
	)
	osmR1 = E.relation(
		E.member(type='way', ref='341778928', role='outer'),
		E.member(type='way', ref='244757370', role='outer'),
		E.member(type='way', ref='341778922', role='outer'),
		E.member(type='way', ref='341778929', role='outer'),
		E.tag(k="type", v="multipolygon"),
		E.tag(k="public_transport", v="station"),
		id='4850654'
	)
	osm = E.osm(
		osmN2520324069, osmN2520324070, osmN2520324073, osmN2520324076, osmN2520324077, osmN2520324079,
		osmN2520324081, osmN2520324084, osmN2520324086, osmN2520324087, osmN3488734504, osmN3488734510,
		osmN3488734524, osmN5820885934, osmN9663189232, osmN9663189233, osmN11364570736,
		osmW341778928, osmW244757370, osmW341778922, osmW341778929,
		osmR1
	)

	wds = model.WalkDataSet(ods=model.OsmDataSet(osm))
	res = geometry.copyOsmMultipolygonToWdm(osmR1, wds)
	resRels = wds.xml.findall("relation")
	resWays = wds.xml.findall("way")
	resNodes = wds.xml.findall("node")

	assert len(resRels) == 0
	assert len(resWays) == 1
	assert len(resNodes) == 17

	assert resWays[0].attrib["id"] == "-1"
	resWayNds = resWays[0].findall("nd")
	assert len(resWayNds) == 18
	test_tags.assertTag(resWays[0], "Ref:Import:Id", "r4850654")


def test_copyOsmMultipolygonToWdm_nonclosed_outer():
	osmN1 = E.node(lat="0", lon="0", id="1")
	osmN2 = E.node(lat="1", lon="1", id="2")
	osmN3 = E.node(lat="2", lon="2", id="3")
	osmW1 = E.way(
		E.nd(ref="1"),
		E.nd(ref="2"),
		id="1"
	)
	osmW2 = E.way(
		E.nd(ref="2"),
		E.nd(ref="3"),
		id="2"
	)
	osmR1 = E.relation(
		E.tag(k="type", v="multipolygon"),
		E.tag(k="public_transport", v="station"),
		E.member(type="way", ref="1", role="outer"),
		E.member(type="way", ref="2", role="outer"),
		id="1"
	)
	osm = E.osm(
		osmN1, osmN2, osmN3,
		osmW1, osmW2,
		osmR1
	)

	wds = model.WalkDataSet(ods=model.OsmDataSet(osm))
	res = geometry.copyOsmMultipolygonToWdm(osmR1, wds)
	resRels = wds.xml.findall("relation")
	resWays = wds.xml.findall("way")
	resNodes = wds.xml.findall("node")

	assert len(resRels) == 0
	assert len(resWays) == 0
	assert len(resNodes) == 1

	n = resNodes[0]
	assert n.attrib["id"] == "-1"
	assert n.attrib["lat"] == "0"
	assert n.attrib["lon"] == "0"
	test_tags.assertTag(n, "Ref:Import:Id", "r1")


def test_getRoadSegmentAroundCrossing_1path():
	wayNode1 = E.node(lat="-0.001", lon="0", id="1")
	wayNode2 = E.node(lat="0", lon="0", id="2")
	wayNode3 = E.node(lat="0.001", lon="0", id="3")
	way1 = E.way(E.nd(ref="1"), E.nd(ref="2"), E.nd(ref="3"), E.tag(k="highway", v="residential"), id="1")

	indepNode1 = E.node(lat="1", lon="0", id="10")

	osm = E.osm(
		indepNode1,
		wayNode1, wayNode2, wayNode3,
		way1
	)

	wds = model.WalkDataSet(ods=model.OsmDataSet(osm))

	assert geometry.getRoadSegmentAroundCrossing(indepNode1, wds) is None
	assert geometry.getRoadSegmentAroundCrossing(wayNode1, wds) == [None, wayNode1, wayNode2]
	assert geometry.getRoadSegmentAroundCrossing(wayNode2, wds) == [wayNode1, wayNode2, wayNode3]
	assert geometry.getRoadSegmentAroundCrossing(wayNode3, wds) == [wayNode2, wayNode3, None]


def test_getRoadSegmentAroundCrossing_2followingPaths():
	n1 = E.node(lat="-0.001", lon="0", id="1")
	n2 = E.node(lat="0", lon="0", id="2")
	n3 = E.node(lat="0.001", lon="0", id="3")
	n4 = E.node(lat="0.002", lon="-0.001", id="4")
	n5 = E.node(lat="0.002", lon="0.001", id="5")

	way1 = E.way(E.nd(ref="1"), E.nd(ref="2"), E.tag(k="highway", v="residential"), id="1")
	way2 = E.way(E.nd(ref="2"), E.nd(ref="3"), E.nd(ref="4"), E.tag(k="highway", v="residential"), id="2")
	way3 = E.way(E.nd(ref="3"), E.nd(ref="5"), E.tag(k="highway", v="residential"), id="3")

	osm = E.osm(
		n1, n2, n3, n4, n5,
		way1, way2, way3
	)

	wds = model.WalkDataSet(ods=model.OsmDataSet(osm))

	assert geometry.getRoadSegmentAroundCrossing(n2, wds) == [n1, n2, n3]
	assert geometry.getRoadSegmentAroundCrossing(n3, wds) is None


def test_getRoadSegmentAroundCrossing_2followingPaths_footway():
	n1 = E.node(lat="-0.001", lon="0", id="1")
	n2 = E.node(lat="0", lon="0", id="2")
	n3 = E.node(lat="0.001", lon="0", id="3")

	way1 = E.way(E.nd(ref="1"), E.nd(ref="2"), E.tag(k="highway", v="residential"), id="1")
	way2 = E.way(E.nd(ref="2"), E.nd(ref="3"), E.tag(k="highway", v="footway"), id="2")

	osm = E.osm(
		n1, n2, n3,
		way1, way2
	)

	wds = model.WalkDataSet(ods=model.OsmDataSet(osm))

	assert geometry.getRoadSegmentAroundCrossing(n1, wds) == [None, n1, n2]
	assert geometry.getRoadSegmentAroundCrossing(n2, wds) is None
	assert geometry.getRoadSegmentAroundCrossing(n3, wds) is None


def test_getRoadSegmentAroundCrossing_circularPath():
	n1 = E.node(lat="-0.001", lon="0", id="1")
	n2 = E.node(lat="0", lon="0", id="2")
	n3 = E.node(lat="0.001", lon="-0.001", id="4")
	n4 = E.node(lat="0.001", lon="0.001", id="5")

	way1 = E.way(E.nd(ref="1"), E.nd(ref="2"), E.tag(k="highway", v="residential"), id="1")
	way2 = E.way(E.nd(ref="2"), E.nd(ref="3"), E.nd(ref="4"), E.nd(ref="2"), E.tag(k="highway", v="residential"), id="2")

	osm = E.osm(
		n1, n2, n3, n4,
		way1, way2
	)

	wds = model.WalkDataSet(ods=model.OsmDataSet(osm))

	assert geometry.getRoadSegmentAroundCrossing(n2, wds) is None


@pytest.mark.parametrize(("x1", "y1", "x2", "y2", "res"), (
	(0, 0, 0, 1, 0),
	(0, 0, 1, 0, 90),
	(1, 1, 1, 0, 180),
	(-1, -1, -2, -1, 269.9912735753293),
	(-1, 1, 0, 2, 44.978182941465036),
	(-1.7810351, 48.2223103, -1.7808996, 48.222137, 152.4839039439924),
))
def test_getAngle(x1, y1, x2, y2, res):
	n1 = E.node(lon=str(x1), lat=str(y1))
	n2 = E.node(lon=str(x2), lat=str(y2))
	assert geometry.getAngle(n1, n2) == res


@pytest.mark.parametrize(("x1", "y1", "angle", "dist", "resX", "resY"), (
	(   0,    0,   0,  5,  "0.0000000",  "0.0000449"),
	(   0,    0,  90,  5,  "0.0000449",  "0.0000000"),
	(-1.7, 48.6, 180, 10, "-1.7000000", "48.5999102"),
	(-1.7, 48.6, 270, 10, "-1.7001358", "48.6000000"),
))
def test_projectPointAtAngle(x1, y1, angle, dist, resX, resY):
	n1 = E.node(lon=str(x1), lat=str(y1))
	res = geometry.projectPointAtAngle(n1, angle, dist)
	print(res)
	assert resX == res[0]
	assert resY == res[1]


def test_hasOtherFeaturesNear_points():
	ref = Point(0,0)
	others = STRtree([
		Point(10,10),
		Point(25,25),
		Point(52,52),
	])

	assert geometry.hasOtherFeaturesNear(ref, others, 50)
	assert not geometry.hasOtherFeaturesNear(ref, others, 0)
	assert not geometry.hasOtherFeaturesNear(Point(-100, -100), others, 50)


def test_hasOtherFeaturesNear_polys():
	ref = Polygon([[0,0], [1, 0], [0, 1], [0, 0]])

	assert geometry.hasOtherFeaturesNear(
		ref,
		STRtree([Point(0.3,0.3)]),
		0
	)
	assert not geometry.hasOtherFeaturesNear(
		ref,
		STRtree([Point(0,1.0001)]),
		0
	)
	assert geometry.hasOtherFeaturesNear(
		ref,
		STRtree([Point(0,1.0001)]),
		1
	)


# ~ def test_hasOtherFeaturesNear_perf():
	# ~ """Performance testing"""

	# ~ def randomCoords():
		# ~ return [randint(-100, 100), randint(-100, 100)]

	# ~ iters = 100000

	# ~ t0 = datetime.datetime.now()
	# ~ otherGeoms = []
	# ~ for i in range(0, iters):
		# ~ otherGeoms.append(Point(randomCoords()))
	# ~ others = STRtree(otherGeoms)

	# ~ t1 = datetime.datetime.now()

	# ~ for i in range(0, iters):
		# ~ first = randomCoords()
		# ~ ref = Polygon([first, randomCoords(), randomCoords(), first])
		# ~ geometry.hasOtherFeaturesNear(ref, others, 10)

	# ~ t2 = datetime.datetime.now()

	# ~ print("Tree init", t1 - t0)
	# ~ print("Near test", t2 - t1)
	# ~ print("  - Avg test", (t2-t1)/iters)

	# ~ assert False


def test_getNearestFeature_neighboors():
	ref = Point(-1.948651, 48.329375)
	others = STRtree([
		LineString([[-1.948699, 48.329328], [-1.948630, 48.329422]]),
		LineString([[-1.948699, 48.329328], [-1.948594, 48.329296]]),
		LineString([[-1.949027, 48.329377], [-1.949079, 48.329310]])
	])

	assert geometry.getNearestFeature(ref, others, 0.1) is None
	assert geometry.getNearestFeature(ref, others, 2.5) == 0
	assert geometry.getNearestFeature(ref, others, 5) == 0


def test_getNearestFeature_many_neighboors():
	ref = Point(-1.948651, 48.329375)
	others = STRtree([
		LineString([[-1.948699, 48.329328], [-1.948630, 48.329422]]),
		LineString([[-1.948699, 48.329328], [-1.948630, 48.329422]]),
		LineString([[-1.949027, 48.329377], [-1.949079, 48.329310]])
	])

	assert geometry.getNearestFeature(ref, others, 0.1) is None
	assert geometry.getNearestFeature(ref, others, 2.5) == 0
	assert geometry.getNearestFeature(ref, others, 5) == 0


def test_getNearestFeature_no_neighboors():
	ref = Point(-1.948518, 48.329864)
	others = STRtree([
		LineString([[-1.948699, 48.329328], [-1.948630, 48.329422]]),
		LineString([[-1.948699, 48.329328], [-1.948594, 48.329296]]),
		LineString([[-1.949027, 48.329377], [-1.949079, 48.329310]])
	])

	assert geometry.getNearestFeature(ref, others, 0.1) is None
	assert geometry.getNearestFeature(ref, others, 2.5) is None
	assert geometry.getNearestFeature(ref, others, 5) is None


def test_xmlToShapely_node():
	osmFeature = E.node(lat="48.12", lon="-1.7", id="1")
	ods = model.OsmDataSet(E.osm(osmFeature))
	res = geometry.xmlToShapely(ods, osmFeature)
	assert isinstance(res, Point)
	assert res.x == -189243.13434856507
	assert res.y == 6126841.828238953


def test_xmlToShapely_node_wgs84():
	osmFeature = E.node(lat="48.12", lon="-1.7", id="1")
	ods = model.OsmDataSet(E.osm(osmFeature))
	res = geometry.xmlToShapely(ods, osmFeature, True)
	assert isinstance(res, Point)
	assert res.x == -1.7
	assert res.y == 48.12


def test_xmlToShapely_line():
	n1 = E.node(id="1", lat="48.12", lon="-1.7")
	n2 = E.node(id="2", lat="48.13", lon="-1.71")
	w1 = E.way(E.nd(ref="1"), E.nd(ref="2"), id="1")
	ods = model.OsmDataSet(E.osm(n1, n2, w1))
	res = geometry.xmlToShapely(ods, w1)
	assert isinstance(res, LineString)
	assert res.equals(LineString([
		[-189243.13434856507, 6126841.828238953],
		[-190356.3292564978, 6128509.51667404]
	]))


def test_xmlToShapely_line_wgs84():
	n1 = E.node(id="1", lat="48.12", lon="-1.7")
	n2 = E.node(id="2", lat="48.13", lon="-1.71")
	w1 = E.way(E.nd(ref="1"), E.nd(ref="2"), id="1")
	osm = E.osm(n1, n2, w1)
	res = geometry.xmlToShapely(model.OsmDataSet(osm), w1, True)
	assert isinstance(res, LineString)
	assert res.equals(LineString([
		[-1.7, 48.12],
		[-1.71, 48.13]
	]))


def test_xmlToShapely_area():
	n1 = E.node(id="1", lat="48.12", lon="-1.7")
	n2 = E.node(id="2", lat="48.13", lon="-1.71")
	n3 = E.node(id="3", lat="48.125", lon="-1.705")
	w1 = E.way(E.nd(ref="1"), E.nd(ref="2"), E.nd(ref="3"), E.nd(ref="1"), id="1")
	osm = E.osm(n1, n2, n3, w1)
	res = geometry.xmlToShapely(model.OsmDataSet(osm), w1)
	assert isinstance(res, Polygon)
	assert res.equals(Polygon([
		[-189243.13434856507, 6126841.828238953],
		[-190356.3292564978, 6128509.51667404],
		[-189799.73180253146, 6127675.631871016],
		[-189243.13434856507, 6126841.828238953],
	]))


def test_xmlToShapely_area_wgs84():
	n1 = E.node(id="1", lat="48.12", lon="-1.7")
	n2 = E.node(id="2", lat="48.13", lon="-1.71")
	n3 = E.node(id="3", lat="48.125", lon="-1.705")
	w1 = E.way(E.nd(ref="1"), E.nd(ref="2"), E.nd(ref="3"), E.nd(ref="1"), id="1")
	osm = E.osm(n1, n2, n3, w1)
	res = geometry.xmlToShapely(model.OsmDataSet(osm), w1, True)
	assert isinstance(res, Polygon)
	assert res.equals(Polygon([
		[-1.7, 48.12],
		[-1.71, 48.13],
		[-1.705, 48.125],
		[-1.7, 48.12],
	]))


def test_getGeometryWidthLength():
	p = Polygon([
		[-216928.30, 6161819.29],
		[-216920.28, 6161835.69],
		[-216908.15, 6161829.67],
		[-216916.05, 6161813.26],
		[-216928.30, 6161819.29]
	])
	res = geometry.getGeometryWidthLength(p)
	assert format(res[0], ".2f") == "9.05"
	assert format(res[1], ".2f") == "12.15"


def test_joinNodeToWay_classic():
	wn1 = E.node(id="-1", lat="0", lon="0")
	wn2 = E.node(id="-2", lat="0", lon="0.1")
	way = E.way(E.nd(ref="-1"), E.nd(ref="-2"), id="-1")

	node = E.node(id="-3", lat="0.5", lon="0.05")
	wds = model.WalkDataSet(
		E.osm(wn1, wn2, node, way),
		ids={ "node": -3, "way": -1, "relation": 0 }
	)
	geometry.joinNodeToWay(wds, node, way)
	wds.sort()

	# Check results in original WDM object
	print(etree.tostring(wds.xml))
	assert len(wds.xml) == 6
	assert wds.xml[0].attrib['id'] == "-4"
	assert wds.xml[0].tag == "node"
	assert wds.xml[1] == node
	assert wds.xml[2] == wn2
	assert wds.xml[3] == wn1
	assert wds.xml[5] == way

	wn3 = wds.xml[0]
	assert wn3.attrib["lon"] == "0.0500000"
	assert wn3.attrib["lat"] == "0.0000000"

	way2 = wds.xml[4]
	assert way2.attrib["id"] == "-2"
	assert way2[0].attrib["ref"] == "-3"
	assert way2[1].attrib["ref"] == "-4"

	assert way[0].attrib["ref"] == "-1"
	assert way[1].attrib["ref"] == "-4"
	assert way[2].attrib["ref"] == "-2"


def test_joinNodeToWay_polygon():
	wn1 = E.node(id="-1", lat="0", lon="0")
	wn2 = E.node(id="-2", lat="0", lon="0.1")
	wn3 = E.node(id="-3", lat="0.1", lon="0.1")
	way = E.way(E.nd(ref="-1"), E.nd(ref="-2"), E.nd(ref="-3"), E.nd(ref="-1"), id="-1")

	node = E.node(id="-4", lat="-0.1", lon="0.05")
	wds = model.WalkDataSet(
		E.osm(wn1, wn2, wn3, node, way),
		ids={ "node": -4, "way": -1, "relation": 0 }
	)
	geometry.joinNodeToWay(wds, node, way)
	wds.sort()

	# Check results in original WDM object
	# print(etree.tostring(wds.xml))
	assert len(wds.xml) == 7
	assert wds.xml[4] == wn1
	assert wds.xml[3] == wn2
	assert wds.xml[2] == wn3
	assert wds.xml[1] == node
	assert wds.xml[6] == way

	wn3 = wds.xml[0]
	assert wn3.attrib["id"] == "-5"
	assert wn3.attrib["lon"] == "0.0500000"
	assert wn3.attrib["lat"] == "0.0000000"

	way2 = wds.xml[5]
	assert way2.attrib["id"] == "-2"
	assert way2[0].attrib["ref"] == "-4"
	assert way2[1].attrib["ref"] == "-5"

	assert way[0].attrib["ref"] == "-1"
	assert way[1].attrib["ref"] == "-5"
	assert way[2].attrib["ref"] == "-2"
	assert way[3].attrib["ref"] == "-3"
	assert way[4].attrib["ref"] == "-1"


def test_joinNodeToWay_reuseNode():
	wn1 = E.node(id="-1", lat="0.0000000", lon="0.0000000")
	wn2 = E.node(id="-2", lat="0.0000000", lon="0.0000100")
	wn3 = E.node(id="-3", lat="0.0000000", lon="0.0000200")
	way = E.way(E.nd(ref="-1"), E.nd(ref="-2"), E.nd(ref="-3"), id="-1")

	node = E.node(id="-4", lat="0.0000005", lon="0.0000100")
	wds = model.WalkDataSet(
		E.osm(wn1, wn2, wn3, node, way),
		ids={"node": -4, "way": -1, "relation": 0}
	)
	converter.newWdmIds = { "node": -4, "way": -1, "relation": 0 }
	geometry.joinNodeToWay(wds, node, way)

	# Check results in original WDM object
	print(etree.tostring(wds.xml))
	assert len(wds.xml) == 6
	assert wds.xml[0] == wn1
	assert wds.xml[1] == wn2
	assert wds.xml[2] == wn3
	assert wds.xml[3] == node
	assert wds.xml[4] == way

	way2 = wds.xml[5]
	assert way2.attrib["id"] == "-2"
	assert way2[0].attrib["ref"] == "-4"
	assert way2[1].attrib["ref"] == "-2"

	assert way[0].attrib["ref"] == "-1"
	assert way[1].attrib["ref"] == "-2"
	assert way[2].attrib["ref"] == "-3"
