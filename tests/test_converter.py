#
# Tests for converter.py
#

import os
import pytest
from lxml import etree
from lxml.builder import ElementMaker, E
from lxml.doctestcompare import LXMLOutputChecker # type: ignore
from doctest import Example
from osm2wdm import model, converter
from . import test_tags

DATA_DIR = os.path.join(os.path.dirname(__file__), "./data")


def assertEqualsXml(validXmlName, toCheckXmlPath):
	"""Compares two given XML to check if they are identical

	Parameters
	----------
	validXmlName : str
		Name of the valid/reference XML stored in /tests/data folder
	toCheckXmlPath : str
		Full path to XML file to test
	"""
	validXmlPath = os.path.join(DATA_DIR, validXmlName)

	with open(validXmlPath, 'r') as validXmlFile:
		with open(toCheckXmlPath, 'r') as toCheckXmlFile:
			validXmlStr = validXmlFile.read()
			toCheckXmlStr = toCheckXmlFile.read()
			checker = LXMLOutputChecker()
			if not checker.check_output(validXmlStr, toCheckXmlStr, 0):
				message = checker.output_difference(Example("", validXmlStr), toCheckXmlStr, 0)
				raise AssertionError(message)


def test_readXMLFile():
	inputPath = os.path.join(DATA_DIR, "wdm_poi_node.xml")
	res = converter.readXMLFile(inputPath)
	assert res.tag == "osm"


def test_writeXMLFile(tmpdir):
	output = tmpdir.join("wdm.xml")
	xml = E.osm(E.node())
	validXmlStr = "<osm><node /></osm>"

	converter.writeXMLFile(xml, output)

	with open(output, 'r') as res:
		toCheckXmlStr = res.read()
		checker = LXMLOutputChecker()
		if not checker.check_output(validXmlStr, toCheckXmlStr, 0):
			message = checker.output_difference(Example("", validXmlStr), toCheckXmlStr, 0)
			raise AssertionError(message)


def test_getOsmiumFilter():
	assert converter.getOsmiumFilter() == "nwr/aerialway nwr/aeroway=aerodrome nwr/amenity=bank,bar,bench,bicycle_parking,boat_rental,bureau_de_change,bus_station,cafe,car_rental,car_wash,casino,chamber_of_commerce,cinema,clinic,college,community_centre,courthouse,crematorium,dentist,doctors,driving_school,fast_food,fire_station,food_court,fuel,hospital,ice_cream,internet_cafe,kindergaten,library,luggage_locker,meeting_point,nightclub,parking,parking_space,pharmacy,place_of_worship,police,post_box,post_office,pub,reception_desk,restaurant,school,shelter,shower,theatre,ticket_validator,toilets,townhall,toy_library,trolley_bay,university,vehicle_inspection,vending_machine,waste_basket nwr/barrier=bar,block,bollard,cycle_barrier,full-height_turnstile,height_restrictor,horse_stile,kissing_gate,motorcycle_barrier,planter,stile,turnstile nwr/door nwr/emergency=defibrillator,phone nwr/entrance nwr/government=audit,customs,local_authority,parliament,prefecture,social_security,social_welfare,tax nwr/healthcare=clinic,doctor,hospital,laboratory,sample_collection nwr/highway nwr/indoor=door,yes nwr/information nwr/leisure=adult_gaming_centre,amusement_arcade,bowling_alley,dance,escape_game,fitness_centre,golf_course,hackerspace,horse_riding,ice_rink,resort,sports_centre,sports_hall,stadium nwr/man_made=manhole,planter,street_cabinet,utility_pole nwr/natural=tree nwr/office=diplomatic,employment_agency,estate_agent,government,guide,insurance,moving_company nwr/power=pole nwr/public_transport=platform,station nwr/railway=halt,platform,station,subway_entrance nwr/religion=buddhist,christian,hindu,jewish,muslim,shinto,sikh,taoist nwr/shop nwr/social_facility=assisted_living,group_home,nursing_home nwr/sport=climbing,swimming nwr/tourism=alpine_hut,apartment,aquarium,arts_centre,camp_site,chalet,gallery,hostel,hotel,information,motel,museum,theme_park,wilderness_hut,zoo"


@pytest.mark.parametrize(("name", "data"), (
	("poi_node", None),
	("poi_way", None),
	("poi_multitags", None),
	("entrance", None),
	("quay", None),
	("station", None),
	("parking", None),
	("obstacle", None),
	("path", None),
	("path_area", None),
	("crossing", None),
	("cycleway", None),
	("nearby", None),
	("sidewalks1", ["path"]),
	("sidewalks2", ["road-with-sidewalk"]),
	("link_nodes", None),
	("stairs", None),
	("station_relation", None),
	("elevators", None),
	("extract", None),
))
def test_transformOsmIntoWDM(tmpdir, name, data):
	if data is None:
		data = ["transport","poi","path", "road-with-sidewalk"]

	resWdmFile = tmpdir.join("wdm.xml")
	osmFile = f"osm_{name}.osm"
	expectedWdmFile = f"wdm_{name}.xml"
	osm = converter.readXMLFile(os.path.join(DATA_DIR, osmFile))
	resWdm = converter.transformOsmIntoWDM(osm, data)
	converter.writeXMLFile(resWdm, resWdmFile)
	assertEqualsXml(expectedWdmFile, resWdmFile)


def test_appendPointOfInterest():
	osmNode = test_tags.setOsmTags(
		E.node(lat="47.2", lon="-1.7", id="123", version="1"),
		{ "shop": "doityourself", "name": "Brico Dépôt", "ref:FR:SIRET": "82326253000038"}
	)
	osm = E.osm(osmNode)
	wds = model.WalkDataSet(ods=model.OsmDataSet(osm))

	converter.appendPointOfInterest(osmNode, wds, {"PointOfInterest": "Shop", "Shop": "Doityourself"})

	assert len(wds.xml) == 1

	wdmPoi = wds.xml[0]
	assert wdmPoi.attrib["lat"] == "47.2"
	assert wdmPoi.attrib["lon"] == "-1.7"

	test_tags.assertTag(wdmPoi, "PointOfInterest", "Shop")
	test_tags.assertTag(wdmPoi, "Shop", "Doityourself")
	test_tags.assertTag(wdmPoi, "Name", "Brico Dépôt")
	test_tags.assertTag(wdmPoi, "Ref:Import:Source", "OpenStreetMap")
	test_tags.assertTag(wdmPoi, "Ref:Import:Id", "n123")
	test_tags.assertTag(wdmPoi, "Ref:Import:Version", "1")
	test_tags.assertTag(wdmPoi, "Ref:FR:SIRET", "82326253000038")


def test_appendEntrance():
	osmNode = test_tags.setOsmTags(
		E.node(lat="47.2", lon="-1.7", id="123", version="1"),
		{
			"door": "hinged",
			"entrance": "emergency",
			"ref": "42",
			"width": "1.5 m",
			"height": "240 cm",
			"kerb:height": "5 cm",
			"automatic_door": "yes",
			"door:handle": "crash_bar",
			"door:handle:inside": "knob",
			"material": "glass",
			"entrance:coloured_band": "yes"
		}
	)
	osm = E.osm(osmNode)
	wds = model.WalkDataSet(ods=model.OsmDataSet(osm))

	res = converter.appendEntrance(osmNode, wds, {"Entrance": "Building"})

	assert len(wds.xml) == 1

	wdmFeature = wds.xml[0]
	assert wdmFeature == res
	assert wdmFeature.attrib["lat"] == "47.2"
	assert wdmFeature.attrib["lon"] == "-1.7"

	test_tags.assertTag(wdmFeature, "EntrancePassage", "Door")
	test_tags.assertTag(wdmFeature, "IsEntry", "No")
	test_tags.assertTag(wdmFeature, "PublicCode", "42")
	test_tags.assertTag(wdmFeature, "Width", "1.50")
	test_tags.assertTag(wdmFeature, "Height", "2.40")
	test_tags.assertTag(wdmFeature, "KerbHeight", "0.05")
	test_tags.assertTag(wdmFeature, "AutomaticDoor", "Yes")
	test_tags.assertTag(wdmFeature, "Door", "Hinged")
	test_tags.assertTag(wdmFeature, "DoorHandle:Outside", "CrashBar")
	test_tags.assertTag(wdmFeature, "DoorHandle:Inside", "Knob")
	test_tags.assertTag(wdmFeature, "GlassDoor", "Yes")
	test_tags.assertTag(wdmFeature, "Entrance", "Building")


def test_appendQuay():
	osmWay = test_tags.setOsmTags(
		E.way(E.nd(ref="1"), E.nd(ref="2"), id="1"),
		{ "local_ref": "42" }
	)
	osm = E.osm(
		E.node(lat="0", lon="0", id="1", version="1"),
		E.node(lat="0", lon="0.001", id="2", version="1"),
		osmWay
	)
	wds = model.WalkDataSet(ods=model.OsmDataSet(osm))

	res = converter.appendQuay(osmWay, wds, {"Quay": "Bus"}, True)

	assert len(wds.xml) == 1

	wdmFeature = wds.xml[0]
	assert wdmFeature == res
	assert wdmFeature.attrib["lat"] == "0"
	assert wdmFeature.attrib["lon"] == "0"

	test_tags.assertTag(wdmFeature, "Quay", "Bus")
	test_tags.assertTag(wdmFeature, "PublicCode", "42")


def test_appendStation():
	osmWay = test_tags.setOsmTags(
		E.way(E.nd(ref="1"), E.nd(ref="2"), id="1"),
		{ "ref:FR:uic8": "1234", "ref:FR:SIRET": "82326253000038" }
	)
	osm = E.osm(
		E.node(lat="0", lon="0", id="1", version="1"),
		E.node(lat="0", lon="0.001", id="2", version="1"),
		osmWay
	)
	wds = model.WalkDataSet(ods=model.OsmDataSet(osm))

	res = converter.appendStation(osmWay, wds, {"PointOfInterest": "StopPlace", "StopPlace": "RailStation"})

	assert len(wds.xml) == 3

	wdmFeature = wds.xml.find("way")
	assert wdmFeature == res
	test_tags.assertTag(wdmFeature, "PointOfInterest", "StopPlace")
	test_tags.assertTag(wdmFeature, "StopPlace", "RailStation")
	test_tags.assertTag(wdmFeature, "Ref:Export:Id", "FR:StopPlace:StopArea_OCE1234_rail:")
	test_tags.assertTag(wdmFeature, "Ref:FR:SIRET", "82326253000038")


def test_appendPath():
	osmWay = test_tags.setOsmTags(
		E.way(E.nd(ref="1"), E.nd(ref="2"), id="1"),
		{ "wheelchair": "yes", "tactile_paving": "no" }
	)
	osm = E.osm(
		E.node(lat="0", lon="0", id="1", version="1"),
		E.node(lat="0", lon="0.001", id="2", version="1"),
		osmWay
	)
	wds = model.WalkDataSet(ods=model.OsmDataSet(osm))

	res = converter.appendPath(osmWay, wds, "Pavement")

	assert len(wds.xml) == 3

	wdmFeature = wds.xml.find("way")
	assert wdmFeature == res
	test_tags.assertTag(wdmFeature, "SitePathLink", "Pavement")
	test_tags.assertTag(wdmFeature, "WheelchairAccess", "Yes")


def test_appendOpenSpace():
	# Area
	osmN1 = E.node(id="1", lon="0", lat="0")
	osmN2 = E.node(
		E.tag(k="amenity", v="toilets"),
		id="2", lon="1", lat="0"
	)
	osmN3 = E.node(id="3", lon="0.5", lat="1")
	osmW1 = E.way(
		E.nd(ref="1"),
		E.nd(ref="2"),
		E.nd(ref="3"),
		E.nd(ref="1"),
		E.tag(k="highway", v="pedestrian"),
		E.tag(k="area", v="yes"),
		id="1"
	)

	# Coming way to node 1
	osmN4 = E.node(id="4", lon="-1", lat="0")
	osmW2 = E.way(
		E.nd(ref="4"),
		E.nd(ref="1"),
		E.tag(k="highway", v="footway"),
		id="2"
	)

	osm = E.osm(
		osmN1, osmN2, osmN3, osmN4,
		osmW1, osmW2
	)

	# Result WDS
	wds = model.WalkDataSet(
		E.osm(),
		ods=model.OsmDataSet(osm),
	)

	converter.appendOpenSpace(osmW1, wds)
	wds.sort()

	# Check output
	print(etree.tostring(wds.xml))
	assert len(wds.xml) == 5

	resN1 = wds.xml[0]
	assert resN1.tag == "node"
	assert resN1.attrib["lon"] == "0"
	assert resN1.attrib["lat"] == "0"

	resN2 = wds.xml[1]
	assert resN2.tag == "node"
	assert resN2.attrib["lon"] == "1"
	assert resN2.attrib["lat"] == "0"

	resN3 = wds.xml[2]
	assert resN3.tag == "node"
	assert resN3.attrib["lon"] == "0.5000000"
	assert resN3.attrib["lat"] == "0.3333333"
	test_tags.assertTag(resN3, "PathJunction", "OpenSpace")
	test_tags.assertTag(resN3, "Ref:Import:Geometry:Id", "w1")

	resW1 = wds.xml[3]
	assert resW1.tag == "way"
	resW1nodes = resW1.findall("nd")
	assert resW1nodes[0].attrib["ref"] == resN1.attrib["id"]
	assert resW1nodes[1].attrib["ref"] == resN3.attrib["id"]
	test_tags.assertTag(resW1, "SitePathLink", "OpenSpace")

	resW2 = wds.xml[4]
	assert resW2.tag == "way"
	resW2nodes = resW2.findall("nd")
	assert resW2nodes[0].attrib["ref"] == resN2.attrib["id"]
	assert resW2nodes[1].attrib["ref"] == resN3.attrib["id"]
	test_tags.assertTag(resW2, "SitePathLink", "OpenSpace")


def test_appendLinearCrossing_3nodes():
	osmN1 = E.node(id="1", version="1", lon="1", lat="1")
	osmN2 = E.node(
		E.tag(k="highway", v="crossing"),
		E.tag(k="tactile_paving", v="yes"),
		id="2", version="1", lon="1", lat="1.0001"
	)
	osmN3 = E.node(id="3", version="1", lon="1", lat="1.0002")
	osmRoadSegment = [ osmN1, osmN2, osmN3 ]

	wdmN2 = E.node(
		E.tag(k="Ref:Import:Source", v="OpenStreetMap"),
		E.tag(k="Ref:Import:Id", v="n2"),
		E.tag(k="Ref:Import:Version", v="1"),
		id="1", lon="1", lat="1.0001"
	)
	wds = model.WalkDataSet(
		E.osm(wdmN2),
		importedOsmFeaturesById={"node": {"2": set([wdmN2])}, "way":{}, "relation":{}},
		osm2wdmNodesIds={"2": "1"}
	)

	res = converter.appendLinearCrossing(osmRoadSegment, wds)

	assert len(wds.xml) == 4
	assert res == wds.xml[3]
	assert wds.xml[0] == wdmN2

	resN1 = wds.xml[1]
	assert resN1.tag == "node"
	assert resN1.attrib["lon"] == "1.0000314"
	assert resN1.attrib["lat"] == "1.0001000"
	test_tags.assertTag(resN1, "PathJunction", "Crossing")
	test_tags.assertTag(resN1, "TactileWarningStrip", "Good")

	test_tags.assertTag(wdmN2, "PathJunction", None)
	test_tags.assertTag(wdmN2, "SitePathLink", None)
	test_tags.assertTag(wdmN2, "Ref:Import:Geometry:Id", "n2")

	resN3 = wds.xml[2]
	assert resN3.tag == "node"
	assert resN3.attrib["lon"] == "0.9999686"
	assert resN3.attrib["lat"] == "1.0001000"
	test_tags.assertTag(resN3, "PathJunction", "Crossing")
	test_tags.assertTag(resN1, "TactileWarningStrip", "Good")

	resW1 = wds.xml[3]
	assert resW1.tag == "way"
	resW1nodes = resW1.findall("nd")
	assert resW1nodes[0].attrib["ref"] == resN1.attrib["id"]
	assert resW1nodes[1].attrib["ref"] == wdmN2.attrib["id"]
	assert resW1nodes[2].attrib["ref"] == resN3.attrib["id"]
	test_tags.assertTag(resW1, "Ref:Import:Id", "n2")
	test_tags.assertTag(resW1, "SitePathLink", "Crossing")
	test_tags.assertTag(resN1, "LinearCue", None)


def test_appendLinearCrossing_2firstnodes():
	osmN2 = E.node(
		E.tag(k="highway", v="crossing"),
		id="2", version="1", lon="1", lat="1.0001"
	)
	osmN3 = E.node(id="3", version="1", lon="1", lat="1.0002")
	osmRoadSegment = [ None, osmN2, osmN3 ]

	wdmN2 = E.node(
		E.tag(k="Ref:Import:Source", v="OpenStreetMap"),
		E.tag(k="Ref:Import:Id", v="n2"),
		E.tag(k="Ref:Import:Version", v="1"),
		id="1", lon="1", lat="1.0001"
	)
	wds = model.WalkDataSet(
		E.osm(wdmN2),
		ods=model.OsmDataSet(E.osm(osmN2, osmN3)),
		importedOsmFeaturesById={"node": {"2": set([wdmN2])}, "way": {}, "relation": {}},
		osm2wdmNodesIds={"2":"1"},
	)

	res = converter.appendLinearCrossing(osmRoadSegment, wds)

	assert len(wds.xml) == 4
	assert res == wds.xml[3]
	assert wds.xml[0] == wdmN2

	resN1 = wds.xml[1]
	assert resN1.tag == "node"
	assert resN1.attrib["lon"] == "1.0000314"
	assert resN1.attrib["lat"] == "1.0001000"
	test_tags.assertTag(resN1, "PathJunction", "Crossing")

	test_tags.assertTag(wdmN2, "PathJunction", None)
	test_tags.assertTag(wdmN2, "SitePathLink", None)
	test_tags.assertTag(wdmN2, "Ref:Import:Geometry:Id", "n2")

	resN3 = wds.xml[2]
	assert resN3.tag == "node"
	assert resN3.attrib["lon"] == "0.9999686"
	assert resN3.attrib["lat"] == "1.0001000"
	test_tags.assertTag(resN3, "PathJunction", "Crossing")

	resW1 = wds.xml[3]
	assert resW1.tag == "way"
	resW1nodes = resW1.findall("nd")
	assert resW1nodes[0].attrib["ref"] == resN1.attrib["id"]
	assert resW1nodes[1].attrib["ref"] == wdmN2.attrib["id"]
	assert resW1nodes[2].attrib["ref"] == resN3.attrib["id"]
	test_tags.assertTag(resW1, "Ref:Import:Id", "n2")
	test_tags.assertTag(resW1, "SitePathLink", "Crossing")


def test_appendLinearCrossing_2lastnodes():
	osmN1 = E.node(id="1", version="1", lon="1", lat="1")
	osmN2 = E.node(
		E.tag(k="highway", v="crossing"),
		id="2", version="1", lon="1", lat="1.0001"
	)
	osmRoadSegment = [ osmN1, osmN2, None ]

	wdmN2 = E.node(
		E.tag(k="Ref:Import:Source", v="OpenStreetMap"),
		E.tag(k="Ref:Import:Id", v="n2"),
		E.tag(k="Ref:Import:Version", v="1"),
		id="1", lon="1", lat="1.0001"
	)
	wds = model.WalkDataSet(
		E.osm(wdmN2),
		importedOsmFeaturesById={"node": {"2": set([wdmN2])}, "way": {}, "relation": {}},
		osm2wdmNodesIds={"2": "1"},
	)

	res = converter.appendLinearCrossing(osmRoadSegment, wds)

	assert len(wds.xml) == 4
	assert res == wds.xml[3]
	assert wds.xml[0] == wdmN2

	resN1 = wds.xml[1]
	assert resN1.tag == "node"
	assert resN1.attrib["lon"] == "1.0000314"
	assert resN1.attrib["lat"] == "1.0001000"
	test_tags.assertTag(resN1, "PathJunction", "Crossing")

	test_tags.assertTag(wdmN2, "PathJunction", None)
	test_tags.assertTag(wdmN2, "SitePathLink", None)
	test_tags.assertTag(wdmN2, "Ref:Import:Geometry:Id", "n2")

	resN3 = wds.xml[2]
	assert resN3.tag == "node"
	assert resN3.attrib["lon"] == "0.9999686"
	assert resN3.attrib["lat"] == "1.0001000"
	test_tags.assertTag(resN3, "PathJunction", "Crossing")

	resW1 = wds.xml[3]
	assert resW1.tag == "way"
	resW1nodes = resW1.findall("nd")
	assert resW1nodes[0].attrib["ref"] == resN1.attrib["id"]
	assert resW1nodes[1].attrib["ref"] == wdmN2.attrib["id"]
	assert resW1nodes[2].attrib["ref"] == resN3.attrib["id"]
	test_tags.assertTag(resW1, "Ref:Import:Id", "n2")
	test_tags.assertTag(resW1, "SitePathLink", "Crossing")


def test_appendParking():
	osmWay = test_tags.setOsmTags(
		E.way(E.nd(ref="1"), E.nd(ref="2"), id="1"),
		{ "capacity:charging": "1" }
	)
	osm = E.osm(
		E.node(lat="0", lon="0", id="1", version="1"),
		E.node(lat="0", lon="0.001", id="2", version="1"),
		osmWay
	)
	wds = model.WalkDataSet(ods=model.OsmDataSet(osm))

	res = converter.appendParking(osmWay, wds)

	assert len(wds.xml) == 1

	wdmFeature = wds.xml.find("node")
	assert wdmFeature == res
	test_tags.assertTag(wdmFeature, "ParkingBay", "Disabled")
	test_tags.assertTag(wdmFeature, "VehicleRecharging", "Yes")


def test_appendObstacle():
	osmWay = test_tags.setOsmTags(
		E.way(E.nd(ref="1"), E.nd(ref="2"), id="1"),
		{ "shower": "yes", "wheelchair": "yes", "tactile_paving": "yes" }
	)
	osm = E.osm(
		E.node(lat="0", lon="0", id="1", version="1"),
		E.node(lat="0", lon="0.001", id="2", version="1"),
		osmWay
	)
	wds = model.WalkDataSet(ods=model.OsmDataSet(osm))

	res = converter.appendObstacle(osmWay, wds, {"Amenity": "Toilets"})

	assert len(wds.xml) == 1

	wdmFeature = wds.xml.find("node")
	assert wdmFeature == res
	test_tags.assertTag(wdmFeature, "Amenity", "Toilets")
	test_tags.assertTag(wdmFeature, "Toilets:Shower", "Yes")
	test_tags.assertTag(wdmFeature, "WheelchairAccess", "Yes")
	test_tags.assertTag(wdmFeature, "TactileWarningStrip", "Good")


@pytest.mark.parametrize(("w1tags", "result"), (
	({"highway": "footway"}, True),
	({"highway": "path", "foot": "yes"}, True),
	({"highway": "cycleway", "foot": "designated"}, True),
	({"highway": "cycleway"}, False),
	({"highway": "unclassified"}, True),
))
def test_isPedestrianCrossingAllowed_1way(w1tags, result):
	n1 = test_tags.setOsmTags(E.node(lat="0", lon="0", id="1", version="1"), {"highway": "crossing"})
	n2 = E.node(lat="1", lon="0", id="2", version="1")
	n3 = E.node(lat="-1", lon="0", id="3", version="1")
	w1 = test_tags.setOsmTags(
		E.way(
			E.nd(ref="3"),
			E.nd(ref="1"),
			E.nd(ref="2"),
			id="1"
		),
		w1tags
	)
	osm = E.osm(n1, n2, n3, w1)
	wds = model.WalkDataSet(ods=model.OsmDataSet(osm))
	assert converter.isPedestrianCrossingAllowed(n1, wds) == result


@pytest.mark.parametrize(("w1tags", "w2tags", "result"), (
	({"highway": "footway"}, {"highway": "unclassified"}, True),
	({"highway": "path", "foot": "yes"}, {"highway": "unclassified"}, True),
	({"highway": "cycleway", "foot": "designated"}, {"highway": "unclassified"}, True),
	({"highway": "cycleway"}, {"highway": "unclassified"}, False),
	({"highway": "unclassified"}, {"highway": "unclassified"}, True),
))
def test_isPedestrianCrossingAllowed_2ways(w1tags, w2tags, result):
	n1 = test_tags.setOsmTags(E.node(lat="0", lon="0", id="1", version="1"), {"highway": "crossing"})
	n2 = E.node(lat="1", lon="0", id="2", version="1")
	n3 = E.node(lat="-1", lon="0", id="3", version="1")
	n4 = E.node(lat="0", lon="1", id="4", version="1")
	n5 = E.node(lat="0", lon="-1", id="5", version="1")
	w1 = test_tags.setOsmTags(
		E.way(
			E.nd(ref="3"),
			E.nd(ref="1"),
			E.nd(ref="2"),
			id="1"
		),
		w1tags
	)
	w2 = test_tags.setOsmTags(
		E.way(
			E.nd(ref="5"),
			E.nd(ref="1"),
			E.nd(ref="4"),
			id="2"
		),
		w2tags
	)
	osm = E.osm(n1, n2, n3, n4, n5, w1, w2)
	wds = model.WalkDataSet(ods=model.OsmDataSet(osm))
	assert converter.isPedestrianCrossingAllowed(n1, wds) == result
