import pytest
from lxml import etree
from lxml.builder import ElementMaker, E
from osm2wdm import tags


def setOsmTags(elem: etree._Element, tags: dict):
	for k in tags:
		v = tags[k]
		elem.append(E.tag(k=k, v=v))

	return elem


def test_findallRule_with_value():
    assert tags.findallRule("railway", "subway_entrance") == ".//tag[@k='railway'][@v='subway_entrance'].."


def test_findallRule_without_value():
	assert tags.findallRule("railway") == ".//tag[@k='railway'].."


def test_findallRule_with_ftag():
    assert tags.findallRule("railway", "subway_entrance", "node") == ".//node/tag[@k='railway'][@v='subway_entrance'].."


@pytest.mark.parametrize(("name", "wikidata", "osmTags", "expected"), (
	("URSSAF", "Q3550086", { "operator:wikidata": "Q3550086" }, True),
	("URSSAF", "Q3550086", { "brand:wikidata": "Q3550086" }, True),
	("URSSAF", "Q3550086", { "network:wikidata": "Q3550086" }, True),
	("URSSAF", "Q3550086", { "wikidata": "Q3550086" }, True),
	("URSSAF", "Q3550086", { "operator:wikidata": "Q1" }, False),
	("URSSAF", "Q3550086", { "name": "URSSAF" }, True),
	("URSSAF", "Q3550086", { "brand": "URSSAF" }, True),
	("URSSAF", "Q3550086", { "operator": "URSSAF" }, True),
	("URSSAF", "Q3550086", { "network": "URSSAF" }, True),
	("URSSAF", "Q3550086", { "operator:wikidata": "Q3550086", "name": "URSSAF de Rennes" }, True),
	("URSSAF", "Q3550086", { "operator:wikidata": "Q1", "name": "URSSAF" }, False),
	("URSSAF", "Q3550086", {}, False),
))
def test_isOsmOperatedBy(name, wikidata, osmTags, expected):
	assert tags.isOsmOperatedBy(osmTags, name, wikidata) == expected


def test_hasAll_success():
    elemTags = {
		"railway": "subway_entrance",
        "name": "Entrance A"
    }
    searchTags = {"railway": "subway_entrance", "name": "Entrance A"}
    assert tags.hasAll(elemTags, searchTags) == True


def test_hasAll_partial_match():
    elemTags = {
		"railway": "subway_entrance",
        "name": "Entrance B"
    }
    searchTags = {"railway": "subway_entrance", "name": "Entrance A"}
    assert tags.hasAll(elemTags, searchTags) == False


def test_hasAll_no_match():
    elemTags = {
		"railway": "train_station",
        "name": "Entrance A"
    }
    searchTags = {"railway": "subway_entrance", "name": "Entrance A"}
    assert tags.hasAll(elemTags, searchTags) == False


def test_hasAll_with_search_wildcard():
    elemTags = {
		"railway": "subway_entrance",
        "name": "Entrance A"
    }
    searchTags = {"railway": "*"}
    assert tags.hasAll(elemTags, searchTags) == True


def test_hasAll_empty_tags():
    searchTags = {"railway": "subway_entrance"}
    assert tags.hasAll({}, searchTags) == False


def test_hasAll_empty_searchtags():
    elemTags = {
		"railway": "subway_entrance"
    }
    searchTags = {}
    assert tags.hasAll(elemTags, searchTags) == True


def test_hasAll_with_excluded_tags():
    elemTags = {
		"railway": "subway_entrance",
        "name": "Entrance A"
    }
    searchTags = {"railway": "subway_entrance", "name": "Entrance A"}
    excludedTags = {"operator": "Metro"}
    assert tags.hasAll(elemTags, searchTags, excludedTags) == True


def test_hasAll_exclusion_match():
    elemTags = {
		"railway": "subway_entrance",
        "operator": "Metro"
    }
    searchTags = {"railway": "subway_entrance"}
    excludedTags = {"operator": "Metro"}
    assert tags.hasAll(elemTags, searchTags, excludedTags) == False


def test_hasAll_with_excluded_wildcard():
    elemTags = {
		"railway": "subway_entrance",
        "name": "Entrance A"
    }
    searchTags = {"railway": "subway_entrance"}
    excludedTags = {"name": "*"}
    assert tags.hasAll(elemTags, searchTags, excludedTags) == False


def test_hasAll_with_search_list():
    elemTags = {
		"railway": "subway_entrance",
        "name": "Entrance B"
    }
    searchTags = {"railway": ["subway_entrance", "train_station"], "name": ["Entrance A", "Entrance B"]}
    assert tags.hasAll(elemTags, searchTags) == True


def test_hasAll_with_exclusion_list():
    elemTags = {
		"railway": "subway_entrance",
        "name": "Entrance C"
    }
    searchTags = {"railway": "subway_entrance"}
    excludedTags = {"name": ["Entrance A", "Entrance B"]}
    assert tags.hasAll(elemTags, searchTags, excludedTags) == True


def test_getAll_multiple_tags():
    elem = E.root(
        E.tag(k="key1", v="value1"),
        E.tag(k="key2", v="value2"),
        E.tag(k="key3", v="value3"),
    )
    expected = {
        "key1": "value1",
        "key2": "value2",
        "key3": "value3"
    }
    assert tags.getAll(elem) == expected


def test_getAll_no_tags():
    elem = E.root(
        E.otherTag(k="key1", v="value1"),
        E.anotherTag(k="key2", v="value2"),
    )
    expected = {}
    assert tags.getAll(elem) == expected


def test_getAll_missing_attributes():
    # Test avec balises 'tag' sans attributs 'k' ou 'v'
    elem = E.root(
        E.tag(k="key1"),
        E.tag(v="value2"),
        E.tag()
    )
    assert tags.getAll(elem) == {}


def test_getAll_tags_with_empty_values():
    # Test avec balises 'tag' ayant des attributs 'k' ou 'v' vides
    elem = E.root(
        E.tag(k="key1", v=""),
        E.tag(k="", v="value2"),
    )
    expected = {}
    assert tags.getAll(elem) == expected


def test_getAll_tags_with_special_characters():
    elem = E.root(
        E.tag(k="key&1", v="value@1"),
        E.tag(k="key>2", v="value<2"),
    )
    expected = {
        "key&1": "value@1",
        "key>2": "value<2"
    }
    assert tags.getAll(elem) == expected


def test_getValue_exists():
	elem = E.node(E.tag(k="name", v="bla"))
	res = tags.getValue(elem, "name")
	assert res == "bla"


def test_getValue_unset():
	elem = E.node()
	res = tags.getValue(elem, "name")
	assert res is None


def test_getValue_emptyElem():
	res = tags.getValue(None, "name")
	assert res is None


@pytest.mark.parametrize(("osmTags", "lookForTags", "expectedResult"), (
	({"width": "1"}, ["width"], "1.00"),
	({"width": "1.5"}, ["width"], "1.50"),
	({"width": "  1.5	"}, ["width"], "1.50"),
	({"width": "1.5 m"}, ["width"], "1.50"),
	({"width": "101.2 cm"}, ["width"], "1.01"),
	({"width": " 1.5	m "}, ["width"], "1.50"),
	({"width": "	101.2    cm "}, ["width"], "1.01"),
	({}, ["width"], None),
	({"width": "whatisit"}, ["width"], None),
	({"width": "1.5"}, ["est_width"], None),
	({"width": "1.5", "est_width": "2"}, ["est_width", "width"], "2.00"),
))
def test_getOsmSizeValue(osmTags, lookForTags, expectedResult):
	res = tags.getOsmSizeValue(osmTags, lookForTags)
	assert res == expectedResult


@pytest.mark.parametrize(("value", "expectedResult"), (
	("1", "1000"),
	("3.5", "3500"),
	("", None),
	("bla", None),
	("2.7 t", "2700"),
	("-2.7", None),
	("2650 kg", "2650"),
	("2.7    t", "2700"),
	(None, None),
))
def test_getOsmWeightValue(value, expectedResult):
	res = tags.getOsmWeightValue(value)
	assert res == expectedResult


@pytest.mark.parametrize(("osm", "wdm"), (
	(None, None),
	("up", None),
	("down", None),
	("5%", "5.00"),
	("-5%", "5.00"),
	("5 %", "5.00"),
	("5.12%", "5.12"),
	("-5.42%", "5.42"),
	("5.25 %", "5.25"),
	("5°", "8.75"),
	("-12.5 °", "22.17"),
))
def test_getOsmSlopeValue(osm, wdm):
	res = tags.getOsmSlopeValue(osm)
	assert res == wdm


def test_appendTag_set():
	elem = E.node()
	tags.appendTag(elem, "Name", "Bla")
	assert elem[0].tag == "tag"
	assert elem[0].get("k") == "Name"
	assert elem[0].get("v") == "Bla"


def test_appendTag_unset():
	elem = E.node()
	tags.appendTag(elem, "Name", None)
	assert len(elem) == 0


def test_appendTag_empty():
	elem = E.node()
	tags.appendTag(elem, "Name", "")
	assert len(elem) == 0


def test_appendTag_duplicate():
	elem = E.node(E.tag(k="Name", v="Balenciaga"))
	tags.appendTag(elem, "Name", "Balenciaga")
	assert len(elem) == 1


def test_appendTag_overwrite():
	elem = E.node(E.tag(k="Name", v="Balenciaga"))
	tags.appendTag(elem, "Name", "Gucci")
	assert len(elem) == 1
	assert elem[0].attrib["v"] == "Gucci"


def test_removeTag_exists():
	elem = E.node(E.tag(k="Name", v="Balenciaga"))
	tags.removeTag(elem, "Name")
	assert len(elem) == 0


def test_removeTag_absent():
	elem = E.node(E.tag(k="Name", v="Balenciaga"))
	tags.removeTag(elem, "SitePathLink")
	assert len(elem) == 1
	assert elem[0].attrib["k"] == "Name"


def test_removeTag_duplicate():
	elem = E.node(E.tag(k="Name", v="Balenciaga"), E.tag(k="Name", v="Balenciaga"))
	tags.removeTag(elem, "Name")
	assert len(elem) == 0


@pytest.mark.parametrize(("mapping", "osmVal", "wdmRes"), (
	({"no": "No", "yes": "Yes", "bad": "Limited"}, "yes", "Yes"),
	({"no": "No", "yes": "Yes", "bad": "Limited"}, "bad", "Limited"),
	({"no": "No", "yes": "Yes"}, None, None),
	({None: "No", "yes": "Yes"}, None, "No"),
	({"no": "No", "yes": "Yes"}, "bla", None),
	({"no": "No", "yes": "Yes", "*": "Whatever"}, "bla", "Whatever"),
	({"no": "No", "yes": "Yes", "*": "Whatever"}, None, None),
))
def test_appendTagValueMapping(mapping, osmVal, wdmRes):
	wdmElem = E.node()
	tags.appendTagValueMapping(wdmElem, "WheelchairAccess", mapping, osmVal)
	assertTag(wdmElem, "WheelchairAccess", wdmRes)


@pytest.mark.parametrize(("osmType", "expectedId"), (
	("node", "n1234"),
	("way", "w1234"),
	("relation", "r1234"),
))
def test_appendRefTags(osmType, expectedId):
	osm = etree.Element(osmType)
	osm.attrib["id"] = "1234"
	osm.attrib["version"] = "1"
	elem = E.node()
	tags.appendRefTags(osm, elem)

	assertTag(elem, "Ref:Import:Source", "OpenStreetMap")
	assertTag(elem, "Ref:Import:Id", expectedId)
	assertTag(elem, "Ref:Import:Version", "1")


def test_appendCommonTags():
	osmTags = {
		"name": "Test",
		"description": "Bla",
		"fixme": "Fix me!",
		"wheelchair:description": "Dis iz bad",
		"deaf:description": "Dis iz not bad",
		"blind:description": "Dis iz good",
		"capacity:seats": "5",
		"backrest": "yes",
	}

	elem = E.node()
	tags.appendCommonTags(osmTags, elem)

	assertTag(elem, "Name", "Test")
	assertTag(elem, "Description", "Bla")
	assertTag(elem, "FixMe", "Fix me!")
	assertTag(elem, "Name", "Test")
	assertTag(elem, "WheelchairAccess:Description", "Dis iz bad")
	assertTag(elem, "VisualSigns:Description", "Dis iz not bad")
	assertTag(elem, "AudibleSignals:Description", "Dis iz good")
	assertTag(elem, "SeatCount", "5")
	assertTag(elem, "BackRest", "Yes")


def test_appendCommonTags_frDesc():
	osmTags = {
		"wheelchair:description": "Dis iz bad",
		"wheelchair:description:fr": "C'est bof",
		"deaf:description:fr": "C'est pas si mal",
		"blind:description": "Dis iz good",
	}

	elem = E.node()
	tags.appendCommonTags(osmTags, elem)

	assertTag(elem, "WheelchairAccess:Description", "Dis iz bad")
	assertTag(elem, "VisualSigns:Description", "C'est pas si mal")
	assertTag(elem, "AudibleSignals:Description", "Dis iz good")


def test_appendCommonTags_image():
	# All tags set
	osmTags = {
		"image": "https://my.img/net.jpg",
		"wikimedia_commons": "File:MyImage.jpg",
		"panoramax": "1234-5678-910-11121314-15161718",
	}
	elem = E.node()
	tags.appendCommonTags(osmTags, elem)
	assertTag(elem, "Image", "https://my.img/net.jpg")

	# Wiki+Panoramax
	osmTags = {
		"wikimedia_commons": "File:MyImage.jpg",
		"panoramax": "1234-5678-910-11121314-15161718",
	}
	elem = E.node()
	tags.appendCommonTags(osmTags, elem)
	assertTag(elem, "Image", "https://commons.wikimedia.org/wiki/File:MyImage.jpg")

	# Panoramax
	osmTags = {
		"panoramax": "1234-5678-910-11121314-15161718",
	}
	elem = E.node()
	tags.appendCommonTags(osmTags, elem)
	assertTag(elem, "Image", "https://api.panoramax.xyz/api/pictures/1234-5678-910-11121314-15161718/sd.jpg")


def test_appendCommonEntranceTags():
	osmTags = {
		"door": "hinged",
		"entrance": "emergency",
		"ref": "42",
		"width": "1.5 m",
		"height": "240 cm",
		"kerb:height": "5 cm",
		"automatic_door": "continuous",
		"door:handle": "crash_bar",
		"material": "glass",
		"entrance:coloured_band": "yes"
	}
	osm = setOsmTags(E.node(), osmTags)

	elem = E.node()
	tags.appendCommonEntranceTags(osm, osmTags, elem)

	assertTag(elem, "EntrancePassage", "Door")
	assertTag(elem, "IsEntry", "No")
	assertTag(elem, "PublicCode", "42")
	assertTag(elem, "Width", "1.50")
	assertTag(elem, "Height", "2.40")
	assertTag(elem, "KerbHeight", "0.05")
	assertTag(elem, "AutomaticDoor", "Yes")
	assertTag(elem, "Door", "Hinged")
	assertTag(elem, "DoorHandle:Inside", "CrashBar")
	assertTag(elem, "DoorHandle:Outside", "CrashBar")
	assertTag(elem, "GlassDoor", "Yes")


def test_appendCommonQuayTags():
	elem = E.node()
	tags.appendCommonQuayTags({
		"local_ref": "42",
		"tactile_paving": "yes",
		"wheelchair": "no",
		"ref:FR:Tallis": "1234"
	}, elem)

	assertTag(elem, "PublicCode", "42")
	assertTag(elem, "TactileWarningStrip", "Good")
	assertTag(elem, "WheelchairAccess", "No")
	assertTag(elem, "Ref:Export:Id", "FR:Quay:1234:")


def test_appendCommonStationTags():
	elem = E.node()
	tags.appendCommonStationTags({
		"departures_board:speech_output": "yes",
		"departures_board": "yes",
		"passenger_information_display": "yes",
		"service:SNCF:acces_plus": "yes",
		"ref:FR:uic8": "12345"
	}, elem)

	assertTag(elem, "AudioInformation", "Yes")
	assertTag(elem, "VisualDisplays", "Yes")
	assertTag(elem, "RealTimeDepartures", "Yes")
	assertTag(elem, "Assistance", "Boarding")
	assertTag(elem, "Ref:Export:Id", "FR:StopPlace:StopArea_OCE12345_rail:")


def test_appendCommonParkingTags():
	elem = E.node()
	tags.appendCommonParkingTags({
		"surface": "asphalt",
		"smoothness": "good",
		"lit": "yes",
		"incline": "5%",
		"incline:across": "8%",
		"orientation": "diagonal",
		"markings": "no",
		"capacity:charging": "2",
		"length": "4",
		"width": "3",
	}, elem)

	assertTag(elem, "FlooringMaterial", "Asphalt")
	assertTag(elem, "Flooring", "Good")
	assertTag(elem, "Lighting", "Yes")
	assertTag(elem, "Slope", "5.00")
	assertTag(elem, "Tilt", "8.00")
	assertTag(elem, "TiltSide", "Right")
	assertTag(elem, "BayGeometry", "Angled")
	assertTag(elem, "ParkingVisibility", "Unmarked")
	assertTag(elem, "VehicleRecharging", "Yes")
	assertTag(elem, "Width", "3.00")
	assertTag(elem, "Length", "4.00")


def test_appendElevatorTags():
	elem = E.node()
	tags.appendElevatorTags({
		"highway": "elevator",
		"width": "1.5",
		"length": "2.0",
		"supervised": "no",
		"ref": "1234",
		"audio_loop": "yes",
		"tactile_writing:engraved_printed_letters": "yes",
		"speech_output": "no",
		"elevator:mirror": "yes",
	}, elem)

	assertTag(elem, "InternalWidth", "1.50")
	assertTag(elem, "Depth", "2.00")
	assertTag(elem, "Attendant", "No")
	assertTag(elem, "PublicCode", "1234")
	assertTag(elem, "MagneticInductionLoop", "Yes")
	assertTag(elem, "RaisedButtons", "Raised")
	assertTag(elem, "AudioAnnouncements", "No")
	assertTag(elem, "MirrorOnOppositeSide", "Yes")


def test_appendCommonPathTags():
	elem = E.node()
	tags.appendCommonPathTags({
		"wheelchair": "designated",
		"incline:across": "5%",
		"footway": "crossing",
		"width": "2 m",
		"covered": "yes",
		"incline": "2%",
		"surface": "artificial_turf",
		"smoothness": "impassable",
		"guide_strips": "no",
		"crossing:island": "yes",
		"button_operated": "yes",
		"crossing": "uncontrolled",
		"crossing:markings": "bla;dashes;blou",
		"crossing:barrier": "full",
		"crossing:bell": "no",
		"tactile_writing": "yes",
		"level": "0",
		"handrail:center": "yes",
		"step_count": "1",
		"step:height": "15 cm",
		"step:length": "0.12",
		"conveying": "yes",
		"maxweight": "0.1",
		"traffic_signals:vibration": "yes",
		"tactile_writing:embossed_printed_letters:fr": "yes",
		"step:condition": "uneven",
		"ramp": "yes",
	}, elem)

	assertTag(elem, "WheelchairAccess", "Yes")
	assertTag(elem, "Tilt", "5.00")
	assertTag(elem, "TiltSide", "Right")
	assertTag(elem, "HighwayType", "Street")
	assertTag(elem, "Width", "2.00")
	assertTag(elem, "StructureType", "Underpass")
	assertTag(elem, "Outdoor", "Covered")
	assertTag(elem, "Incline", "Up")
	assertTag(elem, "FlooringMaterial", "Grass")
	assertTag(elem, "Flooring", "Hazardous")
	assertTag(elem, "CrossingIsland", "Yes")
	assertTag(elem, "CrossingBarrier", "Yes")
	assertTag(elem, "PedestrianLights", "Yes")
	assertTag(elem, "VisualGuidanceBands", "Yes")
	assertTag(elem, "AcousticCrossingAids", "None")
	assertTag(elem, "VibratingCrossingAids", "Good")
	assertTag(elem, "FixMe:VibratingCrossingAids", "Vérifier l'état du dispositif délivrant des vibrations de ce passage piéton")
	assertTag(elem, "Level", "0")
	assertTag(elem, "Handrail", "Center")
	assertTag(elem, "StepCount", "1")
	assertTag(elem, "StepHeight", "0.15")
	assertTag(elem, "StepLength", "0.12")
	assertTag(elem, "Conveying", "Forward")
	assertTag(elem, "MaxWeight", "100")
	assertTag(elem, "Handrail:TactileWriting", "Yes")
	assertTag(elem, "StepCondition", "Uneven")
	assertTag(elem, "StairRamp", "Other")


def test_appendCommonPoiTags():
	osmTags = {
		"ref:FR:SIRET": "823262530000038",
		"phone": "+33 1 23 45 67 89",
		"contact:website": "https://yukaimaps.fr",
		"addr:postcode": "35190",
	}

	elem = E.node()
	tags.appendCommonPoiTags(osmTags, elem)

	assertTag(elem, "Ref:FR:SIRET", "823262530000038")
	assertTag(elem, "Phone", "+33 1 23 45 67 89")
	assertTag(elem, "Website", "https://yukaimaps.fr")
	assertTag(elem, "PostCode", "35190")


@pytest.mark.parametrize(("access", "fee", "wifi", "wdmres"), (
	("wlan", "no", None, "Free"),
	("yes", "no", None, "Free"),
	("wifi", "no", None, "Free"),
	(None, None, "free", "Free"),
	("wlan", None, None, "Yes"),
	("yes", None, None, "Yes"),
	("wifi", None, None, "Yes"),
	("wlan", "42€", None, "Yes"),
	(None, None, "yes", "Yes"),
	("no", None, None, "No"),
	(None, None, "no", "No"),
	(None, None, None, None),
))
def test_appendCommonPoiTags_wifi(access, fee, wifi, wdmres):
	osmTags = {}

	if access is not None:
		osmTags["internet_access"] = access
	if fee is not None:
		osmTags["internet_access:fee"] = fee
	if wifi is not None:
		osmTags["wifi"] = wifi

	elem = E.node()
	tags.appendCommonPoiTags(osmTags, elem)

	assertTag(elem, "WiFi", wdmres)


@pytest.mark.parametrize(("tw", "t", "wdmres"), (
	("yes", "yes", "Yes"),
	("no", "yes", "Bad"),
	(None, "no", "No"),
	(None, None, None),
))
def test_appendCommonPoiTags_toilets(tw, t, wdmres):
	osmTags = {}

	if tw is not None:
		osmTags["toilets:wheelchair"] = tw
	if t is not None:
		osmTags["toilets"] = t

	elem = E.node()
	tags.appendCommonPoiTags(osmTags, elem)

	assertTag(elem, "Toilets", wdmres)


def test_appendCommonObstacleTags():
	osmTags = {
		"length": "2",
		"width": "4",
		"maxwidth": "3.8",
		"kerb": "flush",
		"ele": "200",
		"local_ref": "1234",
		"fee": "yes",
		"changing_table": "no",
		"shower": "yes",
		"supervised": "yes"
	}

	elem = E.node()
	tags.appendCommonObstacleTags(osmTags, elem)

	assertTag(elem, "Length", "2.00")
	assertTag(elem, "Width", "4.00")
	assertTag(elem, "RemainingWidth", "3.80")
	assertTag(elem, "KerbDesign", "Flush")
	assertTag(elem, "KerbHeight", "0.01")
	assertTag(elem, "FixMe:KerbHeight", "Vérifier la hauteur du ressaut")
	assertTag(elem, "Altitude", "200")
	assertTag(elem, "PublicCode", "1234")
	assertTag(elem, "FreeToUse", "No")
	assertTag(elem, "Toilets:BabyChange", "No")
	assertTag(elem, "Staffing", "FullTime")


@pytest.mark.parametrize(("osmTags", "expectedWdmTags"), (
	({"wheelchair": "yes"}, {"WheelchairAccess": "Yes"}),
	({"wheelchair": "no"}, {"WheelchairAccess": "No"}),
	({"wheelchair": "bad"}, {"WheelchairAccess": "Limited"}),
	({"wheelchair": "limited"}, {"WheelchairAccess": "Limited"}),
	({"tactile_paving": "yes"}, {"TactileWarningStrip": "Good", "FixMe:TactileWarningStrip": "Vérifier l’état du marquage de la borne d’éveil à vigilance"}),
	({"tactile_paving": "contrasted"}, {"TactileWarningStrip": "Good", "FixMe:TactileWarningStrip": "Vérifier l’état du marquage de la borne d’éveil à vigilance"}),
	({"tactile_paving": "no"}, {"TactileWarningStrip": "None"}),
	({"lit": "yes"}, {"Lighting": "Yes"}),
	({"lit": "no"}, {"Lighting": "No"}),
	({"lit:perceived": "good"}, {"Lighting": "Yes"}),
	({"lit:perceived": "daylike"}, {"Lighting": "Yes"}),
	({"lit:perceived": "none"}, {"Lighting": "No"}),
	({"lit:perceived": "minimal"}, {"Lighting": "No"}),
	({"lit:perceived": "bof"}, {"Lighting": "Bad"}),
	({"lit": "no", "lit:perceived": "bof"}, {"Lighting": "No"}),
	({"departures_board:speech_output": "yes"}, {"AudibleSignals": "Yes"}),
	({"passenger_information_display:speech_output": "yes"}, {"AudibleSignals": "Yes"}),
	({"announcement": "yes"}, {"AudibleSignals": "Yes"}),
	({"speech_output": "yes"}, {"AudibleSignals": "Yes"}),
	({"departures_board:speech_output": "no"}, {"AudibleSignals": "No"}),
	({"passenger_information_display:speech_output": "no"}, {"AudibleSignals": "No"}),
	({"announcement": "no"}, {"AudibleSignals": "No"}),
	({"speech_output": "no"}, {"AudibleSignals": "No"}),
	({"departures_board": "yes"}, {"VisualSigns": "Yes"}),
	({"passenger_information_display": "yes"}, {"VisualSigns": "Yes"}),
))
def test_appendImpairmentTags(osmTags, expectedWdmTags):
	wdm = E.node()

	tags.appendImpairmentTags(osmTags, wdm)

	for ek, ev in expectedWdmTags.items():
		assertTag(wdm, ek, ev)


@pytest.mark.parametrize(("prefix", "nb", "street", "wdmres"), (
	("addr", "42", "Rue du Bois", "42 Rue du Bois"),
	("contact", "42", "Rue du Bois", "42 Rue du Bois"),
	("addr", None, "Rue du Bois", "Rue du Bois"),
	("contact", None, "Rue du Bois", "Rue du Bois"),
	("addr", "42", None, None),
	("contact", "42", None, None),
	(None, None, None, None),
))
def test_appendAddressTags(prefix, nb, street, wdmres):
	osmTags = {}

	if prefix is not None:
		osmTags[prefix+":postcode"] = "35190"
	if nb is not None:
		osmTags[prefix+":housenumber"] = nb
	if street is not None:
		osmTags[prefix+":street"] = street

	elem = E.node()
	tags.appendAddressTags(osmTags, elem)

	assertTag(elem, "AddressLine", wdmres)
	if prefix is not None:
		assertTag(elem, "PostCode", "35190")


def test_appendAddressTags_withRel():
	osmTags = {"addr:housenumber": "42"}
	osmRels = [
		E.relation(
			E.member(type="node", ref="1", role="house"),
			E.tag(k="type", v="associatedStreet"),
			E.tag(k="name", v="Boulevard du Pâté"),
			E.tag(k="addr:postcode", v="35190")
		)
	]

	wdm = E.node()
	tags.appendAddressTags(osmTags, wdm, osmRels)
	assertTag(wdm, "AddressLine", "42 Boulevard du Pâté")
	assertTag(wdm, "PostCode", "35190")


@pytest.mark.parametrize(("lit", "perceived", "wdmres"), (
	(None, None, None),
	("no", None, "No"),
	("yes", None, "Yes"),
	(None, "good", "Yes"),
	(None, "daylike", "Yes"),
	(None, "none", "No"),
	(None, "minimal", "No"),
	(None, "bofbof", "Bad"),
	("yes", "none", "Yes"),
))
def test_appendLightingTags(lit, perceived, wdmres):
	osmTags = {}

	if lit is not None:
		osmTags["lit"] = lit
	if perceived is not None:
		osmTags["lit:perceived"] = perceived

	elem = E.node()
	tags.appendLightingTag(osmTags, elem)

	assertTag(elem, "Lighting", wdmres)


@pytest.mark.parametrize(("input", "output"), (
	({}, {}),
	({"changing_table": "yes", "changing_table:wheelchair": "yes"}, {"Toilets:BabyChange": "Yes"}),
	({"changing_table": "yes"}, {"Toilets:BabyChange": "Bad"}),
	({"changing_table": "no"}, {"Toilets:BabyChange": "No"}),
	({"shower": "yes"}, {"Toilets:Shower": "Yes"}),
	({"shower": "no"}, {"Toilets:Shower": "No"}),
	({"supervised": "no"}, {"Staffing": "No"}),
	({"supervised": "yes"}, {"Staffing": "FullTime"}),
	({"supervised": "sometimes"}, {"Staffing": "PartTime"}),
	({"toilets:handwashing": "yes"}, {"Toilets:HandWashing": "Yes"}),
	({"toilets:handwashing": "no"}, {"Toilets:HandWashing": "No"}),
	({"drinking_water": "yes"}, {"Toilets:DrinkingWater": "Yes"}),
	({"drinking_water": "no"}, {"Toilets:DrinkingWater": "No"}),
	({"toilets:position": "urinal"}, {"Toilets:Position": "Urinal"}),
	({"toilets:position": "urinal;seated"}, {"Toilets:Position": "Seated;Urinal"}),
	({"toilets:position": "squat;urinal;seated"}, {"Toilets:Position": "Seated;Urinal;Squat"}),
))
def test_appendToiletsTags(input, output):
	wdm = E.node()

	tags.appendToiletsTags(input, wdm)

	for ek, ev in output.items():
		assertTag(wdm, ek, ev)


def assertTag(elem, k, v):
	tag = elem.findall(f"tag[@k='{k}']")
	print("Expect:", k,"=",v,"Got:",[etree.tostring(t) for t in tag])
	if v is not None:
		assert len(tag) == 1
		assert tag[0].get("v") == v
	else:
		assert len(tag) == 0


@pytest.mark.parametrize(("txt", "res"), (
	("test", "Test"),
	("testTwo", "TestTwo"),
	("brewing_supplies", "BrewingSupplies"),
	("", ""),
	(None, None),
))
def test_toWDMCase(txt, res):
	assert tags.toWDMCase(txt) == res


@pytest.mark.parametrize(("wdmTags", "res"), (
	({ "Ref:Import:Id": "n-1" }, False),
	({ "Ref:Import:Geometry:Id": "n-1" }, False),
	({ "SitePathLink": "Crossing" }, True),
))
def test_hasMeaningfulWDMTags(wdmTags, res):
	f = setOsmTags(E.node(), wdmTags)
	assert tags.hasMeaningfulWDMTags(f) == res
